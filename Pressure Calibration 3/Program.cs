﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Pressure_Calibration_3
{
    static class Program
    {
        public static equipmentTCPIP.equipmentParoTCPIP sensorPressure; //System pressure sensor.
        public static ManagerPresssure.pressureManager pManager; //System pressure Manager.
        public static RelayController.relayControllerSystem pRelay;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        //[STAThread]
        static void Main()
        {
            //Checking for network.


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());

        }

        static private bool checkForNetwork()
        {
            return System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
        }

        static private bool checkForAddress()
        {
            System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient();
            try
            {
                client.Connect("192.168.1.1", 80);
                client.Close();
                return true;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                client.Close();
                return false;
            }

        }
    }
}
