﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace rackCalibration
{
    public class rackPressure
    {
        //Public Veriables
        
        //Rack ID
        public string rackId = "";
        public List<runnerPressureAssembly.runnerPressure> runners = new List<runnerPressureAssembly.runnerPressure>();

        //Private items.
        bool connectOnStartup = true;


        public rackPressure(string ID)
        {
            //Getting the rack ID.
            rackId = ID;

        }


        public void addRunner(string runnerID, List<string> arrayOfRunnerIPAddress, List<int> arrayOfRunnerPorts)
        {
            runnerPressureAssembly.runnerPressure tempRunner = new runnerPressureAssembly.runnerPressure();
            tempRunner.runnerID = rackId + runnerID;

            if (connectOnStartup)
            {
                tempRunner.connectRunnerToSystem(arrayOfRunnerIPAddress.ToArray(), arrayOfRunnerPorts.ToArray());
            }

            runners.Add(tempRunner); 
        }

        public void connectOnStartUp(bool state)
        {
            connectOnStartup = state;
        }

    }

    public class rackSettings
    {
        public string rackID;
        public List<string> runnerSettings;

    }

}
