﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Pressure_Calibration_3
{

    //Delegate for update
    delegate void upDateGraph();
    delegate void addGraphData(int x, double y);


    public partial class frmPressureControl : Form
    {

        Chart pChart = null;
        double pChartYMin = 2000;
        double pChartYMax = 0;
        double YScaleAdd = 50;

        int pChartXMin = 0;
        int pChartXMax = 30;

        

        public frmPressureControl()
        {
            InitializeComponent();

        }

        private void frmPressureControl_Load(object sender, EventArgs e)
        {
            updateDisplay();

            createGraph();

            System.Threading.Thread genData = new System.Threading.Thread(testData);
            genData.Name = "Test Data";
            //genData.Start();

            if (Program.sensorPressure != null)
            {
                Program.pManager = new ManagerPresssure.pressureManager();
                Program.pManager.setPressureManagerRelay(Program.pRelay);
                Program.pManager.setReferancePressureSensor(Program.sensorPressure);

                Program.pManager.setDesiredPressureEnviroment((Convert.ToDouble(textBoxSetPoint.Text) - Convert.ToDouble(textBoxTol.Text)), (Convert.ToDouble(textBoxSetPoint.Text) + Convert.ToDouble(textBoxTol.Text)),
                    Convert.ToDouble(textBoxSetPoint.Text), 60);
                Program.pManager.setDesiredPressureStability(-.01, 0.01, 0, 30);

                Program.sensorPressure.updatedPressureValue.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(updatedPressureValue_PropertyChange);
            }
        }

        void updatedPressureValue_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            try
            {
                this.Invoke(new addGraphData(this.addGraphData), new object[] { 0, Program.sensorPressure.getCurrentPressure() });
            }
            catch
            {

            }

            try
            {
                findScale();
                this.Invoke(new upDateGraph(this.updateGraphScale));
                this.Invoke(new upDateGraph(this.updatePressureDisplay));
            }
            catch
            {

            }

        }

        /// <summary>
        /// Updated the display regrading system changes.
        /// </summary>
        void updateDisplay()
        {
            //Enable/Disable control
            if (Program.sensorPressure != null)
            {
                groupBoxPMControl.Enabled = true;
            }
            else
            {
                groupBoxPMControl.Enabled = false;
            }
        }

        void createGraph()
        {
            pChart = new Chart();
            pChart.Location = new System.Drawing.Point(0, 0);
            pChart.Size = new Size(groupBoxPMControl.Size.Width, 150);

            pChart.ChartAreas.Add("draw");

            pChart.ChartAreas["draw"].AxisX.Minimum = pChartXMin;
            pChart.ChartAreas["draw"].AxisX.Maximum = pChartXMax;
            pChart.ChartAreas["draw"].AxisX.Interval = 1;
            pChart.ChartAreas["draw"].AxisX.LabelStyle.Enabled = false;
            pChart.ChartAreas["draw"].AxisX.MajorGrid.LineColor = Color.White;
            pChart.ChartAreas["draw"].AxisX.MajorGrid.LineDashStyle = ChartDashStyle.Dash;

            pChart.ChartAreas["draw"].AxisY.Minimum = 600;
            pChart.ChartAreas["draw"].AxisY.Maximum = 1050;
            pChart.ChartAreas["draw"].AxisY.Interval = 100;
            pChart.ChartAreas["draw"].AxisY.LabelStyle.Format = "0.000";
            pChart.ChartAreas["draw"].AxisY.MajorGrid.LineColor = Color.White;
            pChart.ChartAreas["draw"].AxisY.MajorGrid.LineDashStyle = ChartDashStyle.Dash;

            pChart.ChartAreas["draw"].BackColor = Color.Black;

            //Creating a series.
            pChart.Series.Add("pData");

            pChart.Series["pData"].ChartType = SeriesChartType.Line;

            //Line settings.
            pChart.Series["pData"].Color = Color.LightGreen;
            pChart.Series["pData"].BorderWidth = 3;

            double[] curPData = new double[0];

            for (int i = 0; i < curPData.Length; i++)
            {
                pChart.Series["pData"].Points.AddXY(i, curPData[i]);
            }

            groupBoxPMControl.Controls.Add(pChart);

        }

        private void testData()
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;

            //Random fakePressure = new Random(1000);

            while (true)
            {
                try
                {

                    //Adding some fake data.
                    //this.Invoke(new addGraphData(this.addGraphData), new object[] { 0, fakePressure.Next(800, 1050) });

                    double min = 2000;
                    double max = 0;

                    //Going through the data.
                    for (int i = pChartXMin; i < pChart.Series["pData"].Points.Count; i++)
                    {
                        double[] yData = pChart.Series["pData"].Points[i].YValues;

                        if (min > yData[0]) { pChartYMin = yData[0]; min = yData[0]; }
                        if (max < yData[0]) { pChartYMax = yData[0]; max = yData[0]; }
                    }

                    YScaleAdd = (max - min) / 10;

                    try
                    {
                        this.Invoke(new upDateGraph(this.updateGraphScale));
                    }
                    catch
                    {

                    }

                    System.Threading.Thread.Sleep(1000);
                }
                catch
                {
                    break;
                }
            }
        }

        private void findScale()
        {
            double min = 2000;
            double max = 0;

            //Going through the data.
            for (int i = pChartXMin; i < pChart.Series["pData"].Points.Count; i++)
            {
                double[] yData = pChart.Series["pData"].Points[i].YValues;

                if (min > yData[0]) { pChartYMin = yData[0]; min = yData[0]; }
                if (max < yData[0]) { pChartYMax = yData[0]; max = yData[0]; }
            }

            YScaleAdd = (max - min) / 10;
        }

        private void updateGraphScale()
        {
            pChart.ChartAreas["draw"].AxisY.Minimum = pChartYMin - YScaleAdd;
            pChart.ChartAreas["draw"].AxisY.Maximum = pChartYMax + YScaleAdd;



            if (YScaleAdd > 0.001)
            {
                pChart.ChartAreas["draw"].AxisY.Interval = YScaleAdd;
            }
            else
            {
                pChart.ChartAreas["draw"].AxisY.Interval = 0.0010;
            }
            

            if (pChart.Series["pData"].Points.Count > 28)
            {
                pChartXMin += 1;
                pChartXMax += 1;

                pChart.ChartAreas["draw"].AxisX.Minimum = pChartXMin;
                pChart.ChartAreas["draw"].AxisX.Maximum = pChartXMax;
            }

        }

        private void addGraphData(int X, double Y)
        {
            pChart.Series["pData"].Points.AddXY(pChart.Series["pData"].Points.Count, Y);
        }

        private void updatePressureDisplay()
        {
            labelPressureReading.Text = Program.sensorPressure.getCurrentPressure().ToString("0.000");

            if (Program.pManager != null)
            {
                labelPRate.Text = Program.pManager.getChangeRatePerMin().ToString("0.000");
            }
            else
            {
                labelPRate.Text = "";
            }
        }

        private void updateControl()
        {
            buttonGo.Enabled = true;
        }

        private void buttonGo_Click(object sender, EventArgs e)
        {

            /*
            Program.pManager = new ManagerPresssure.pressureManager();
            Program.pManager.setPressureManagerRelay(Program.pRelay);
            Program.pManager.setReferancePressureSensor(Program.sensorPressure);
            */

            //Setting the Desired Pressure.
            Program.pManager.setDesiredPressureEnviroment((Convert.ToDouble(textBoxSetPoint.Text) - Convert.ToDouble(textBoxTol.Text)), (Convert.ToDouble(textBoxSetPoint.Text) + Convert.ToDouble(textBoxTol.Text)),
                    Convert.ToDouble(textBoxSetPoint.Text), 60);
            Program.pManager.setDesiredPressureStability(-.01, 0.01, 0, 30);

            Program.pManager.gotoPressureEvent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(gotoPressureEvent_PropertyChange);
            Program.pManager.setPressureSystemTemp(20.0);

            //Starting the move.
            Program.pManager.gotoPressure(6, Convert.ToDouble(textBoxSetPoint.Text));

            buttonGo.Enabled = false;

        }

        void gotoPressureEvent_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            if (data.NewValue == "Goto Pressure Processes has ended")
            {
                this.Invoke(new upDateGraph(this.updateControl));
            }
        }


        //Controls the Pressure Up and Down functions.
        private void buttonUp_MouseDown(object sender, MouseEventArgs e)
        {
            Program.pRelay.commandRelayBoard("PressureUp", true);
        }

        private void buttonUp_MouseUp(object sender, MouseEventArgs e)
        {
            Program.pRelay.commandRelayBoard("PressureUp", false);
        }

        private void buttonDown_MouseDown(object sender, MouseEventArgs e)
        {
            Program.pRelay.commandRelayBoard("PressureDown", true);
        }

        private void buttonDown_MouseUp(object sender, MouseEventArgs e)
        {
            Program.pRelay.commandRelayBoard("PressureDown", false);
        }


    }
}
