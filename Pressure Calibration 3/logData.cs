﻿///Logging class for making excel output files.
///William Jones
///4/22/2014

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Logs
{
    /// <summary>
    /// Class only seems to work with Office 2010 and greater.
    /// </summary>
    public class logData
    {
        /*
        Microsoft.Office.Interop.Excel.Application oXL;
        Microsoft.Office.Interop.Excel._Workbook oWB;
        Microsoft.Office.Interop.Excel._Worksheet oSheet;
        Microsoft.Office.Interop.Excel.Range oRng;

        int pointerCol = 1;
        int pointerRow = 2;
        string[] excelRowLetter;

        public logData(string dataName)
        {

            populateRowLetters();

            try
            {
                //Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = true;

                //Get a new workbook.
                oWB = (Microsoft.Office.Interop.Excel._Workbook)(oXL.Workbooks.Add(System.Reflection.Missing.Value));
                oSheet = (Microsoft.Office.Interop.Excel._Worksheet)oWB.ActiveSheet;
                oSheet.Name = dataName;


                /*
                //Add table headers going cell by cell.
                oSheet.Cells[1, 1] = "First Name";
                oSheet.Cells[1, 2] = "Last Name";
                oSheet.Cells[1, 3] = "Full Name";
                oSheet.Cells[1, 4] = "Salary";

                //Format A1:D1 as bold, vertical alignment = center.
                oSheet.get_Range("A1", "D1").Font.Bold = true;
                oSheet.get_Range("A1", "D1").VerticalAlignment =
                    Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                // Create an array to multiple values at once.
                string[,] saNames = new string[5, 2];

                saNames[0, 0] = "John";
                saNames[0, 1] = "Smith";
                saNames[1, 0] = "Tom";
                saNames[1, 1] = "Brown";
                saNames[2, 0] = "Sue";
                saNames[2, 1] = "Thomas";
                saNames[3, 0] = "Jane";
                saNames[3, 1] = "Jones";
                saNames[4, 0] = "Adam";
                saNames[4, 1] = "Johnson";

                //Fill A2:B6 with an array of values (First and Last Names).
                oSheet.get_Range("A2", "B6").Value2 = saNames;

                //Fill C2:C6 with a relative formula (=A2 & " " & B2).
                oRng = oSheet.get_Range("C2", "C6");
                oRng.Formula = "=A2 & \" \" & B2";

                //Fill D2:D6 with a formula(=RAND()*100000) and apply format.
                oRng = oSheet.get_Range("D2", "D6");
                oRng.Formula = "=RAND()*100000";
                oRng.NumberFormat = "$0.00";

                //AutoFit columns A:D.
                oRng = oSheet.get_Range("A1", "D1");
                oRng.EntireColumn.AutoFit();

                //Manipulate a variable number of columns for Quarterly Sales Data.
                //DisplayQuarterlySales(oSheet);

                //Make sure Excel is visible and give the user control
                //of Microsoft Excel's lifetime.
                oXL.Visible = true;
                oXL.UserControl = true;
                 
            }
            catch (Exception theException)
            {
                String errorMessage;
                errorMessage = "Error: ";
                errorMessage = String.Concat(errorMessage, theException.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, theException.Source);

                //MessageBox.Show(errorMessage, "Error");
            }

        }

        private void populateRowLetters()
        {
            List<string> fin = new List<string>();

            string[] list = new string[26]{"A", "B", "C", "D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};

            for (int i = 0; i < list.Length; i++)
            {
                fin.Add(list[i]);
            }


            for(int i = 0; i<list.Length; i++)
            {
                

                for (int x = 0; x < list.Length; x++)
                {
                    string temp = list[i];
                    temp += list[x];
                    fin.Add(temp);
                }

                
            }

            excelRowLetter = fin.ToArray();
        }

        public void addCol(string name)
        {
            //Add table headers going cell by cell.
            oSheet.Cells[1, pointerCol] = name;

            //Format as bold, vertical alignment = center.
            oRng = oSheet.get_Range("A1", excelRowLetter[pointerCol].ToString() + "1");
            oRng.Font.Bold = true;
            oRng.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oRng.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            

            pointerCol++;
        }

        public void addRow(string[] rowData)
        {
            for (int i = 0; i < rowData.Length; i++)
            {
                oSheet.Cells[pointerRow, i + 1] = rowData[i];
            }
            pointerRow++;
        }

        public void formatOutput()
        {
            //AutoFit columns
            oRng = oSheet.get_Range("A1", excelRowLetter[pointerCol].ToString() + "1");
            oRng.EntireColumn.AutoFit();
        }

        public void formatData()
        {
            oRng = oSheet.get_Range("A1", excelRowLetter[pointerCol].ToString() + pointerRow.ToString());
            oRng.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            //oRng.Cells.NumberFormat = "0.000";

            Object arr = oRng.Value;


            int borderCount = 9;
            for (int div = 0; div < pointerRow - 1; div++)
            {
                if (div == borderCount)
                {
                    oRng = oSheet.get_Range("A" + (div).ToString(), excelRowLetter[pointerCol-2] + (div).ToString());
                    oRng.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                    borderCount += 8;
                }
            }


            
            List<string> cells = new List<string>();

            //Locating the fail sondes.
            for (int c = 0; c < pointerCol - 1; c++)
            {
                for (int r = 0; r < pointerRow - 1; r++)
                {
                    Microsoft.Office.Interop.Excel.Range objRange = oSheet.get_Range(excelRowLetter[c] + (r + 1).ToString(), excelRowLetter[c] + (r + 1).ToString());
                    string strData = objRange.get_Value(System.Reflection.Missing.Value).ToString();

                    if (strData == "Fail")
                    {
                        cells.Add(excelRowLetter[c] + (r + 1).ToString());
                    }
                }
            }

            //Turning failed sonde red
            foreach (string s in cells)
            {
                Microsoft.Office.Interop.Excel.Range objRange = oSheet.get_Range(s, s);
                objRange.Cells.Interior.Color = System.Drawing.Color.Red;
                objRange.EntireRow.Font.Bold = true;


            }


        }

        public void show()
        {
            oXL.Visible = true;
            oXL.UserControl = true;
        }

        
        public void addSheet(string title)
        {
            pointerCol = 1;
            pointerRow = 2;
            Microsoft.Office.Interop.Excel.Worksheet newSheet = (Microsoft.Office.Interop.Excel.Worksheet)this.oXL.Worksheets.Add();
            newSheet.Name = title;

            oSheet = newSheet;
        }
        

        public void setPrint()
        {
            oSheet.PageSetup.PrintArea = "A1:" + excelRowLetter[pointerCol - 1] + pointerRow.ToString();
            
            //oSheet.PageSetup.Zoom = false;
            
            
        }

        public void setLandscape()
        {
            oSheet.PageSetup.FitToPagesWide = 1;
            //oSheet.PageSetup.Zoom = false;
            oSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
            
        }

        public void setPortrait()
        {
            oSheet.PageSetup.FitToPagesWide = 1;
            //oSheet.PageSetup.Zoom = false;
            oSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlPortrait;
        }

        public void saveFile(string fileName)
        {
            if (fileName != null)
            {
                oSheet.SaveAs(fileName, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value, System.Reflection.Missing.Value);
            }
        }

        */
    }

    /// <summary>
    /// Designed for the pressure calibration
    /// </summary>
    public class LogDataHTML
    {
        string totalHTML;

        public LogDataHTML(string title, string reportHeaderDetails)
        {
            totalHTML += "<html>\n<title>" + title + "</title>\n<head>\n<h2>" + title + "</h2>\n<strong>" + reportHeaderDetails + "</strong>\n</head>\n<body>\n";
        }


        //Build row of upto four.

        public void addRowData(object[] rowData)
        {
            if (rowData.Length > 4)
            {
                throw new Exception("Limit of 4 exceeped.");
            }
            

            //Added starting part.
            totalHTML += "<table width=\"100%\">\n";
            totalHTML += "<tr>\n";

            foreach (object run in rowData)
            {
                List<reportData> curRunner = (List<reportData>)run;

                //Adding the col header.
                totalHTML += "<td>\n";
                totalHTML += "<table style=\"border-collapse: collapse;\" width=\"180px\">\n";
                totalHTML += "<tr style=\"border:2px solid Black;\">\n";
                totalHTML += "<td>\n";
                totalHTML += "<table style=\"border-collapse: collapse;\" width=\"100%\">\n";
                totalHTML += "<tr>\n";
                totalHTML += "<td width=\"33%\"></td>\n";
                totalHTML += "<td width=\"33%\"><center>" + curRunner[0].Runner + "</center></td>\n";
                totalHTML += "<td width=\"33%\"></td>\n";
                totalHTML += "</tr>\n";
                totalHTML += "<tr>\n";
                totalHTML += "<td style=width:33% bgcolor=\"white\"><table><tr><td><center>SondeID</center></td></tr></table></td>\n";
                totalHTML += "<td style=width:33% bgcolor=\"white\"><center>Pos</center></td>\n";
                totalHTML += "<td style=width:33% bgcolor=\"white\"><center>Pass/Fail</center></td>\n";
                totalHTML += "</tr>\n";


                foreach (reportData rd in curRunner)
                {
                    if(rd.result)
                    {
                        totalHTML +="<tr>\n";
                    }
                    else
                    {
                        totalHTML += "<tr style=\"border:2px solid Black;\">\n";
                    }

                    totalHTML += "<td style=width:33% bgcolor=\"white\"><table><tr><td><center>" + rd.SondeID + "</center></td></tr></table></td>\n";
                    totalHTML += "<td style=width:33% bgcolor=\"white\"><center>" + rd.Pos + "</center></td>\n";

                    if(rd.result)
                    {
                        totalHTML += "<td style=width:33% bgcolor=\"white\"><center>Pass</center></td>\n";
                    }
                    else
                    {
                        totalHTML += "<td style=width:33% bgcolor=\"red\"><center>Fail</center></td>\n";
                    }

                    totalHTML += "</tr>\n";
	
	
                }

                totalHTML += "</table>\n";
                totalHTML += "</td>\n";
                totalHTML += "</tr>\n";
                totalHTML += "</table>\n";
                totalHTML += "</td>\n";



            }

            totalHTML += "</tr>\n";
            totalHTML += "</table>\n";
        }


        //close off html format
        public void finishFile()
        {
            totalHTML += "</body>\n</html>\n";
        }

        //save to file.
        public void saveFile(string fileName)
        {
           
            System.IO.File.WriteAllText(fileName, totalHTML);
        }
        
    }

    [Serializable]
    public class reportData
    {
        public string Runner;
        public string SondeID;
        public string Pos;
        public bool result;
    }

}
