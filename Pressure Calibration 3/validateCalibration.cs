﻿///William Jones
///4/15/2014
///InterMet Systems
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidateCalibration
{
    /// <summary>
    /// Class dedicated to collecting radiosonde and refreance data to QA check the radiosonde.
    /// This class was designed to be used with the Pressure Cablibration 3 software and hardware.
    /// </summary>
    public class validatePressureCalibration
    {
        /////////////////////////////////////////////////////
        //Public Veriables
        ////////////////////////////////////////////////////

        //Events
        public updateCreater.objectUpdate manifoldGoupChanged = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate manifoldGoupUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate statusUpdate = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate processComplete = new updateCreater.objectUpdate();

        public List<validatePoint> calPoints; //Calibration points.
        public managerChamber.chamberManager managerChamber; //The stability manager for the chamber
        public List<Calibration.runnerGroup> calibrationGroups = new List<Calibration.runnerGroup>();   //Groups that leak less then .006 and can be calibrated together.

        /////////////////////////////////////////////////////
        //Private Veriables
        ////////////////////////////////////////////////////

        //Calibration equipment
        private ManagerPresssure.pressureManager managerPressure; //Manager for pressure control
        //private TestEquipment.Paroscientific sensorPressure; //Pressure Sensor that will be used for the calibration OLD class
        private equipmentTCPIP.equipmentParoTCPIP sensorPressure;
        private TestEquipment.Thermotron chamber; //Chamber that will be used in calibration.

        private rackCalibration.rackPressure[] racks;
        private RelayController.relayControllerSystem[] relayPresureManifold;


        ///Veriables relating directly to the current calibration
        ///Things like is there a point trying to be achived.
        private bool calibrationUnderWay = false;
        private bool loggingData = false; //Might need to be moved to a logging class but for now here it is.
        private int currentCalibrationStep = -1;
        private DateTime startOfCalibration;
        private DateTime startOfCalibrationPoint;
        private int groupInCal = -1;
        private bool errorMode = false;
        private System.Threading.Thread threadValidate; //Thread to run the test in.

        //Auto Reset events to prevent process from moving forward until conditions are meet.
        private System.Threading.AutoResetEvent pressureConditionsReady = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent pressureStabilityReady = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent pressureWait = new System.Threading.AutoResetEvent(false);

        private System.Threading.AutoResetEvent chamberConditionReady = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent chamberStabilityReady = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent chamberTHReady = new System.Threading.AutoResetEvent(false);

        public validatePressureCalibration() { }

        public validatePressureCalibration(equipmentTCPIP.equipmentParoTCPIP calPressureSensor, ManagerPresssure.pressureManager calPressureManager, TestEquipment.Thermotron calChamber, rackCalibration.rackPressure[] incomingRacks, RelayController.relayControllerSystem[] incomingRelayManifolds)
        {
            //Applying the equipment to the calibration
            sensorPressure = calPressureSensor;
            managerPressure = calPressureManager;
            chamber = calChamber;
            racks = incomingRacks;
            relayPresureManifold = incomingRelayManifolds;

            //Connecting to pressure managers events
            managerPressure.gotoPressureEvent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(gotoPressureEvent_PropertyChange);
            managerPressure.managerError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(managerError_PropertyChange);
            managerPressure.pressureConditionReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(pressureConditionReady_PropertyChange);
            managerPressure.pressureStabilityReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(pressureStabilityReady_PropertyChange);

            //Connecting to the Pressure sensor
            sensorPressure.updatedPressureValue.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(updatedPressureValue_PropertyChange);

            //Connecting to chamber data.
            configureChamberManager();
        }

        public void start(List<Calibration.runnerGroup> incomingGroup, List<validatePoint> desiredCalPoints)
        {
            calibrationGroups = incomingGroup;  //Group to be tested.

            //Setting up the thread that will perform the test.
            calibrationUnderWay = true;
            currentCalibrationStep = 0;
            threadValidate = new System.Threading.Thread(threadValidateCalibration);
            threadValidate.Name = "threadValidate";
            threadValidate.IsBackground = true;
            

            calPoints = desiredCalPoints; //Setting the cal points for the calibration.
            startOfCalibration = DateTime.Now;
            startOfCalibrationPoint = DateTime.Now;

            //Starting the thread that will test the radiosondes.
            threadValidate.Start();
        }

        public void stop()
        {
            //Queing the thread to stop.
            calibrationUnderWay = false;
        }


        /// <summary>
        /// Thread that will do the testing.
        /// </summary>
        void threadValidateCalibration()
        {
            List<setPointData> totalData = new List<setPointData>();    //All of the data collected durring the run.

            while (calibrationUnderWay && calPoints.Count > currentCalibrationStep) //Main loop. Stops if ordered to or the calibration reaches past the last point.
            {
                setPointData curSetPointData = new setPointData();
                curSetPointData.groups = new List<curActiveGroup>();

                startOfCalibrationPoint = DateTime.Now;
                //Needs to break out after exceding the index.
                
                 
                if (chamber.CurrentChamberSPPC != calPoints[currentCalibrationStep].airTempSetPoint || currentCalibrationStep == 0) //Changing and resetting the chamber air temp if different then current.
                {
                    //Need to setup the chamber stability stuff.
                    managerChamber.setDesiredEnviroment(calPoints[currentCalibrationStep].airTempLower, calPoints[currentCalibrationStep].airTempUpper, calPoints[currentCalibrationStep].airTempSetPoint, calPoints[currentCalibrationStep].packetCountAT);
                    managerChamber.setDesiredEnviromentTh(calPoints[currentCalibrationStep].airTHLower, calPoints[currentCalibrationStep].airTHUpper, calPoints[currentCalibrationStep].airTHSetPoint, calPoints[currentCalibrationStep].packetCountTH);
                    managerChamber.setDesiredEnviromentStability(-0.06, 0.06, 0, 90); //Setting chamber stability settings. This might need to be moved to a file.
                    statusUpdate.UpdatingObject = (object)("Chamber set to: " + calPoints[currentCalibrationStep].airTempSetPoint.ToString());

                    //Move pressure to somewhere safe for temp change.
                    statusUpdate.UpdatingObject = (object)("Moving pressure to 700 mBar for tempature change.");
                    managerPressure.setDesiredPressureEnviroment(600, 800, 700, 30); //Setting manager to provent hang up.
                    managerPressure.gotoPressure(2, 700);

                    //Setting the chamber enviroments set point
                    //Changing to product temp
                    //chamber.setChamberTemp(calPoints[currentCalibrationStep].airTempSetPoint);
                    chamber.setProductTemp(calPoints[currentCalibrationStep].airTempSetPoint);

                    System.Threading.Thread.Sleep(1000);
                    chamber.startChamberManualMode(); //Starting the chamber.

                    ////////////////////////////////////////////////////////////
                    //This needs to be revisited to make the calibration stoped between points.
                    ///////////////////////////////////////////////////////////////


                    //Resetting chamber TH reset event.
                    chamberTHReady = new System.Threading.AutoResetEvent(false);

                    //Resetting the chamber condition ready set.
                    chamberConditionReady = new System.Threading.AutoResetEvent(false);
                    statusUpdate.UpdatingObject = (object)("Waiting for chamber to become stable.");


                    while (calibrationUnderWay && !chamberConditionReady.WaitOne(2000))
                    {
                        System.Threading.Thread.Sleep(1000);
                    }

                    statusUpdate.UpdatingObject = (object)("Chamber Conditions Ready.");


                    //Waiting for chamber to become stable.
                    //I think I want a time out here,,, or something that checks to make sure the system is running so we don't get caught here.
                    statusUpdate.UpdatingObject = (object)("Waiting for chamber TH to become stable.");






                    //Disableing this because it never seems to get to this point right now,,,
                    //chamberTHReady.WaitOne();




                    statusUpdate.UpdatingObject = (object)("Chamber TH Conditions Ready.");

                    chamberStabilityReady = new System.Threading.AutoResetEvent(false); //Setting the system to check for stability after we know the enviroment has been matched.
                    chamberStabilityReady.WaitOne();
                    statusUpdate.UpdatingObject = (object)("Chamber Stability Ready.");

                }

                

                if (managerPressure.checkMoveStatus())
                {
                    while (managerPressure.checkMoveStatus() == true)
                    {
                        statusUpdate.UpdatingObject = (object)("Pressure Move has not stopped. Waiting for it to shutdown. (PreSet)");
                        System.Threading.Thread.Sleep(5000);
                    }
                }


                //Set pressure conditions and stability settings.
                statusUpdate.UpdatingObject = (object)(currentCalibrationStep.ToString() + "," + calPoints[currentCalibrationStep].pressureLower.ToString() + "," + calPoints[currentCalibrationStep].pressureUpper.ToString() + "," + calPoints[currentCalibrationStep].pressureSetPoint.ToString() + "," +
                    calPoints[currentCalibrationStep].packetCountPressure.ToString());


                managerPressure.setDesiredPressureEnviroment(calPoints[currentCalibrationStep].pressureLower, calPoints[currentCalibrationStep].pressureUpper, calPoints[currentCalibrationStep].pressureSetPoint,
                    calPoints[currentCalibrationStep].packetCountPressure);
                managerPressure.setDesiredPressureStability(calPoints[currentCalibrationStep].pressureStabilityLower, calPoints[currentCalibrationStep].pressureStabilityUpper, calPoints[currentCalibrationStep].pressureStabilitySetPoint,
                    calPoints[currentCalibrationStep].packetCountPressureStability);


                //Loop through the runnerGroups.
                int countGroup = 0;
                foreach (Calibration.runnerGroup group in calibrationGroups)
                {
                    //Creating data contatiner for active group
                    curActiveGroup tempGroupData = new curActiveGroup();
                    tempGroupData.conditionData = new List<groupData>();

                    //Trigger the current manifold being worker on event.
                    groupInCal = countGroup;
                    manifoldGoupChanged.UpdatingObject = (object)countGroup;

                    //Opening group manifolds.
                    statusUpdate.UpdatingObject = (object)("Opening goup " + groupInCal.ToString() + " manifold ports.");
                    operateGroupManifold(group, true);

                    //Waiting for the system to recoil
                    System.Threading.Thread.Sleep(3000); //This is need to insure that the move calc for fine moves are not way off. Effected more at low pressure.


                    //Moving the system to pressure.
                    statusUpdate.UpdatingObject = (object)("Setting the group to the desired pressure.");


                    if (!managerPressure.gotoPressure(6, calPoints[currentCalibrationStep].pressureSetPoint)) //Moving the system to the desired pressure. If it can try again in 10 sec.
                    {
                        while (managerPressure.gotoPressure(6, calPoints[currentCalibrationStep].pressureSetPoint) == false)
                        {
                            System.Threading.Thread.Sleep(10000);
                        }
                    }

                    //Resetting that waiting for pressure conditions and stability to be ready.
                    resetAndWaitForPressure();
                    managerPressure.stopGoToPressure(); //This will stop a pressure move if we are ready to collect data. Just in case.

                    if (!calibrationUnderWay) { break; }     //Error handeling for cancel event.

                    if (managerPressure.checkMoveStatus())
                    {
                        while (managerPressure.checkMoveStatus() == true)
                        {
                            statusUpdate.UpdatingObject = (object)("Pressure Move has not stopped. Waiting for it to shutdown.");
                            System.Threading.Thread.Sleep(5000);
                        }
                    }


                    //Logging data.
                    int recordCount = 0;
                    statusUpdate.UpdatingObject = (object)("Loading data.");
                    while (recordCount < 30 && calibrationUnderWay) //Log the calibration data until packet count or cancel.
                    {
                        if (!calibrationUnderWay) { break; }     //Error handeling for cancel event.

                        //Waiting for new pressure data.
                        pressureWait.WaitOne();

                        statusUpdate.UpdatingObject = (object)("Logging: " + recordCount.ToString());

                        //Creating temp current data moment data container.
                        groupData tempMomentDat = new groupData();

                        //collecting the data.
                        tempMomentDat.groupDataItem = collectData();
                        tempMomentDat.group = group;

                        //Moving collected data to main holding object.
                        tempGroupData.conditionData.Add(tempMomentDat);

                        recordCount++;
                    }

                    //Applying the data out to the group.
                    curSetPointData.groups.Add(tempGroupData);
                    

                    //Close manifold ports.
                    statusUpdate.UpdatingObject = (object)("Closing the group manifold ports.");
                    operateGroupManifold(group, false);


                    countGroup++;

                    //updating group to -1 trigger the fact that all groups are closed.
                    if (countGroup == calibrationGroups.Count)
                    {
                        countGroup = -1;
                        manifoldGoupChanged.UpdatingObject = (object)countGroup;
                    }
                }

                totalData.Add(curSetPointData);

                statusUpdate.UpdatingObject = (object)("Moving to next calibration step.");
                currentCalibrationStep++; //Moving to the next calibration steps.
            }

            List<object> reportData = checkData(totalData); //List of reportData each item is a list of the report for the radiosonde.

            if(!System.IO.Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "VRunData\\"))
            {
                System.IO.Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "VRunData\\");
            }

            //Writing run results to a file.

            string outFileName = AppDomain.CurrentDomain.BaseDirectory + "VRunData\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "-Vrun";

            //Bin Saving
            System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.Stream stream = new System.IO.FileStream(outFileName + ".bin", System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);
            formatter.Serialize(stream, reportData);
            stream.Close();

            //Bin Saving
            System.IO.Stream stream2 = new System.IO.FileStream(outFileName + "-Full.bin", System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);
            formatter.Serialize(stream2, processData(totalData));
            stream.Close();

            //XML Saving
            try
            {
                //ValidateCalibration.setPointData[] testConvert = (ValidateCalibration.setPointData[])totalData.ToArray();
                List<List<reportData>> xmlData = processData(totalData);

                List<reportData>compile = new List<reportData>();

                for (int sp = 0; sp < xmlData.Count; sp++)
                { 
                    foreach(reportData rd in xmlData[sp])
                    {
                        compile.Add(rd);
                    }
                }


                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(reportData[]));
                System.IO.TextWriter textWriter = new System.IO.StreamWriter(outFileName + "-SetPoint.xml", false, Encoding.UTF8);
                serializer.Serialize(textWriter, compile.ToArray());
                textWriter.Close();
            }
            catch(Exception XMLerror)
            {
                statusUpdate.UpdatingObject = (object)XMLerror.InnerException.Message;
            }

            generateReport(reportData, outFileName);


            processComplete.UpdatingObject = reportData;

            //starting program to display results.

            System.Threading.Thread.Sleep(2000);

            System.Diagnostics.Process.Start(outFileName + ".html");
        }

        private void configureChamberManager()
        {
            if (managerChamber != null)
            {
                managerChamber.chamberConditionReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(chamberConditionReady_PropertyChange);
                managerChamber.chamberStabilityReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(chamberStabilityReady_PropertyChange);
                managerChamber.chamberTHReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(chamberTHReady_PropertyChange);
                managerChamber.managerError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(managerChamberError_PropertyChange);
                managerChamber = null;

                statusUpdate.UpdatingObject = (object)"Calibration Process Error: Restarting managerChamber";

                System.Threading.Thread.Sleep(1000);
            }

            managerChamber = new managerChamber.chamberManager(chamber);
            managerChamber.chamberConditionReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(chamberConditionReady_PropertyChange);
            managerChamber.chamberStabilityReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(chamberStabilityReady_PropertyChange);
            managerChamber.chamberTHReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(chamberTHReady_PropertyChange);
            managerChamber.managerError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(managerChamberError_PropertyChange);

            //Setting some defaults
            //Need to setup the chamber stability stuff.
            if (!calibrationUnderWay)
            {
                managerChamber.setDesiredEnviroment(-20, 50, 30, 60);
                managerChamber.setDesiredEnviromentTh(-100, 100, 0, 60);
                managerChamber.setDesiredEnviromentStability(-0.048, 0.048, 0, 90); //Setting chamber stability settings. This might need to be moved to a file.
            }
            else
            {
                managerChamber.setDesiredEnviroment(calPoints[currentCalibrationStep].airTempLower, calPoints[currentCalibrationStep].airTempUpper, calPoints[currentCalibrationStep].airTempSetPoint, calPoints[currentCalibrationStep].packetCountAT);
                managerChamber.setDesiredEnviromentTh(calPoints[currentCalibrationStep].airTHLower, calPoints[currentCalibrationStep].airTHUpper, calPoints[currentCalibrationStep].airTHSetPoint, calPoints[currentCalibrationStep].packetCountTH);
                managerChamber.setDesiredEnviromentStability(-0.048, 0.048, 0, 90); //Setting chamber stability settings. This might need to be moved to a file.
            }
        }

        private void resetAndWaitForPressure()
        {
        restartWait:
            //Resetting the auto reset events.
            pressureConditionsReady = new System.Threading.AutoResetEvent(false);
            pressureStabilityReady = new System.Threading.AutoResetEvent(false);

            //Waiting for the pressure to become in range and stable.
            pressureConditionsReady.WaitOne();

            //Waiting for pressure condition to stabilize. If it doesn't in less then 10 min then the group needs to be rechecked for leaks.
            if (!pressureStabilityReady.WaitOne(600000) && !errorMode) //The recheck will only happen when the calibration is running.
            {
                goto restartWait; //Resetting and waiting for stabliziation.
            }
        }

        public void operateGroupManifold(Calibration.runnerGroup group, bool incomingState)
        {
            foreach (runnerPressureAssembly.runnerPressure run in group.runners)
            {
                foreach (RelayController.relayControllerSystem rm in relayPresureManifold)
                {
                    if (rm.checkValidRequest(run.manifoldPos))
                    {
                        rm.commandRelayBoard(run.manifoldPos, incomingState); //Opening the manifolds.
                    }
                }
            }
        }

        public List<currentDataMoment> collectData()
        {
            //The data for the moment.
            List<currentDataMoment> tempTotalData = new List<currentDataMoment>();

            //Go throw the radiosodnes exposed to the pressure enviroment and collect the latest radiosondes data, and sensor data.
            foreach (RelayController.relayControllerSystem rm in relayPresureManifold)
            {
                string[] currentRelayFunctions = rm.RelayFunction();
                foreach (string relay in currentRelayFunctions) //Going throw the current relay looking for the open ports.
                {
                    if (rm.getRelayState(rm.getItemsIndex(relay)))
                    {
                        //If the relay is open now we will go throw the runners looking for the matching manifoldPos and then collect the radiosonde data and pressure sensor data.
                        foreach (rackCalibration.rackPressure r in racks)
                        {
                            foreach (runnerPressureAssembly.runnerPressure run in r.runners)
                            {
                                if (run.manifoldPos == relay)
                                {
                                    //Located the open relay and runner.
                                    foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)
                                    {
                                        if (run.currentRadiosondes.Contains(sonde.port)) //Collecting the data from radiosondes confirmed connected to system.
                                        {
                                            currentDataMoment temp = new currentDataMoment();
                                            temp.dataPointDateTime = DateTime.Now;
                                            temp.sondeData = sonde.CalData;
                                            temp.refPressure = sensorPressure.getCurrentPressure();
                                            temp.refAirTemp = chamber.CurrentChamberC;
                                            temp.refHumidity = chamber.CurrentChamberRH;
                                            temp.currentPoint = calPoints[currentCalibrationStep];
                                            temp.dataResults = new results();

                                            tempTotalData.Add(temp);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
               
            }

            return tempTotalData;
        }

        /// <summary>
        /// Method for checking the data from the radiosonde to the referances.
        /// </summary>
        /// <param name="dataToProcess"></param>
        public List<object> checkData(List<setPointData> dataToProcess)
        {
            List<object> totalRadiosondeReportData = new List<object>();

            foreach (setPointData sP in dataToProcess)  //All setpoint data.
            {
                List<reportData> radiosondeReportData = new List<reportData>();

                foreach (curActiveGroup aG in sP.groups) //All groups
                {
                    foreach (groupData gD in aG.conditionData) //All data collected at this setpoint for this group.
                    {
                        foreach (currentDataMoment cdm in gD.groupDataItem) //One record for one radiosonde of the data taken at the setpoint.
                        {
                            //Creating a filtered record for a radioosonde if it doesn't have one.
                            int sondeCount = 0;
                            foreach (reportData sonde in radiosondeReportData)
                            {
                                if (sonde.sondeID == cdm.sondeData.SondeID)
                                {
                                    sondeCount++;
                                }
                            }

                            reportData curSondeData;

                            //Selecting the radioosnde recored to use or create a new one if there is none.
                            if (sondeCount == 0)
                            {
                                curSondeData = new reportData();
                                curSondeData.sondeID = cdm.sondeData.SondeID;
                                curSondeData.sondeData = new List<currentDataMoment>();
                                curSondeData.sondeData.Add(cdm);


                                //Finding the runner and the pos.
                                foreach (runnerPressureAssembly.runnerPressure run in gD.group.runners)
                                {
               
                                    for(int sonde = 0; sonde < run.runnerRadiosondePorts.Length; sonde++)
                                    {
                                        if (run.runnerRadiosondePorts[sonde].CalData.SondeID == curSondeData.sondeID)
                                        {
                                            curSondeData.runner = run.runnerID;
                                            curSondeData.runnerPos = (sonde + 1).ToString();
                                        }
                                    }

                                }


                                radiosondeReportData.Add(curSondeData);
                                
                            }
                            else
                            {
                                foreach (reportData sonde in radiosondeReportData)
                                {
                                    if (sonde.sondeID == cdm.sondeData.SondeID)
                                    {
                                        curSondeData = sonde;
                                        curSondeData.sondeData.Add(cdm);
                                    }
                                }
                            }
                        }
                             
                    }
                }

                //Do some processing of data reduceing the data collecting at the setpoint for each radiosonde to one record.
                foreach (reportData sondeSPData in radiosondeReportData)
                {
                    currentDataMoment tempCompiledMoment = new currentDataMoment();
                    tempCompiledMoment = sondeSPData.sondeData[0];

                    for (int i = 1; i < sondeSPData.sondeData.Count; i++)
                    {
                        //Add all the relivent data into on object.
                        tempCompiledMoment.refPressure += sondeSPData.sondeData[i].refPressure;
                        tempCompiledMoment.refAirTemp += sondeSPData.sondeData[i].refAirTemp;
                        tempCompiledMoment.refHumidity += sondeSPData.sondeData[i].refHumidity;

                        tempCompiledMoment.sondeData.Pressure += sondeSPData.sondeData[i].sondeData.Pressure;
                        tempCompiledMoment.sondeData.Temperature += sondeSPData.sondeData[i].sondeData.Temperature;
                        tempCompiledMoment.sondeData.Humidity += sondeSPData.sondeData[i].sondeData.Humidity;
                    }

                    //Getting the average.
                    tempCompiledMoment.refPressure = tempCompiledMoment.refPressure / sondeSPData.sondeData.Count;
                    tempCompiledMoment.refAirTemp = tempCompiledMoment.refAirTemp / sondeSPData.sondeData.Count;
                    tempCompiledMoment.refHumidity = tempCompiledMoment.refHumidity / sondeSPData.sondeData.Count;

                    tempCompiledMoment.sondeData.Pressure = tempCompiledMoment.sondeData.Pressure / sondeSPData.sondeData.Count;
                    tempCompiledMoment.sondeData.Temperature = tempCompiledMoment.sondeData.Temperature / sondeSPData.sondeData.Count;
                    tempCompiledMoment.sondeData.Humidity = tempCompiledMoment.sondeData.Humidity / sondeSPData.sondeData.Count;

                    //Adding consolidated date to total data.
                    sondeSPData.sondeData.Clear();
                    sondeSPData.sondeData.Add(tempCompiledMoment);

                    //Running tests for pass fail on consolidated data.
                    sondeSPData.sondeData[0].dataResults.pDiff = sondeSPData.sondeData[0].sondeData.Pressure - sondeSPData.sondeData[0].refPressure;
                    sondeSPData.sondeData[0].dataResults.ATDiff = sondeSPData.sondeData[0].sondeData.Temperature - sondeSPData.sondeData[0].refAirTemp;
                    sondeSPData.sondeData[0].dataResults.uDiff = sondeSPData.sondeData[0].sondeData.Humidity - sondeSPData.sondeData[0].refHumidity;

                    sondeSPData.sondeData[0].dataResults.pressure = false;
                    sondeSPData.sondeData[0].dataResults.airTemp = false;
                    sondeSPData.sondeData[0].dataResults.humidity = false;

                    if (Math.Abs(sondeSPData.sondeData[0].dataResults.pDiff) < sondeSPData.sondeData[0].currentPoint.tolPressure) { sondeSPData.sondeData[0].dataResults.pressure = true; }
                    if (Math.Abs(sondeSPData.sondeData[0].dataResults.ATDiff) < sondeSPData.sondeData[0].currentPoint.tolAirTemp) { sondeSPData.sondeData[0].dataResults.airTemp = true; }
                    if (Math.Abs(sondeSPData.sondeData[0].dataResults.uDiff) < sondeSPData.sondeData[0].currentPoint.tolHumidity) { sondeSPData.sondeData[0].dataResults.humidity = true; }
                    
                }


                
        


                totalRadiosondeReportData.Add(radiosondeReportData);

            }

            return totalRadiosondeReportData;
        }

        /// <summary>
        /// Processed the results of the report data.
        /// </summary>
        /// <param name="radiosondeReportData"></param>
        /// <returns></returns>
        public List<reportData> processReportData(List<reportData> radiosondeReportData)
        {
            foreach (reportData sondeSPData in radiosondeReportData)
            {
                currentDataMoment tempCompiledMoment = new currentDataMoment();
                tempCompiledMoment = sondeSPData.sondeData[0];

                for (int i = 1; i < sondeSPData.sondeData.Count; i++)
                {
                    //Add all the relivent data into on object.
                    tempCompiledMoment.refPressure += sondeSPData.sondeData[i].refPressure;
                    tempCompiledMoment.refAirTemp += sondeSPData.sondeData[i].refAirTemp;
                    tempCompiledMoment.refHumidity += sondeSPData.sondeData[i].refHumidity;

                    tempCompiledMoment.sondeData.Pressure += sondeSPData.sondeData[i].sondeData.Pressure;
                    tempCompiledMoment.sondeData.Temperature += sondeSPData.sondeData[i].sondeData.Temperature;
                    tempCompiledMoment.sondeData.Humidity += sondeSPData.sondeData[i].sondeData.Humidity;
                }

                //Getting the average.
                tempCompiledMoment.refPressure = tempCompiledMoment.refPressure / sondeSPData.sondeData.Count;
                tempCompiledMoment.refAirTemp = tempCompiledMoment.refAirTemp / sondeSPData.sondeData.Count;
                tempCompiledMoment.refHumidity = tempCompiledMoment.refHumidity / sondeSPData.sondeData.Count;

                tempCompiledMoment.sondeData.Pressure = tempCompiledMoment.sondeData.Pressure / sondeSPData.sondeData.Count;
                tempCompiledMoment.sondeData.Temperature = tempCompiledMoment.sondeData.Temperature / sondeSPData.sondeData.Count;
                tempCompiledMoment.sondeData.Humidity = tempCompiledMoment.sondeData.Humidity / sondeSPData.sondeData.Count;

                //Adding consolidated date to total data.
                sondeSPData.sondeData.Clear();
                sondeSPData.sondeData.Add(tempCompiledMoment);

                //Running tests for pass fail on consolidated data.
                sondeSPData.sondeData[0].dataResults.pDiff = sondeSPData.sondeData[0].sondeData.Pressure - sondeSPData.sondeData[0].refPressure;
                sondeSPData.sondeData[0].dataResults.ATDiff = sondeSPData.sondeData[0].sondeData.Temperature - sondeSPData.sondeData[0].refAirTemp;
                sondeSPData.sondeData[0].dataResults.uDiff = sondeSPData.sondeData[0].sondeData.Humidity - sondeSPData.sondeData[0].refHumidity;

                sondeSPData.sondeData[0].dataResults.pressure = false;
                sondeSPData.sondeData[0].dataResults.airTemp = false;
                sondeSPData.sondeData[0].dataResults.humidity = false;

                if (Math.Abs(sondeSPData.sondeData[0].dataResults.pDiff) < sondeSPData.sondeData[0].currentPoint.tolPressure) { sondeSPData.sondeData[0].dataResults.pressure = true; }
                if (Math.Abs(sondeSPData.sondeData[0].dataResults.ATDiff) < sondeSPData.sondeData[0].currentPoint.tolAirTemp) { sondeSPData.sondeData[0].dataResults.airTemp = true; }
                if (Math.Abs(sondeSPData.sondeData[0].dataResults.uDiff) < sondeSPData.sondeData[0].currentPoint.tolHumidity) { sondeSPData.sondeData[0].dataResults.humidity = true; }

            }

            return radiosondeReportData;
        }

        public List<List<reportData>> processData(List<setPointData> dataToProcess)
        {
            List<List<reportData>> totalRadiosondeReportData = new List<List<reportData>>();

            foreach (setPointData sP in dataToProcess)  //All setpoint data.
            {
                List<reportData> radiosondeReportData = new List<reportData>();

                foreach (curActiveGroup aG in sP.groups) //All groups
                {
                    foreach (groupData gD in aG.conditionData) //All data collected at this setpoint for this group.
                    {
                        foreach (currentDataMoment cdm in gD.groupDataItem) //One record for one radiosonde of the data taken at the setpoint.
                        {
                            //Creating a filtered record for a radioosonde if it doesn't have one.
                            int sondeCount = 0;
                            foreach (reportData sonde in radiosondeReportData)
                            {
                                if (sonde.sondeID == cdm.sondeData.SondeID)
                                {
                                    sondeCount++;
                                }
                            }

                            reportData curSondeData;

                            //Selecting the radioosnde recored to use or create a new one if there is none.
                            if (sondeCount == 0)
                            {
                                curSondeData = new reportData();
                                curSondeData.sondeID = cdm.sondeData.SondeID;
                                curSondeData.sondeData = new List<currentDataMoment>();
                                curSondeData.sondeData.Add(cdm);


                                //Finding the runner and the pos.
                                foreach (runnerPressureAssembly.runnerPressure run in gD.group.runners)
                                {

                                    for (int sonde = 0; sonde < run.runnerRadiosondePorts.Length; sonde++)
                                    {
                                        if (run.runnerRadiosondePorts[sonde].CalData.SondeID == curSondeData.sondeID)
                                        {
                                            curSondeData.runner = run.runnerID;
                                            curSondeData.runnerPos = (sonde + 1).ToString();
                                        }
                                    }

                                }


                                radiosondeReportData.Add(curSondeData);

                            }
                            else
                            {
                                foreach (reportData sonde in radiosondeReportData)
                                {
                                    if (sonde.sondeID == cdm.sondeData.SondeID)
                                    {
                                        curSondeData = sonde;
                                        curSondeData.sondeData.Add(cdm);
                                    }
                                }
                            }
                        }

                    }
                }

                //Do some processing of data reduceing the data collecting at the setpoint for each radiosonde to one record.
                /*
                foreach (reportData sondeSPData in radiosondeReportData)
                {
                    currentDataMoment tempCompiledMoment = new currentDataMoment();
                    tempCompiledMoment = sondeSPData.sondeData[0];

                    for (int i = 1; i < sondeSPData.sondeData.Count; i++)
                    {
                        //Add all the relivent data into on object.
                        tempCompiledMoment.refPressure += sondeSPData.sondeData[i].refPressure;
                        tempCompiledMoment.refAirTemp += sondeSPData.sondeData[i].refAirTemp;
                        tempCompiledMoment.refHumidity += sondeSPData.sondeData[i].refHumidity;

                        tempCompiledMoment.sondeData.Pressure += sondeSPData.sondeData[i].sondeData.Pressure;
                        tempCompiledMoment.sondeData.Temperature += sondeSPData.sondeData[i].sondeData.Temperature;
                        tempCompiledMoment.sondeData.Humidity += sondeSPData.sondeData[i].sondeData.Humidity;
                    }

                    //Getting the average.
                    tempCompiledMoment.refPressure = tempCompiledMoment.refPressure / sondeSPData.sondeData.Count;
                    tempCompiledMoment.refAirTemp = tempCompiledMoment.refAirTemp / sondeSPData.sondeData.Count;
                    tempCompiledMoment.refHumidity = tempCompiledMoment.refHumidity / sondeSPData.sondeData.Count;

                    tempCompiledMoment.sondeData.Pressure = tempCompiledMoment.sondeData.Pressure / sondeSPData.sondeData.Count;
                    tempCompiledMoment.sondeData.Temperature = tempCompiledMoment.sondeData.Temperature / sondeSPData.sondeData.Count;
                    tempCompiledMoment.sondeData.Humidity = tempCompiledMoment.sondeData.Humidity / sondeSPData.sondeData.Count;

                    //Adding consolidated date to total data.
                    sondeSPData.sondeData.Clear();
                    sondeSPData.sondeData.Add(tempCompiledMoment);

                    //Running tests for pass fail on consolidated data.
                    sondeSPData.sondeData[0].dataResults.pDiff = sondeSPData.sondeData[0].sondeData.Pressure - sondeSPData.sondeData[0].refPressure;
                    sondeSPData.sondeData[0].dataResults.ATDiff = sondeSPData.sondeData[0].sondeData.Temperature - sondeSPData.sondeData[0].refAirTemp;
                    sondeSPData.sondeData[0].dataResults.uDiff = sondeSPData.sondeData[0].sondeData.Humidity - sondeSPData.sondeData[0].refHumidity;

                    sondeSPData.sondeData[0].dataResults.pressure = false;
                    sondeSPData.sondeData[0].dataResults.airTemp = false;
                    sondeSPData.sondeData[0].dataResults.humidity = false;

                    if (Math.Abs(sondeSPData.sondeData[0].dataResults.pDiff) < sondeSPData.sondeData[0].currentPoint.tolPressure) { sondeSPData.sondeData[0].dataResults.pressure = true; }
                    if (Math.Abs(sondeSPData.sondeData[0].dataResults.ATDiff) < sondeSPData.sondeData[0].currentPoint.tolAirTemp) { sondeSPData.sondeData[0].dataResults.airTemp = true; }
                    if (Math.Abs(sondeSPData.sondeData[0].dataResults.uDiff) < sondeSPData.sondeData[0].currentPoint.tolHumidity) { sondeSPData.sondeData[0].dataResults.humidity = true; }

                }



                */


                totalRadiosondeReportData.Add(radiosondeReportData);

            }

            return totalRadiosondeReportData;
        }

        public void generateReport(List<object> localList, string outputFile)
        {
            //Sorting Data
            List<object> sorted = sort(localList);

            //processing the data into unit of 4 runner per row.
            List<Logs.reportData> rpData = new List<Logs.reportData>();

            List<ValidateCalibration.reportData> curSP = (List<ValidateCalibration.reportData>)sorted[0];

            //Setting up each radiosonde
            foreach (ValidateCalibration.reportData rp in curSP)
            {
                Logs.reportData tempRD = new Logs.reportData();
                tempRD.SondeID = rp.sondeID;
                tempRD.Runner = rp.runner;
                tempRD.Pos = rp.runnerPos;
                tempRD.result = true;

                rpData.Add(tempRD);
            }

            //Looking for failed sondes.
            foreach (object spData in sorted)
            {
                curSP = (List<ValidateCalibration.reportData>)spData;

                foreach (ValidateCalibration.reportData valData in curSP)
                {
                    foreach (Logs.reportData rp in rpData)
                    {
                        if (rp.SondeID == valData.sondeID && rp.Runner == valData.runner)
                        {
                            if (!valData.sondeData[0].dataResults.airTemp || !valData.sondeData[0].dataResults.humidity || !valData.sondeData[0].dataResults.pressure)
                            {
                                rp.result = false;
                            }
                        }
                    }
                }

            }

            //Trying to get the data into it last form.

            List<string> runners = new List<string>();
            List<object> outputData = new List<object>();

            //combining the radiosondes into runner group objects.
            foreach (Logs.reportData rp in rpData)
            {
                if (!runners.Contains(rp.Runner))
                {
                    runners.Add(rp.Runner);
                }
            }

            foreach (string s in runners)
            {
                List<Logs.reportData> tempRunner = new List<Logs.reportData>();

                foreach (Logs.reportData rp in rpData)
                {
                    if (rp.Runner == s)
                    {
                        tempRunner.Add(rp);
                    }
                }

                outputData.Add((object)tempRunner);
            }


            //Breaking runners into groups of 4.
            List<object[]> outGoing = new List<object[]>();
            List<object> temp = new List<object>();

            for (int run = 0; run < outputData.Count; run++)
            {
                temp.Add(outputData[run]);


                if (temp.Count == 4)
                {
                    outGoing.Add(temp.ToArray());
                    temp = new List<object>();
                }

            }

            if (temp.Count > 0)
            {
                outGoing.Add(temp.ToArray());
            }

            

            Logs.LogDataHTML logHTML = new Logs.LogDataHTML("Pressure Verified", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            foreach (object[] dat in outGoing)
            {
                logHTML.addRowData(dat);
            }

            logHTML.finishFile();


            //Checking to make sure the file is writen as a HTML file.
            if (outputFile.Substring(outputFile.Length - 5, 5) != ".html")
            {
                outputFile = outputFile + ".html";
            }


            logHTML.saveFile(outputFile);
            
        }

        /// <summary>
        /// Method for sorting collected varified data into the correct possitional order.
        /// </summary>
        /// <param name="localList"></param>
        /// <returns></returns>
        public List<object> sort(List<object> localList)
        {

            //sort order.
            string[] runnerOrder = new string[24];
            runnerOrder[0] = "A1A1A1";
            runnerOrder[1] = "A1A1B1";
            runnerOrder[2] = "A1A2A1";
            runnerOrder[3] = "A1A2B1";
            runnerOrder[4] = "A1A3A1";
            runnerOrder[5] = "A1A3B1";
            runnerOrder[6] = "B1A1A1";
            runnerOrder[7] = "B1A1B1";
            runnerOrder[8] = "B1A2A1";
            runnerOrder[9] = "B1A2B1";
            runnerOrder[10] = "B1A3A1";
            runnerOrder[11] = "B1A3B1";
            runnerOrder[12] = "C1A1A1";
            runnerOrder[13] = "C1A1B1";
            runnerOrder[14] = "C1A2A1";
            runnerOrder[15] = "C1A2B1";
            runnerOrder[16] = "C1A3A1";
            runnerOrder[17] = "C1A3B1";
            runnerOrder[18] = "D1A1A1";
            runnerOrder[19] = "D1A1B1";
            runnerOrder[20] = "D1A2A1";
            runnerOrder[21] = "D1A2B1";
            runnerOrder[22] = "D1A3A1";
            runnerOrder[23] = "D1A3B1";


            List<object> sortedLocalList = new List<object>();

            foreach (object sp in localList)
            {
                List<ValidateCalibration.reportData> pointData = (List<ValidateCalibration.reportData>)sp;

                List<ValidateCalibration.reportData> pointSort = new List<ValidateCalibration.reportData>();

                List<string> checking = new List<string>();


                for (int order = 0; order < runnerOrder.Length; order++)
                {
                    foreach (ValidateCalibration.reportData data in pointData)
                    {
                        if (data.runner == runnerOrder[order])
                        {
                            pointSort.Add(data);
                        }
                    }
                }

                sortedLocalList.Add(pointSort);

            }

            return sortedLocalList;
        }

        private string decodeBool(bool state)
        {
            if (state)
            {
                return "Pass";
            }
            else
            {
                return "Fail";
            }
        }

        public void writeCSVData(List<object> localList, string outputFile)
        {

        }

        #region Methods triggered by events. These mainly to check for stability and alert in the event of error.

        //Use to check the enviroment air temp stability and run through the stability provess
        void dataPacketRecived_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {

        }

        //Used to check for pressure stability. Maybe??? not sure if needed. Pressure manager might be handleing this.
        void updatedPressureValue_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            pressureWait.Set(); //This will set true everytime there is a pressure data packet update.
        }

        // Pressure Manager Error Event
        void managerError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
        }

        void managerChamberError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {

        }

        void gotoPressureEvent_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            string incomingMessage = (string)data.NewValue;
            statusUpdate.UpdatingObject = (object)incomingMessage;
        }

        //Setting the wait when pressure conditions are ready.
        void pressureStabilityReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            pressureStabilityReady.Set();
        }

        void pressureConditionReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            pressureConditionsReady.Set();
        }

        //Setting chamber condtions as ready.
        void chamberStabilityReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            chamberStabilityReady.Set();
        }

        void chamberConditionReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            chamberConditionReady.Set();
        }

        void chamberTHReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            chamberTHReady.Set();
        }

        #endregion

        public void saveBin(string outFileName, object reportData)
        {
            //Bin Saving
            System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.Stream stream = new System.IO.FileStream(outFileName + ".bin", System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);
            formatter.Serialize(stream, reportData);
            stream.Close();
        }

    }

    /// <summary>
    /// Data object for radiosondes and referances with included time tag.
    /// </summary>
    [Serializable]
    public struct currentDataMoment
    {
        public DateTime dataPointDateTime;
        public ipRadiosonde.CalData sondeData;
        public double refPressure;
        public double refAirTemp;
        public double refHumidity;
        public validatePoint currentPoint;
        public results dataResults;
  
    }

    [Serializable]
    public class results
    {
        public bool pressure;
        public bool airTemp;
        public bool humidity;

        public double pDiff;
        public double ATDiff;
        public double uDiff;
    }

    [Serializable]
    public struct groupData
    {
        public List<currentDataMoment> groupDataItem;
        public Calibration.runnerGroup group;
    }

    [Serializable]
    public struct curActiveGroup
    {
        public List<groupData> conditionData;
    }

    [Serializable]
    public struct setPointData
    {
        public List<curActiveGroup> groups;
    }

    /// <summary>
    /// Validate point information object.
    /// </summary>
    [Serializable]
    public class validatePoint
    {
        //Calibration point.
        public int index;

        //Air Temp
        public double airTempSetPoint;
        public double airTempLower;
        public double airTempUpper;
        public int packetCountAT;

        //Air Temp Theratal... 
        public double airTHSetPoint;
        public double airTHLower;
        public double airTHUpper;
        public int packetCountTH;

        //Pressure Point
        public double pressureSetPoint;
        public double pressureLower;
        public double pressureUpper;
        public int packetCountPressure;

        public double pressureStabilitySetPoint;
        public double pressureStabilityLower;
        public double pressureStabilityUpper;
        public int packetCountPressureStability;

        //Humidity Point
        public double humidtySetPoint;
        public double humidityLower;
        public double humidityUpper;
        public int packetCountHumidity;

        //Tolerance setpoints
        public double tolPressure;
        public double tolAirTemp;
        public double tolHumidity;

    }

    [Serializable]
    public struct reportData
    {
        public string sondeID;
        public string runner;
        public string runnerPos;
        public List<currentDataMoment> sondeData;
    }
}
