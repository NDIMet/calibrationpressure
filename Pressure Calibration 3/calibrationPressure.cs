﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calibration
{
    /// <summary>
    /// This class contains the methods for running the calibration.
    /// </summary>
    public class calibrationPressure
    {

        //Public veriables

        //Background workers for the several calibration tasks.
        //This worker will start the process off. The worker will do the following.
        //Start the chamber in a very close to current air temp and presure around 600mBs and check all
        //runners for leaks and build aceptable runner leak range groups.
        public System.ComponentModel.BackgroundWorker backgroundWorkerPreCalibrationWorker;

        //This worker will doing a move the calibration along from point to point and wait until
        //the conditions are met. Once the conditions are met then data will be collected.
        public System.ComponentModel.BackgroundWorker backgroundWorkerCalibration;

        //This worker runs after the calibration has ended and runs it data through the
        //coef maker then loads it to the radiosonde.
        public System.ComponentModel.BackgroundWorker backgroundWorkerPostCalibration;

        //Event that lets me konw that preCalWorker is done.
        public updateCreater.objectUpdate preCalComplete = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate statusUpdate = new updateCreater.objectUpdate();

        //Calibration events.
        public updateCreater.objectUpdate calibrationComplete = new updateCreater.objectUpdate();
        public updateCreater.objectUpdate manifoldGoupChanged = new updateCreater.objectUpdate();
        
        //Error event. Lets the program know something has gone wrong.
        public updateCreater.objectUpdate calibrationError = new updateCreater.objectUpdate();


        public List<calibraitonPoint> calPoints; //Calibration points.

        public managerChamber.chamberManager managerChamber; //The stability manager for the chamber

        //Groups that leak less then .006 and can be calibrated together.
        public List<runnerGroup> calibrationGroups = new List<runnerGroup>();

        /////////////////////////////////////////////////////
        //Private Veriables
        ////////////////////////////////////////////////////

        //Calibration equipment
        private ManagerPresssure.pressureManager managerPressure; //Manager for pressure control
        private TestEquipment.Paroscientific sensorPressure; //Pressure Sensor that will be used for the calibration
        private TestEquipment.Thermotron chamber; //Chamber that will be used in calibration.
        
        private rackCalibration.rackPressure[] racks;
        private RelayController.relayControllerSystem[] relayPresureManifold;

        //Radiosonde runner boards location on the system.
        //private runnerPressureAssembly.runnerPressure[] runner; /////////////////// Might just remove... Not sure right now.


        ///Veriables relating directly to the current calibration
        ///Things like is there a point trying to be achived.
        private bool calibrationUnderWay = false;
        private bool loggingData = false; //Might need to be moved to a logging class but for now here it is.
        private int currentCalibrationStep = -1;
        private DateTime startOfCalibration;
        private int groupInCal = -1;
        private bool errorMode = false;


        //Auto Reset events to prevent process from moving forward until conditions are meet.
        private System.Threading.AutoResetEvent pressureConditionsReady = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent pressureStabilityReady = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent pressureWait = new System.Threading.AutoResetEvent(false);

        private System.Threading.AutoResetEvent chamberConditionReady = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent chamberStabilityReady = new System.Threading.AutoResetEvent(false);
        private System.Threading.AutoResetEvent chamberTHReady = new System.Threading.AutoResetEvent(false);
       

        //Data Logging location
        string calibrationLogLOC = "";

        /// <summary>
        /// Method starts up the Pressure calibration object.
        /// </summary>
        /// <param name="calPressureSensor"></param>
        /// <param name="calChamber"></param>
        public calibrationPressure(TestEquipment.Paroscientific calPressureSensor, ManagerPresssure.pressureManager calPressureManager, TestEquipment.Thermotron calChamber, rackCalibration.rackPressure[] incomingRacks, RelayController.relayControllerSystem[] incomingRelayManifolds)
        {
            //Applying the equipment to the calibration
            sensorPressure = calPressureSensor;
            managerPressure = calPressureManager;
            chamber = calChamber;
            racks = incomingRacks;
            relayPresureManifold = incomingRelayManifolds;

            //Connecting to pressure managers events
            managerPressure.gotoPressureEvent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(gotoPressureEvent_PropertyChange);
            managerPressure.managerError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(managerError_PropertyChange);
            managerPressure.pressureConditionReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(pressureConditionReady_PropertyChange);
            managerPressure.pressureStabilityReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(pressureStabilityReady_PropertyChange);

            //Connecting to the Pressure sensor
            sensorPressure.updatedPressureValue.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(updatedPressureValue_PropertyChange);

            //Connecting to chamber data.
            configureChamberManager();

        }

        public void Start(string mode, List<Calibration.calibraitonPoint> desiredCalPoints, string logLOC)
        {
            //Setting up the pre-calibration worker
            backgroundWorkerPreCalibrationWorker = new System.ComponentModel.BackgroundWorker();
            backgroundWorkerPreCalibrationWorker.WorkerSupportsCancellation = true;
            backgroundWorkerPreCalibrationWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerPreCalibrationWorker_DoWork);
            backgroundWorkerPreCalibrationWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorkerPreCalibrationWorker_RunWorkerCompleted);

            calPoints = desiredCalPoints; //Setting the cal points for the calibration.
            calibrationLogLOC = logLOC; //Setting the start logging loc.
            startOfCalibration = DateTime.Now;

            backgroundWorkerPreCalibrationWorker.RunWorkerAsync(mode);
        }

        private void configureChamberManager()
        {
            if (managerChamber != null)
            {
                managerChamber.chamberConditionReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(chamberConditionReady_PropertyChange);
                managerChamber.chamberStabilityReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(chamberStabilityReady_PropertyChange);
                managerChamber.chamberTHReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(chamberTHReady_PropertyChange);
                managerChamber.managerError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(managerChamberError_PropertyChange);
                managerChamber = null;

                statusUpdate.UpdatingObject = (object)"Calibration Process Error: Restarting managerChamber";

                System.Threading.Thread.Sleep(1000);
            }

            managerChamber = new managerChamber.chamberManager(chamber);
            managerChamber.chamberConditionReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(chamberConditionReady_PropertyChange);
            managerChamber.chamberStabilityReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(chamberStabilityReady_PropertyChange);
            managerChamber.chamberTHReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(chamberTHReady_PropertyChange);
            managerChamber.managerError.PropertyChange +=new updateCreater.objectUpdate.PropertyChangeHandler(managerChamberError_PropertyChange);

            //Setting some defaults
            //Need to setup the chamber stability stuff.
            if (!calibrationUnderWay)
            {
                managerChamber.setDesiredEnviroment(-20, 50, 30, 60);
                managerChamber.setDesiredEnviromentTh(-100, 100, 0, 60);
                managerChamber.setDesiredEnviromentStability(-0.048, 0.048, 0, 90); //Setting chamber stability settings. This might need to be moved to a file.
            }
            else
            {
                managerChamber.setDesiredEnviroment(calPoints[currentCalibrationStep].airTempLower, calPoints[currentCalibrationStep].airTempUpper, calPoints[currentCalibrationStep].airTempSetPoint, calPoints[currentCalibrationStep].packetCountAT);
                managerChamber.setDesiredEnviromentTh(calPoints[currentCalibrationStep].airTHLower, calPoints[currentCalibrationStep].airTHUpper, calPoints[currentCalibrationStep].airTHSetPoint, calPoints[currentCalibrationStep].packetCountTH);
                managerChamber.setDesiredEnviromentStability(-0.048, 0.048, 0, 90); //Setting chamber stability settings. This might need to be moved to a file.
            }
        }


        /// <summary>
        /// Stops all the current running background Workers.
        /// </summary>
        public void Stop()
        {
            //Stopping the preCal if running.
            if (backgroundWorkerPreCalibrationWorker != null)
            {
                if (backgroundWorkerPreCalibrationWorker.IsBusy)
                {
                    backgroundWorkerPreCalibrationWorker.CancelAsync();
                }
            }

            //Stopping the current calibraiton if running
            if (backgroundWorkerCalibration != null)
            {
                if (backgroundWorkerCalibration.IsBusy)
                {
                    backgroundWorkerCalibration.CancelAsync();
                }
            }

            //Stopping the post calibration if running.
            if (backgroundWorkerPostCalibration != null)
            {
                if (backgroundWorkerPostCalibration.IsBusy)
                {
                    backgroundWorkerPostCalibration.CancelAsync();
                }
            }
        }

        /// <summary>
        /// Method return the calibration statis in a object array.
        /// </summary>
        /// <returns></returns>
        public object[] getCalStatus()
        {
            object[] statusData = new object[4] { calibrationUnderWay, loggingData, currentCalibrationStep, startOfCalibration };
            return statusData;
        }

        #region Background Workers the calibration

        #region Pre-Cal background worker

        void backgroundWorkerPreCalibrationWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            string mode = (string)e.Argument;

            int preCalStabilityCount = 15;
            
            //Pre run checkout.
            //Set Chamber to the current air temp reading and start chamber.
            chamber.setChamberTemp(chamber.CurrentChamberC);
            System.Threading.Thread.Sleep(1000);
//            chamber.startChamberManualMode();

            //Checking all radiosondes to make sure there is some movement of the sensors connect to the system. This will keep broken radiosondes from messing with the pre-cal sort.


            
            //Move the current system pressure to something safe yet low enough pressure for testing. Say 300mB
            managerPressure.setDesiredPressureEnviroment(225, 275, 250, 15); //rough setpoints.
            managerPressure.setDesiredPressureStability(-.010, .010, 0, preCalStabilityCount);
            managerPressure.gotoPressure(2,250);
            statusUpdate.UpdatingObject = (object)"Setting Pressure Manager to test point";

            //Needs to wait until the system becomes stable.
            //Something like a wait one,,, maybe with some kind of fail option.


            pressureStabilityReady.WaitOne();
            /* Removing this code. I was hoping to provent errors, but this just causes more.
            if(!pressureStabilityReady.WaitOne(600000))
            {
                statusUpdate.UpdatingObject = (object)"Time Out is waiting for stability";
                return;
            }
            */

            //Checking to see if the radiosondes are good.




            //Need to test all connections and look for pressure changes on a runner to assisoiate the runner to the manifold position.
            //Collect data from the radiosodnes to compare any changes to.
            List<ipRadiosonde.CalData> beforReadings;
            beforReadings = getAllCaldata(); //These readings are the current stable state.

            List<string> manResults = new List<string>(); //The results of the manifold testing.
            
            //Index through all the runner manifolds looking to connect manifold ports with runners.
            for (int r = 0; r < relayPresureManifold.Length; r++) //Indexing manifold controllers.
            {
                //Need some shutdown safe code here.

                string[] tempRelayFunction = relayPresureManifold[r].RelayFunction(); //Gettin the functions of the relays.
                for (int trf = 0; trf < tempRelayFunction.Length; trf++) //Going through the relay functions.
                {

                    //Need to read stable "normal" unexposed
                    //Resetting the stability waitone
                    pressureStabilityReady = new System.Threading.AutoResetEvent(false);
                    //Waiting for pressure to become stable. //This will prevent false,,,, false. <--- Hate this note.
                    if (!pressureStabilityReady.WaitOne(300000))
                    {
                        //Something went wrong... This needs to go into some kind of error thing, not just stopping the worker.
                        statusUpdate.UpdatingObject = (object)"Time Out is waiting for stability";
                        return;
                    }

                    //A little break to make sure the venting is complete.
                    System.Threading.Thread.Sleep(2000);

                    //Clearing all old radiosonde data.
                    beforReadings.Clear();
                    beforReadings = getAllCaldata(); //Reloading the data.



                    //////////////////////////////////////////////////
                    //Need something here to protect agenst the pressure sensor taking something like 5 sec+ to read.
                    //////////////////////////////////////////




                    if (tempRelayFunction[trf] != "" && tempRelayFunction[trf] != null)
                    {
                        statusUpdate.UpdatingObject = (object)("Testing: " + tempRelayFunction[trf]);

                        double beforePressure = sensorPressure.getCurrentPressure(); //Getting the current pressure to compare.
                        relayPresureManifold[r].commandRelayBoard(tempRelayFunction[trf], true); //Opening the pressure port.
                        
                        //Rough check to see if pressure has changed. If the change is greater then 5 mBar then the connection is open to the air.
                        //However is there is very little change then the port is blocked.
                        System.Threading.Thread.Sleep(3000); //A little pause to allow for some change.
                        double pressureNow = sensorPressure.getCurrentPressure();

                        if (Math.Abs(beforePressure - pressureNow) > 7) //Checking to see if anything is connected.
                        {
                            relayPresureManifold[r].commandRelayBoard(tempRelayFunction[trf], false); //Closing port. Nothing is connected.
                            manResults.Add(tempRelayFunction[trf].ToString() + "," + "Open - " + Math.Abs(beforePressure - pressureNow).ToString());
                            statusUpdate.UpdatingObject = (object)(tempRelayFunction[trf] + "," + " Open");

                            //After the system has been opened like this I want to wait for it to settle.
                            managerPressure.setDesiredPressureStability(-.010, .010, 0, preCalStabilityCount);
                            pressureStabilityReady.WaitOne();

                        }
                        else
                        {
                            if (Math.Abs(beforePressure - pressureNow) < 1) //If there is very little change then most likly the port is blocked.
                            {
                                relayPresureManifold[r].commandRelayBoard(tempRelayFunction[trf], false); //Closing port. Nothing is connected.
                                manResults.Add(tempRelayFunction[trf].ToString() + "," + "Blocked - " + Math.Abs(beforePressure - pressureNow).ToString());
                                statusUpdate.UpdatingObject = (object)(tempRelayFunction[trf] + "," + " Blocked");
                            }
                            else //Where we now collect data again and look for the radiosondes that changed and figure out where they are connected.
                            {

                                statusUpdate.UpdatingObject = (object)"Runner Found";

                                //Find out what one's have changed.
                                System.Threading.Thread.Sleep(4000); //This lets the systems stable to get good readings.
                                List<ipRadiosonde.CalData> currentState = getAllCaldata();
                                List<string> changeList = new List<string>(); //What Sn's have changed. This will be used to compare and look for the manifold.


                                foreach (ipRadiosonde.CalData data in beforReadings)
                                {
                                    for (int index = 0; index < currentState.Count; index++)
                                    {
                                        //Locking on to the same radisonde to do the compare.
                                        if (data.SerialNumber == currentState[index].SerialNumber)
                                        {
                                            if (data.PressureCounts - currentState[index].PressureCounts > 10000)
                                            {
                                                changeList.Add(data.SerialNumber); //Adding the found to the list.
                                                statusUpdate.UpdatingObject = (object)("Radiosonde there: " + data.SerialNumber + "," + data.PressureCounts.ToString() + "," + currentState[index].PressureCounts.ToString());

                                            }
                                            else
                                            {
                                                statusUpdate.UpdatingObject = (object)("Radiosonde Problem: " + data.SerialNumber + "," + data.PressureCounts.ToString() + "," + currentState[index].PressureCounts.ToString());
                                            }
                                        }
                                    }
                                }

                                //Now that I know what ones have changed I will find out what manifold they are on.
                                List<string[]> whereAreThey = new List<string[]>();
                                foreach (string sn in changeList)
                                {
                                    string[] loc = findRadiosondeLOC(sn);
                                    statusUpdate.UpdatingObject = loc[0] + "," + loc[1];
                                    whereAreThey.Add(loc);
                                }

                                //Checking to see if the first one matches all of the others.
                                bool sameLOC = false;
                                string[] testingChar = whereAreThey[0];
                                for (int chk = 0; chk < whereAreThey.Count; chk++)
                                {
                                    string[] whatIsBeingTested = whereAreThey[chk];
                                    if (testingChar[0] == whatIsBeingTested[0] && testingChar[1] == whatIsBeingTested[1])
                                    {
                                        sameLOC = true;
                                    }
                                }

                                //Debug
                                statusUpdate.UpdatingObject = (object)("Same LOC reports: " + sameLOC.ToString());
                                //Getting the average leek. 
                                //Resetting the stability waitone
                                pressureStabilityReady = new System.Threading.AutoResetEvent(false);
                                //Waiting for pressure to become stable. //This will prevent false,,,, false. <--- Hate this note.
                                if (!pressureStabilityReady.WaitOne(300000))
                                {
                                    //Something went wrong... This needs to go into some kind of error thing, not just stopping the worker.
                                    statusUpdate.UpdatingObject = (object)"Time Out is waiting for stability";
                                    SystemException error = new SystemException("Pre-Calibration Error," + tempRelayFunction[trf] + ",Runner unable to become stable.");
                                    calibrationError.UpdatingObject = (object)error;
                                    manResults.Add(tempRelayFunction[trf].ToString() + "," + "Blocked - " + "unstable");

                                }
                                else
                                {

                                    int perCount = 0;
                                    List<double> pressureAvgData = new List<double>();
                                    while (perCount < 7)
                                    {
                                        //Debug
                                        statusUpdate.UpdatingObject = (object)("Waiting for pressure Data");
                                        pressureWait.WaitOne();
                                        pressureAvgData.Add(managerPressure.getPressureDifferancePerSec());
                                        statusUpdate.UpdatingObject = (object)("Pressure Data Collected: " + perCount.ToString() + " - " + managerPressure.getPressureDifferancePerSec().ToString("0.0000"));
                                        perCount++;
                                    }

                                    //Now I know what runner the radiosondes are on and what manifold port I am connected to I am storing the manifold port information into the runner.
                                    foreach (rackCalibration.rackPressure ra in racks)
                                    {
                                        foreach (runnerPressureAssembly.runnerPressure run in ra.runners)
                                        {
                                            if (run.runnerID == testingChar[1])
                                            {
                                                //Getting the manifold avg.
                                                double leakAvg = 0;
                                                foreach (double reading in pressureAvgData)
                                                {
                                                    leakAvg += reading;
                                                }

                                                leakAvg = leakAvg / pressureAvgData.Count;
                                                statusUpdate.UpdatingObject = (object)("Manual Calc: " + leakAvg.ToString("0.0000"));
                                                statusUpdate.UpdatingObject = (object)("Auto Calc: " + pressureAvgData.Average().ToString("0.0000"));

                                                run.manifoldPos = tempRelayFunction[trf];
                                                run.currentPressureStabilityAvg = leakAvg; //storing avg leak.

                                                //If leak is greater then .003 then setting it's leak flag to true.
                                                if (pressureAvgData.Average() > 0.003)
                                                {
                                                    run.leakStatus = true;
                                                }

                                                //Debug
                                                statusUpdate.UpdatingObject = (object)(tempRelayFunction[trf] + " = " + pressureAvgData.Average().ToString("0.0000") + " - " + run.leakStatus.ToString());
                                            }
                                        }
                                    }

                                    manResults.Add(tempRelayFunction[trf].ToString() + "," + testingChar[0] + "," + testingChar[1] + "," + pressureAvgData.Average().ToString());
                                    relayPresureManifold[r].commandRelayBoard(tempRelayFunction[trf], false); //Closing manifold port.
                                }
                            
                            }


                        }

                        System.Threading.Thread.Sleep(2000); //Taking a little pause to avoid mis-reads from pressure bounce.

                    }
                }
            }



            //Sending out the report.
            foreach (string report in manResults)
            {
                statusUpdate.UpdatingObject = (object)report;
            }

            //Go through the runners and look for runners that could be combined and yet have a leek rate lower then .006 mB/Sec
            
            

            List<runnerPressureAssembly.runnerPressure> allRunners = new List<runnerPressureAssembly.runnerPressure>();
            //Compiled all the runners into a list.
            foreach (rackCalibration.rackPressure r in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure run in r.runners)
                {
                    if (run.manifoldPos.Contains("port")) //This limits runners that have not been found connected to a port connection.
                    {
                        allRunners.Add(run);
                    }
                }
            }

            //Compile those runners into a group.
            while (allRunners.Count > 0)
            {

                List<runnerPressureAssembly.runnerPressure> tempRunners = new List<runnerPressureAssembly.runnerPressure>();
                runnerGroup tempGroup = new runnerGroup();
                double currentAddedRate = 0;


                //Need to provent unused runners from being check. 


                for (int ind = 0; ind<allRunners.Count; ind++)
                {
                    statusUpdate.UpdatingObject = (object)("Current Group Leak Total: " + currentAddedRate.ToString("0.00000"));
                    if (currentAddedRate + Math.Abs(allRunners[ind].currentPressureStabilityAvg) < 0.006 || tempGroup.runners.Count == 0)
                    {
                        tempGroup.runners.Add(allRunners[ind]);
                        statusUpdate.UpdatingObject = (object)("Adding " + allRunners[ind].runnerID + " To group");
                        currentAddedRate = currentAddedRate + Math.Abs(allRunners[ind].currentPressureStabilityAvg);
                    }
                    else
                    {
                        statusUpdate.UpdatingObject = (object)"Breaking from loop";
                        break;
                    }

                    //Limiting the amount of runners allowed in a group.
                    if (tempGroup.runners.Count == 4)
                    {
                        statusUpdate.UpdatingObject = (object)"Max allowable runners reached.";
                        break;
                    }

                    
                }

                calibrationGroups.Add(tempGroup); //Adding the temp group the the master list.
                statusUpdate.UpdatingObject = (object)("Current Group Leak Total: " + currentAddedRate.ToString("0.00000"));

                //Removing grouped runners from the list of avaiable before we loop and go looking again.
                foreach (runnerGroup audit in calibrationGroups)
                {
                    foreach (runnerPressureAssembly.runnerPressure run in audit.runners)
                    {
                        if (allRunners.Contains(run))
                        {
                            allRunners.Remove(run);
                            statusUpdate.UpdatingObject = (object)(run.runnerID + " All ready used removing from list.");
                        }
                    }
                }

            }

            //Report final checkout/pre-cal results.
            //If this is a check then raise event that pre-cal has ended and shutdown.


            //If this is a real calibration then spin up the calibration backgroundWorker.
            //Mode running in.
            statusUpdate.UpdatingObject = (object)mode;
            e.Result = (object)mode;

        }

        void backgroundWorkerPreCalibrationWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                statusUpdate.UpdatingObject = e.Error.Message;

                //Needs to close any open manifild ports.
                foreach (RelayController.relayControllerSystem re in relayPresureManifold)
                {
                    string[] relayFunctions = re.RelayFunction();
                    foreach (string relay in relayFunctions)
                    {
                        re.commandRelayBoard(relay, false);
                    }
                }
                 
            }

            string mode = (string)e.Result;

            switch (mode)
            {
                case "calibration": //If calibration mode spinning up the calibration worker and starting it.
                    backgroundWorkerCalibration = new System.ComponentModel.BackgroundWorker();
                    backgroundWorkerCalibration.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorkerCalibration_DoWork);
                    backgroundWorkerCalibration.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(backgroundWorkerCalibration_RunWorkerCompleted);
                    backgroundWorkerCalibration.WorkerSupportsCancellation = true;
                    currentCalibrationStep = 0; //Setting the calibration to the first step.
                    backgroundWorkerCalibration.RunWorkerAsync();
                    break;

                case"test":
                    chamber.stopChamberManualMode();
                    break;

                case"retest": //This is a test creeated for retesting the runner groups.
                    groupInCal = 0;
                    System.Threading.ThreadStart startHere = new System.Threading.ThreadStart(startGroupRetest);
                    System.Threading.Thread retest = new System.Threading.Thread(startHere);
                    retest.Start();

                    break;
            }

            //Trigger end of precal end event and send out report object.
            preCalComplete.UpdatingObject = (object)mode;
        }

        #endregion

        #region Calibration background worker.

        void backgroundWorkerCalibration_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            calibrationUnderWay = true;

            //Create logging dir and start files.
            setupLogLOC();

            statusUpdate.UpdatingObject = (object)("Starting Calibration");

            //Writing a log of radiosondes to group information to be used to sort out file information.
            writeGroupLog();

            //Covering my bases


            while (!backgroundWorkerCalibration.CancellationPending && calPoints.Count > currentCalibrationStep) //Main loop. Stops if ordered to or the calibration reaches past the last point.
            {

                //Needs to break out after exceding the index.
                if (chamber.CurrentChamberSPC != calPoints[currentCalibrationStep].airTempSetPoint || currentCalibrationStep == 0) //Changing and resetting the chamber air temp if different then current.
                {
                    //Need to setup the chamber stability stuff.
                    managerChamber.setDesiredEnviroment(calPoints[currentCalibrationStep].airTempLower, calPoints[currentCalibrationStep].airTempUpper, calPoints[currentCalibrationStep].airTempSetPoint, calPoints[currentCalibrationStep].packetCountAT);
                    managerChamber.setDesiredEnviromentTh(calPoints[currentCalibrationStep].airTHLower, calPoints[currentCalibrationStep].airTHUpper, calPoints[currentCalibrationStep].airTHSetPoint, calPoints[currentCalibrationStep].packetCountTH);
                    managerChamber.setDesiredEnviromentStability(-0.048, 0.048, 0, 90); //Setting chamber stability settings. This might need to be moved to a file.
                    statusUpdate.UpdatingObject = (object)("Chamber set to: " + calPoints[currentCalibrationStep].airTempSetPoint.ToString());

                    //Move pressure to somewhere safe for temp change.
                    statusUpdate.UpdatingObject = (object)("Moving pressure to 700 mBar for tempature change.");
					// Setting manager to prevent hang up.
                    managerPressure.setDesiredPressureEnviroment(600, 800, 700, 30);
					// Now go to pressure.
                    managerPressure.gotoPressure(2,700);

                    //Setting the chamber enviroments set point
                    chamber.setChamberTemp(calPoints[currentCalibrationStep].airTempSetPoint);
                    System.Threading.Thread.Sleep(1000);
                    chamber.startChamberManualMode(); //Starting the chamber.

                    //Resetting chamber TH reset event.
                    chamberTHReady = new System.Threading.AutoResetEvent(false);

                    //Resetting the chamber condition ready set.
                    chamberConditionReady = new System.Threading.AutoResetEvent(false);
                    statusUpdate.UpdatingObject = (object)("Waiting for chamber to become stable.");
                    chamberConditionReady.WaitOne();
                    statusUpdate.UpdatingObject = (object)("Chamber Conditions Ready.");


                    //Waiting for chamber to become stable.
                    //I think I want a time out here,,, or something that checks to make sure the system is running so we don't get caught here.
                    statusUpdate.UpdatingObject = (object)("Waiting for chamber TH to become stable.");
                    chamberTHReady.WaitOne();
                    statusUpdate.UpdatingObject = (object)("Chamber TH Conditions Ready.");

                    chamberStabilityReady = new System.Threading.AutoResetEvent(false); //Setting the system to check for stability after we know the enviroment has been matched.
                    chamberStabilityReady.WaitOne();
                    statusUpdate.UpdatingObject = (object)("Chamber Stability Ready.");

                }

                //Set pressure conditions and stability settings.
                statusUpdate.UpdatingObject = (object)(currentCalibrationStep.ToString() + "," + calPoints[currentCalibrationStep].pressureLower.ToString() + "," + calPoints[currentCalibrationStep].pressureUpper.ToString() + "," + calPoints[currentCalibrationStep].pressureSetPoint.ToString() + "," +
                    calPoints[currentCalibrationStep].packetCountPressure.ToString());


                managerPressure.setDesiredPressureEnviroment(calPoints[currentCalibrationStep].pressureLower, calPoints[currentCalibrationStep].pressureUpper, calPoints[currentCalibrationStep].pressureSetPoint,
                    calPoints[currentCalibrationStep].packetCountPressure);
                managerPressure.setDesiredPressureStability(calPoints[currentCalibrationStep].pressureStabilityLower, calPoints[currentCalibrationStep].pressureStabilityUpper, calPoints[currentCalibrationStep].pressureStabilitySetPoint,
                    calPoints[currentCalibrationStep].packetCountPressureStability);


                //Loop through the runnerGroups.
                int countGroup = 0;
                foreach (runnerGroup group in calibrationGroups)
                {
                    //Trigger the current manifold being worker on event.
                    groupInCal = countGroup;
                    manifoldGoupChanged.UpdatingObject = (object)countGroup;

                    //Opening group manifolds.
                    statusUpdate.UpdatingObject = (object)("Opening the goup manifold ports.");
                    foreach (runnerPressureAssembly.runnerPressure run in group.runners)
                    {
                        foreach (RelayController.relayControllerSystem rm in relayPresureManifold)
                        {
                            if (rm.checkValidRequest(run.manifoldPos))
                            {
                                rm.commandRelayBoard(run.manifoldPos, true); //Opening the manifolds.
                            }
                        }
                    }



                    //Waiting for the system to recoil
                    System.Threading.Thread.Sleep(2000); //Could be less be we are starting here.

                    //Moving the system to pressure.
                    statusUpdate.UpdatingObject = (object)("Setting the group to the desired pressure.");
                    managerPressure.gotoPressure(6, calPoints[currentCalibrationStep].pressureSetPoint); //Moving the system to the desired pressure.

                    //Resetting that waiting for pressure conditions and stability to be ready.
                    resetAndWaitForPressure();
                    managerPressure.stopGoToPressure(); //This will stop a pressure move if we are ready to collect data. Just in case.

                    //Logging data.
                    List<object[]> currentCalStepData = new List<object[]>();
                    int recordCount = 0;
                    statusUpdate.UpdatingObject = (object)("Loading data.");
                    while (recordCount < 25 && !backgroundWorkerCalibration.CancellationPending) //Log the calibration data until packet count or cancel.
                    {
                        //Waiting for new pressure data.
                        pressureWait.WaitOne();

                        //collecting the data.
                        List<object[]> temp = collectCalibrationData();
                        foreach (object[] data in temp)
                        {
                            currentCalStepData.Add(data);
                        }

                        statusUpdate.UpdatingObject = (object)("Logging: " + recordCount.ToString());

                        recordCount++;
                    }

                    //Write data to hard drive.
                    statusUpdate.UpdatingObject = (object)("Writing data to files.");
                    writeDataToFile(countGroup.ToString(), currentCalStepData);                 

                    //Close manifold ports.
                    statusUpdate.UpdatingObject = (object)("Closing the group manifold ports.");
                    foreach (runnerPressureAssembly.runnerPressure run in group.runners)
                    {
                        foreach (RelayController.relayControllerSystem rm in relayPresureManifold)
                        {
                            if (rm.checkValidRequest(run.manifoldPos))
                            {
                                rm.commandRelayBoard(run.manifoldPos, false); //Closing the manifolds.
                            }
                        }
                    }


                    countGroup++;
                }

                statusUpdate.UpdatingObject = (object)("Moving to next calibration step.");
                currentCalibrationStep++; //Moving to the next calibration steps.
            }

        }

        void backgroundWorkerCalibration_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            //If there is an error.
            if (e.Error != null)
            {
                statusUpdate.UpdatingObject = "Calibration Error: " + e.Error.Message;

                //Needs to close any open manifild ports.
                foreach (RelayController.relayControllerSystem re in relayPresureManifold)
                {
                    string[] relayFunctions = re.RelayFunction();
                    foreach (string relay in relayFunctions)
                    {
                        re.commandRelayBoard(relay, false);
                    }
                }
            }

            calibrationUnderWay = false;
            chamber.stopChamberManualMode(); //Stopping chamber.


            //Triggering an event to indicate the calibration has ended.
            calibrationComplete.UpdatingObject = (object)calibrationLogLOC;

            //Loading group information.

            //Loading coefs to radiosondes.

            //Checking the radiosondes output to current Pressure Referacne.

            //Generate report.

            //Alert users that calibration has ended.
        }

        private void resetAndWaitForPressure()
        {
            restartWait:
            //Resetting the auto reset events.
            pressureConditionsReady = new System.Threading.AutoResetEvent(false);
            pressureStabilityReady = new System.Threading.AutoResetEvent(false);

            //Waiting for the pressure to become in range and stable.
            pressureConditionsReady.WaitOne();

            //Waiting for pressure condition to stabilize. If it doesn't in less then 10 min then the group needs to be rechecked for leaks.
            if(!pressureStabilityReady.WaitOne(600000) && !errorMode) //The recheck will only happen when the calibration is running.
            {
                startGroupRetest();
                goto restartWait; //Resetting and waiting for stabliziation.
            }
        }

        private void startGroupRetest()
        {
            statusUpdate.UpdatingObject = (object)("Current Calibration group failed to stabilize. Running recheck.");
            errorMode = true;

            //Checking group.
            recheckGroup(groupInCal);

            //Reset the pressure conditions to the prevouis desired for calibration.
            if (calibrationUnderWay)
            {
                managerPressure.setDesiredPressureEnviroment(calPoints[currentCalibrationStep].pressureLower, calPoints[currentCalibrationStep].pressureUpper, calPoints[currentCalibrationStep].pressureSetPoint,
                    calPoints[currentCalibrationStep].packetCountPressure);
                managerPressure.setDesiredPressureStability(calPoints[currentCalibrationStep].pressureStabilityLower, calPoints[currentCalibrationStep].pressureStabilityUpper, calPoints[currentCalibrationStep].pressureStabilitySetPoint,
                    calPoints[currentCalibrationStep].packetCountPressureStability);
                statusUpdate.UpdatingObject = (object)("Setting the group to the desired pressure.");
                managerPressure.gotoPressure(6, calPoints[currentCalibrationStep].pressureSetPoint); //Moving the system to the desired pressure.
            }

            //Clearing errorMode.
            errorMode = false;

            statusUpdate.UpdatingObject = (object)("Retest complete");
        }

        private void recheckGroup(int group)
        {
            //Closing all runner pressure manifolds.
            for (int r = 0; r < relayPresureManifold.Length; r++) //Indexing manifold controllers.
            {

                string[] tempRelayFunction = relayPresureManifold[r].RelayFunction(); //Gettin the functions of the relays.
                for (int trf = 0; trf < tempRelayFunction.Length; trf++) //Going through the relay functions.
                {
                    relayPresureManifold[r].commandRelayBoard(tempRelayFunction[trf], false);
                }
            }

            //Move the current system pressure to something safe yet low enough pressure for testing. Say 300mB
            managerPressure.setDesiredPressureEnviroment(225, 275, 250, 15); //rough setpoints.
            managerPressure.setDesiredPressureStability(-.010, .010, 0, 15);
            managerPressure.gotoPressure(2, 250);
            statusUpdate.UpdatingObject = (object)"Setting Pressure Manager to test point";

            resetAndWaitForPressure(); //Waiting for system to become stable.

            List<string> manResults = new List<string>(); //The results of the manifold testing.

            //Running checks.
            foreach (runnerPressureAssembly.runnerPressure ru in calibrationGroups[group].runners)
            {
                //A little break to make sure the venting is complete.
                System.Threading.Thread.Sleep(2000);

                statusUpdate.UpdatingObject = (object)("Testing: " + ru.runnerID + "," + ru.manifoldPos);

                //Opening the runners manifold port
                foreach (RelayController.relayControllerSystem rm in relayPresureManifold)
                {
                    if (rm.checkValidRequest(ru.manifoldPos))
                    {
                        rm.commandRelayBoard(ru.manifoldPos, true); //Opening the manifolds.
                    }
                }

                pressureStabilityReady = new System.Threading.AutoResetEvent(false); //Resetting the wait.

                pressureStabilityReady.WaitOne();

                //Rough check to see if pressure has changed. If the change is greater then 5 mBar then the connection is open to the air.
                //However is there is very little change then the port is blocked.
                System.Threading.Thread.Sleep(3000); //A little pause to allow for some change.

                int perCount = 0;
                List<double> pressureAvgData = new List<double>();
                while (perCount < 7)
                {
                    //Debug
                    statusUpdate.UpdatingObject = (object)("Waiting for pressure Data");
                    pressureWait.WaitOne();
                    pressureAvgData.Add(managerPressure.getPressureDifferancePerSec());
                    statusUpdate.UpdatingObject = (object)("Pressure Data Collected: " + perCount.ToString() + " - " + managerPressure.getPressureDifferancePerSec().ToString("0.0000"));
                    perCount++;
                }

                ru.currentPressureStabilityAvg = pressureAvgData.Average();
                statusUpdate.UpdatingObject = (object)("Auto Calc: " + pressureAvgData.Average().ToString("0.0000"));

                //If leak is greater then .003 then setting it's leak flag to true.
                if (pressureAvgData.Average() > 0.003)
                {
                    ru.leakStatus = true;
                }

                statusUpdate.UpdatingObject = (object)(ru.runnerID + " Leak status: " + ru.leakStatus.ToString());

                //Closing the runners manifold port
                foreach (RelayController.relayControllerSystem rm in relayPresureManifold)
                {
                    if (rm.checkValidRequest(ru.manifoldPos))
                    {
                        rm.commandRelayBoard(ru.manifoldPos, false); //Opening the manifolds.
                    }
                }

            }

            //Finding the runners that need to be remove.
            List<int> runnerToRemove = new List<int>();
            for(int x = 0; x < calibrationGroups[group].runners.Count; x++)
            {
                //Removing the runner from the group.
                if (calibrationGroups[group].runners[x].leakStatus)
                {
                    statusUpdate.UpdatingObject = (object)(calibrationGroups[group].runners[x].runnerID + " Leak found. Removing from group.");
                    runnerToRemove.Add(x);
                }
            }

            //Removing the runners.
            foreach (int index in runnerToRemove)
            {
                calibrationGroups[group].runners.Remove(calibrationGroups[group].runners[index]);
            }

            //Closing all manifold ports.
            for (int r = 0; r < relayPresureManifold.Length; r++) //Indexing manifold controllers.
            {

                string[] tempRelayFunction = relayPresureManifold[r].RelayFunction(); //Gettin the functions of the relays.
                for (int trf = 0; trf < tempRelayFunction.Length; trf++) //Going through the relay functions.
                {
                    relayPresureManifold[r].commandRelayBoard(tempRelayFunction[trf], false);
                }
            }

            //If calibration is underway.
            if (calibrationUnderWay)
            {
                //re-opening the group pressure manifolds.
                foreach (runnerPressureAssembly.runnerPressure run in calibrationGroups[group].runners)
                {
                    foreach (RelayController.relayControllerSystem rm in relayPresureManifold)
                    {
                        if (rm.checkValidRequest(run.manifoldPos))
                        {
                            statusUpdate.UpdatingObject = (object)("Opening: " + run.manifoldPos);
                            rm.commandRelayBoard(run.manifoldPos, true); //Opening the manifolds.
                        }
                    }
                }
            }
        }

        private void setupLogLOC()
        {
            //Checking to see if the base folder is there.
            if(!System.IO.Directory.Exists(calibrationLogLOC)) //Checking to see if the base folder is there.
            {
                System.IO.Directory.CreateDirectory(calibrationLogLOC); //creating the base folder.
            }

            //Creating the new logging folder loc.
            calibrationLogLOC += DateTime.Now.ToString("yyyyMMdd-HHmm") + "\\";

            //Creating the log,raw file, report dir.
            if (!System.IO.Directory.Exists(calibrationLogLOC)) //Checking to see if the sub folder is there.
            {
                System.IO.Directory.CreateDirectory(calibrationLogLOC); //creating the sub folder.
            }
        }

        /// <summary>
        /// Collects data from the radiosondes currently open to the pressure enviroment.
        /// </summary>
        /// <returns></returns>
        private List<object[]> collectCalibrationData()
        {
            //The data for the moment.
            List<object[]> currentDataMoment = new List<object[]>();

            //Go throw the radiosodnes exposed to the pressure enviroment and collect the latest radiosondes data, and sensor data.
            foreach (RelayController.relayControllerSystem rm in relayPresureManifold)
            {
                string[] currentRelayFunctions = rm.RelayFunction();
                foreach (string relay in currentRelayFunctions) //Going throw the current relay looking for the open ports.
                {
                    if (rm.getRelayState(rm.getItemsIndex(relay)))
                    {
                        //If the relay is open now we will go throw the runners looking for the matching manifoldPos and then collect the radiosonde data and pressure sensor data.
                        foreach (rackCalibration.rackPressure r in racks)
                        {
                            foreach (runnerPressureAssembly.runnerPressure run in r.runners)
                            {
                                if (run.manifoldPos == relay)
                                {
                                    //Located the open relay and runner.
                                    foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)
                                    {
                                        if (run.currentRadiosondes.Contains(sonde.port)) //Collecting the data from radiosondes confirmed connected to system.
                                        {
                                            object[] tempData = new object[4];
                                            tempData[0] = DateTime.Now;
                                            tempData[1] = sonde.CalData;
                                            tempData[2] = sensorPressure.getCurrentPressure();
                                            tempData[3] = chamber.CurrentChamberC;
                                            currentDataMoment.Add(tempData);
                                        }
                                    }
                                }
                            }
                        }                     

                    }
                }
            }

            return currentDataMoment;
        }

        /// <summary>
        /// Method writes the all the data to radiosonde raw files and pressure data is written all to one file.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="incomingData"></param>
        private void writeDataToFile(string group, List<object[]>incomingData)
        {
            //Checking for files.
            //Checking for radiosondes raw files.
            foreach (object[] data in incomingData)
            {
                //Pulling radiosonde data.
                ipRadiosonde.CalData radiosondeCalData = (ipRadiosonde.CalData)data[1];

                //Check for radiosonde raw file
                if(!System.IO.File.Exists(calibrationLogLOC + "_" + radiosondeCalData.SerialNumber + ".p_raw"))
                {
                    //create a writer and open the file
                    System.IO.TextWriter currentLogging = System.IO.File.AppendText(calibrationLogLOC + "_" + radiosondeCalData.SerialNumber + ".p_raw");
                    currentLogging.WriteLine("Pressure Calibration Process -- Raw Radiosonde Data");
                    currentLogging.WriteLine();
                    currentLogging.WriteLine("Date Time: " + startOfCalibration.ToString("MM/dd/yyyy HH:mm:ss"));
                    currentLogging.WriteLine();
                    currentLogging.WriteLine("TimeStamp(sec),PressCnt,PressCntRef,TempCnt,TempCntRef");
                    currentLogging.Close();
                }
            }

            //Checking for referance pressure file.
            if (!System.IO.File.Exists(calibrationLogLOC + group + "_Paroscientific_785.p_ref"))
            {
                //create a writer and open the file
                System.IO.TextWriter currentLogging = System.IO.File.AppendText(calibrationLogLOC + group + "_Paroscientific_785.p_ref");
                currentLogging.WriteLine("Pressure Calibration Process -- Reference Pressure Data");
                currentLogging.WriteLine();
                currentLogging.WriteLine("Date Time: " + startOfCalibration.ToString("MM/dd/yyyy HH:mm:ss"));
                currentLogging.WriteLine();
                currentLogging.WriteLine("TimeStamp(sec),PressRef(hPa),RefTemp(C)");
                currentLogging.Close();
            }

            //Writing radiosonde data its raw file and compiling the pressure data.
            List<object[]> pressureData = new List<object[]>();

            foreach (object[] data in incomingData)
            {
                //TimeStamp(sec),PressCnt,PressCntRef,TempCnt,TempCntRef
                //2852.813,434744,564256,131210,564271

                //Pulling radiosonde data.
                ipRadiosonde.CalData radiosondeCalData = (ipRadiosonde.CalData)data[1];
                
                //Getting the elapsed time from start of cal to the data point.
                TimeSpan elapsedTime = (DateTime)data[0] - startOfCalibration;

                //create a writer and open the file
                System.IO.TextWriter currentLogging = System.IO.File.AppendText(calibrationLogLOC + "_" + radiosondeCalData.SerialNumber + ".p_raw");
                currentLogging.WriteLine(elapsedTime.TotalSeconds.ToString("0.000") + "," + radiosondeCalData.PressureCounts + "," + radiosondeCalData.PressureRefCounts + "," +
                    radiosondeCalData.PressureTempCounts + "," + radiosondeCalData.PressureTempRefCounts);
                currentLogging.Close();

                //Compileing pressure data.
                object[] tempPressure = new object[3];
                tempPressure[0] = data[0];
                tempPressure[1] = data[2];
                tempPressure[2] = data[3];

                //Adding the data if not found.
                //Checking data
                if (pressureData.Count != 0) //No data protection.
                {
                    bool dataFound = false;
                    for (int ch = 0; ch < pressureData.Count; ch++)
                    {
                        object[] testingItem = (object[])pressureData[ch];
                        //Checking to make sure the data doesn't esisit.
                        if ((DateTime)testingItem[0] == (DateTime)tempPressure[0])// && Convert.ToDouble(testingItem[1]) == Convert.ToDouble(pressureData[1]) &&
                               // Convert.ToDouble(testingItem[2]) == Convert.ToDouble(tempPressure[2]))
                        {
                            dataFound = true;
                            break;
                        }
                    }

                    if (!dataFound) //If data not found then add it.
                    {
                        pressureData.Add(tempPressure);
                    }

                }
                else
                {
                    pressureData.Add(tempPressure); //Just adding the data.
                }
            }

            //Writing pressure referance data to file.
            //Setting up the pressure writer.
            System.IO.TextWriter pressureLogging = System.IO.File.AppendText(calibrationLogLOC + group + "_Paroscientific_785.p_ref");
            foreach (object[] pData in pressureData)
            {
                //Getting the elapsed time from start of cal to the data point.
                TimeSpan elapsedTime = (DateTime)pData[0] - startOfCalibration;
                pressureLogging.WriteLine(elapsedTime.TotalSeconds.ToString("0.000") + "," + Convert.ToDouble(pData[1]).ToString("0.000") + "," + Convert.ToDouble(pData[2]).ToString("0.000"));
            }
            //Closing file.
            pressureLogging.Close();

        }

        private void writeGroupLog()
        {
            int counterGroup = 0;
            foreach (runnerGroup group in calibrationGroups)
            {
                List<string> fileNames = new List<string>();
                foreach (runnerPressureAssembly.runnerPressure run in group.runners)
                {
                    foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)
                    {
                        fileNames.Add(calibrationLogLOC + "_" + sonde.CalData.SerialNumber + ".p_raw");
                    }
                }

                //Adding pressure ref file to file list.
                fileNames.Add(calibrationLogLOC + counterGroup + "_Paroscientific_785.p_ref");

                //Writing file list to group log file.
                System.IO.File.WriteAllLines(calibrationLogLOC + counterGroup.ToString() + "_Log.glog", fileNames.ToArray());

                counterGroup++;
            }
        }

        /// <summary>
        /// Method returns the calibration log loc.
        /// </summary>
        /// <returns></returns>
        public string getCalibrationLogLOC()
        {
            return calibrationLogLOC;
        }

        #endregion

        #endregion

        #region Methods triggered by events. These mainly to check for stability and alert in the event of error.

        //Use to check the enviroment air temp stability and run through the stability provess
        void dataPacketRecived_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {

        }

        //Used to check for pressure stability. Maybe??? not sure if needed. Pressure manager might be handleing this.
        void updatedPressureValue_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            pressureWait.Set(); //This will set true everytime there is a pressure data packet update.
        }

        // Pressure Manager Error Event
        void managerError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
        }

        void managerChamberError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {

        }

        void gotoPressureEvent_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            string incomingMessage = (string)data.NewValue;
            statusUpdate.UpdatingObject = (object)incomingMessage;
        }

        //Setting the wait when pressure conditions are ready.
        void pressureStabilityReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            pressureStabilityReady.Set();
        }

        void pressureConditionReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            pressureConditionsReady.Set();
        }

        //Setting chamber condtions as ready.
        void chamberStabilityReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            chamberStabilityReady.Set();
        }

        void chamberConditionReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            chamberConditionReady.Set();
        }

        void chamberTHReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            chamberTHReady.Set();
        }

        #endregion

        #region Methods for collecting and working with data.

        private List<ipRadiosonde.CalData> getAllCaldata()
        {
            //Temp Readings
            List<ipRadiosonde.CalData> tempReadings = new List<ipRadiosonde.CalData>();

            //Looping through all radiosonds online to collect current pressure count data.
            foreach (rackCalibration.rackPressure r in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure run in r.runners)
                {
                    foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)
                    {
                        if (run.currentRadiosondes.Contains(sonde.port)) //Limiting the access to port that already report radiosonde connected.
                        {
                            tempReadings.Add(sonde.CalData);
                        }
                    }
                }
            }

            return tempReadings;
        }

        private string[] findRadiosondeLOC(string serialNumber)
        {
            foreach (rackCalibration.rackPressure r in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure run in r.runners)
                {
                    foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)
                    {
                        if (sonde.CalData.SerialNumber == serialNumber)
                        {
                            return new string[3] { r.rackId, run.runnerID, serialNumber };
                        }
                    }
                }
            }

            return new string[3] { "error", "error", "error" };
        }


        #endregion

    }

    /// <summary>
    /// Calibration point information object.
    /// </summary>
    public class calibraitonPoint
    {
        //Calibration point.
        public int index;

        //Air Temp
        public double airTempSetPoint;
        public double airTempLower;
        public double airTempUpper;
        public int packetCountAT;

        //Air Temp Theratal... 
        public double airTHSetPoint;
        public double airTHLower;
        public double airTHUpper;
        public int packetCountTH;

        //Pressure Point
        public double pressureSetPoint;
        public double pressureLower;
        public double pressureUpper;
        public int packetCountPressure;

        public double pressureStabilitySetPoint;
        public double pressureStabilityLower;
        public double pressureStabilityUpper;
        public int packetCountPressureStability;

        //Humidity Point
        public double humidtySetPoint;
        public double humidityLower;
        public double humidityUpper;
        public int packetCountHumidity;

    }

    /// <summary>
    /// This is a group of runners that can be calibrated as the same time.
    /// </summary>
    public class runnerGroup
    {
        public List<runnerPressureAssembly.runnerPressure> runners = new List<runnerPressureAssembly.runnerPressure>(); //Runners that can be calibrated together.
        public List<DateTime[]> calibrationStableTime = new List<DateTime[]>(); //Times when the group meets stable conditions.

    }

    public class reports
    {
        //Date the report is created.
        public DateTime reportData;



        void report()
        {
            reportData = DateTime.Now;
        }

    }

}
