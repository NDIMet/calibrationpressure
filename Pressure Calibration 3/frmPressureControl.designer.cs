﻿namespace Pressure_Calibration_3
{
    partial class frmPressureControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxPMControl = new System.Windows.Forms.GroupBox();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.buttonGo = new System.Windows.Forms.Button();
            this.textBoxTol = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSetPoint = new System.Windows.Forms.TextBox();
            this.labelPressureReading = new System.Windows.Forms.Label();
            this.labelPRate = new System.Windows.Forms.Label();
            this.groupBoxPMControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxPMControl
            // 
            this.groupBoxPMControl.Controls.Add(this.labelPRate);
            this.groupBoxPMControl.Controls.Add(this.buttonDown);
            this.groupBoxPMControl.Controls.Add(this.buttonUp);
            this.groupBoxPMControl.Controls.Add(this.buttonGo);
            this.groupBoxPMControl.Controls.Add(this.textBoxTol);
            this.groupBoxPMControl.Controls.Add(this.label2);
            this.groupBoxPMControl.Controls.Add(this.label1);
            this.groupBoxPMControl.Controls.Add(this.textBoxSetPoint);
            this.groupBoxPMControl.Controls.Add(this.labelPressureReading);
            this.groupBoxPMControl.Location = new System.Drawing.Point(12, 12);
            this.groupBoxPMControl.Name = "groupBoxPMControl";
            this.groupBoxPMControl.Size = new System.Drawing.Size(461, 278);
            this.groupBoxPMControl.TabIndex = 0;
            this.groupBoxPMControl.TabStop = false;
            this.groupBoxPMControl.Text = "Pressure Manager";
            // 
            // buttonDown
            // 
            this.buttonDown.Location = new System.Drawing.Point(268, 236);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(52, 23);
            this.buttonDown.TabIndex = 7;
            this.buttonDown.Text = "DOWN";
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonDown_MouseDown);
            this.buttonDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonDown_MouseUp);
            // 
            // buttonUp
            // 
            this.buttonUp.Location = new System.Drawing.Point(268, 207);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(52, 23);
            this.buttonUp.TabIndex = 6;
            this.buttonUp.Text = "UP";
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.buttonUp_MouseDown);
            this.buttonUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buttonUp_MouseUp);
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(170, 248);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(75, 23);
            this.buttonGo.TabIndex = 5;
            this.buttonGo.Text = "Go";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // textBoxTol
            // 
            this.textBoxTol.Location = new System.Drawing.Point(220, 222);
            this.textBoxTol.Name = "textBoxTol";
            this.textBoxTol.Size = new System.Drawing.Size(33, 20);
            this.textBoxTol.TabIndex = 4;
            this.textBoxTol.Text = "0.5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 225);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tol:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(161, 199);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Set Point:";
            // 
            // textBoxSetPoint
            // 
            this.textBoxSetPoint.Location = new System.Drawing.Point(220, 196);
            this.textBoxSetPoint.Name = "textBoxSetPoint";
            this.textBoxSetPoint.Size = new System.Drawing.Size(33, 20);
            this.textBoxSetPoint.TabIndex = 1;
            this.textBoxSetPoint.Text = "5";
            // 
            // labelPressureReading
            // 
            this.labelPressureReading.BackColor = System.Drawing.Color.Black;
            this.labelPressureReading.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelPressureReading.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPressureReading.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelPressureReading.Location = new System.Drawing.Point(95, 158);
            this.labelPressureReading.Name = "labelPressureReading";
            this.labelPressureReading.Size = new System.Drawing.Size(132, 35);
            this.labelPressureReading.TabIndex = 0;
            this.labelPressureReading.Text = "1035.00";
            this.labelPressureReading.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPRate
            // 
            this.labelPRate.BackColor = System.Drawing.Color.Black;
            this.labelPRate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelPRate.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPRate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelPRate.Location = new System.Drawing.Point(233, 158);
            this.labelPRate.Name = "labelPRate";
            this.labelPRate.Size = new System.Drawing.Size(132, 35);
            this.labelPRate.TabIndex = 8;
            this.labelPRate.Text = "5.000";
            this.labelPRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmPressureControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 305);
            this.Controls.Add(this.groupBoxPMControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPressureControl";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.frmPressureControl_Load);
            this.groupBoxPMControl.ResumeLayout(false);
            this.groupBoxPMControl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxPMControl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSetPoint;
        private System.Windows.Forms.Label labelPressureReading;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.TextBox textBoxTol;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Label labelPRate;


    }
}