﻿namespace Pressure_Calibration_3
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        

        /// <summary>
        /// Clean up any resources being used.  
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.offLineCalculationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.radiosondesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCoefsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.genSimpleReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.genDetailReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopChamberToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testRadiosondesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validateRadiosondeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualPressureControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testRetestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBoxSensors = new System.Windows.Forms.GroupBox();
            this.groupBoxRadiosondeEXData = new System.Windows.Forms.GroupBox();
            this.labelRadiosondeCount = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBoxChamber = new System.Windows.Forms.GroupBox();
            this.labelChamberHumiditySetPoint = new System.Windows.Forms.Label();
            this.labelChamberPTSetPoint = new System.Windows.Forms.Label();
            this.labelChamberATSetPoint = new System.Windows.Forms.Label();
            this.labelChamberHumidityThrottle = new System.Windows.Forms.Label();
            this.labelChamberHumidity = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelChamberPTThrottle = new System.Windows.Forms.Label();
            this.labelChamberProductTemp = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelChamberATThrottle = new System.Windows.Forms.Label();
            this.labelChamberAirTemp = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxPressure = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelPressureChangeSetPoint = new System.Windows.Forms.Label();
            this.labelPressureSetPoint = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelPressureChange = new System.Windows.Forms.Label();
            this.labelCurrentPressure = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelChamberDisplay = new System.Windows.Forms.Panel();
            this.labelLoadingDisplay = new System.Windows.Forms.Label();
            this.groupBoxCurrentCalibration = new System.Windows.Forms.GroupBox();
            this.labelCalibrationStepRunTime = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.panelCurrentMode = new System.Windows.Forms.Panel();
            this.labelCurrentMode = new System.Windows.Forms.Label();
            this.labelCurrentGroup = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelCalibrationTHCount = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelCalibrationPressureRate = new System.Windows.Forms.Label();
            this.labelCalibrationATRate = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelCalibrationElementStability = new System.Windows.Forms.Label();
            this.labelCalibrationATStability = new System.Windows.Forms.Label();
            this.labelCalibrationElementRange = new System.Windows.Forms.Label();
            this.labelCalibrationATRange = new System.Windows.Forms.Label();
            this.labelCalibrationElementOption = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelCalibrationTotalSteps = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelCalibrationCurrentStep = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelCalibrationRunTime = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxControls = new System.Windows.Forms.GroupBox();
            this.checkBoxValidate = new System.Windows.Forms.CheckBox();
            this.comboBoxMode = new System.Windows.Forms.ComboBox();
            this.comboBoxCalibrationSettings = new System.Windows.Forms.ComboBox();
            this.buttonStartCalibration = new System.Windows.Forms.Button();
            this.textBoxDebug = new System.Windows.Forms.TextBox();
            this.updateValidateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripMain.SuspendLayout();
            this.statusStripMain.SuspendLayout();
            this.groupBoxSensors.SuspendLayout();
            this.groupBoxRadiosondeEXData.SuspendLayout();
            this.groupBoxChamber.SuspendLayout();
            this.groupBoxPressure.SuspendLayout();
            this.panelChamberDisplay.SuspendLayout();
            this.groupBoxCurrentCalibration.SuspendLayout();
            this.panelCurrentMode.SuspendLayout();
            this.groupBoxControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.radiosondesToolStripMenuItem,
            this.debugToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(789, 24);
            this.menuStripMain.TabIndex = 0;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.offLineCalculationToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // offLineCalculationToolStripMenuItem
            // 
            this.offLineCalculationToolStripMenuItem.Name = "offLineCalculationToolStripMenuItem";
            this.offLineCalculationToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.offLineCalculationToolStripMenuItem.Text = "Off-Line Calculation";
            this.offLineCalculationToolStripMenuItem.Click += new System.EventHandler(this.offLineCalculationToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // radiosondesToolStripMenuItem
            // 
            this.radiosondesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadCoefsToolStripMenuItem,
            this.genSimpleReportToolStripMenuItem,
            this.genDetailReportToolStripMenuItem,
            this.updateValidateToolStripMenuItem});
            this.radiosondesToolStripMenuItem.Name = "radiosondesToolStripMenuItem";
            this.radiosondesToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.radiosondesToolStripMenuItem.Text = "Radiosondes";
            // 
            // loadCoefsToolStripMenuItem
            // 
            this.loadCoefsToolStripMenuItem.Name = "loadCoefsToolStripMenuItem";
            this.loadCoefsToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.loadCoefsToolStripMenuItem.Text = "Load Coef\'s";
            this.loadCoefsToolStripMenuItem.Click += new System.EventHandler(this.loadCoefsToolStripMenuItem_Click);
            // 
            // genSimpleReportToolStripMenuItem
            // 
            this.genSimpleReportToolStripMenuItem.Name = "genSimpleReportToolStripMenuItem";
            this.genSimpleReportToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.genSimpleReportToolStripMenuItem.Text = "Gen Simple Report";
            this.genSimpleReportToolStripMenuItem.Click += new System.EventHandler(this.genSimpleReportToolStripMenuItem_Click);
            // 
            // genDetailReportToolStripMenuItem
            // 
            this.genDetailReportToolStripMenuItem.Name = "genDetailReportToolStripMenuItem";
            this.genDetailReportToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.genDetailReportToolStripMenuItem.Text = "Gen Detail Report";
            this.genDetailReportToolStripMenuItem.Click += new System.EventHandler(this.genDetailReportToolStripMenuItem_Click);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stopChamberToolStripMenuItem,
            this.testRadiosondesToolStripMenuItem,
            this.validateRadiosondeToolStripMenuItem,
            this.manualPressureControlToolStripMenuItem,
            this.testRetestToolStripMenuItem});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.debugToolStripMenuItem.Text = "Debug";
            // 
            // stopChamberToolStripMenuItem
            // 
            this.stopChamberToolStripMenuItem.Name = "stopChamberToolStripMenuItem";
            this.stopChamberToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.stopChamberToolStripMenuItem.Text = "Stop Chamber";
            this.stopChamberToolStripMenuItem.Click += new System.EventHandler(this.stopChamberToolStripMenuItem_Click);
            // 
            // testRadiosondesToolStripMenuItem
            // 
            this.testRadiosondesToolStripMenuItem.Name = "testRadiosondesToolStripMenuItem";
            this.testRadiosondesToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.testRadiosondesToolStripMenuItem.Text = "Test Radiosondes";
            this.testRadiosondesToolStripMenuItem.Click += new System.EventHandler(this.testRadiosondesToolStripMenuItem_Click_1);
            // 
            // validateRadiosondeToolStripMenuItem
            // 
            this.validateRadiosondeToolStripMenuItem.Name = "validateRadiosondeToolStripMenuItem";
            this.validateRadiosondeToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.validateRadiosondeToolStripMenuItem.Text = "Validate Radiosonde";
            this.validateRadiosondeToolStripMenuItem.Click += new System.EventHandler(this.validateRadiosondeToolStripMenuItem_Click);
            // 
            // manualPressureControlToolStripMenuItem
            // 
            this.manualPressureControlToolStripMenuItem.Name = "manualPressureControlToolStripMenuItem";
            this.manualPressureControlToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.manualPressureControlToolStripMenuItem.Text = "Manual Pressure Control";
            this.manualPressureControlToolStripMenuItem.Click += new System.EventHandler(this.manualPressureControlToolStripMenuItem_Click);
            // 
            // testRetestToolStripMenuItem
            // 
            this.testRetestToolStripMenuItem.Name = "testRetestToolStripMenuItem";
            this.testRetestToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.testRetestToolStripMenuItem.Text = "Test Retest";
            this.testRetestToolStripMenuItem.Click += new System.EventHandler(this.testRetestToolStripMenuItem_Click);
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelMain});
            this.statusStripMain.Location = new System.Drawing.Point(0, 622);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(789, 22);
            this.statusStripMain.TabIndex = 1;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // toolStripStatusLabelMain
            // 
            this.toolStripStatusLabelMain.Name = "toolStripStatusLabelMain";
            this.toolStripStatusLabelMain.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabelMain.Text = "toolStripStatusLabel1";
            // 
            // groupBoxSensors
            // 
            this.groupBoxSensors.Controls.Add(this.groupBoxRadiosondeEXData);
            this.groupBoxSensors.Controls.Add(this.groupBoxChamber);
            this.groupBoxSensors.Controls.Add(this.groupBoxPressure);
            this.groupBoxSensors.Location = new System.Drawing.Point(561, 27);
            this.groupBoxSensors.Name = "groupBoxSensors";
            this.groupBoxSensors.Size = new System.Drawing.Size(216, 386);
            this.groupBoxSensors.TabIndex = 2;
            this.groupBoxSensors.TabStop = false;
            this.groupBoxSensors.Text = "Current Enviroment";
            // 
            // groupBoxRadiosondeEXData
            // 
            this.groupBoxRadiosondeEXData.Controls.Add(this.labelRadiosondeCount);
            this.groupBoxRadiosondeEXData.Controls.Add(this.label19);
            this.groupBoxRadiosondeEXData.Location = new System.Drawing.Point(6, 312);
            this.groupBoxRadiosondeEXData.Name = "groupBoxRadiosondeEXData";
            this.groupBoxRadiosondeEXData.Size = new System.Drawing.Size(204, 68);
            this.groupBoxRadiosondeEXData.TabIndex = 2;
            this.groupBoxRadiosondeEXData.TabStop = false;
            this.groupBoxRadiosondeEXData.Text = "Radiosonde Data";
            // 
            // labelRadiosondeCount
            // 
            this.labelRadiosondeCount.BackColor = System.Drawing.Color.White;
            this.labelRadiosondeCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelRadiosondeCount.Location = new System.Drawing.Point(83, 25);
            this.labelRadiosondeCount.Name = "labelRadiosondeCount";
            this.labelRadiosondeCount.Size = new System.Drawing.Size(56, 23);
            this.labelRadiosondeCount.TabIndex = 17;
            this.labelRadiosondeCount.Text = "9999";
            this.labelRadiosondeCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelRadiosondeCount.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 30);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 13);
            this.label19.TabIndex = 16;
            this.label19.Text = "Radiosondes:";
            this.label19.Visible = false;
            // 
            // groupBoxChamber
            // 
            this.groupBoxChamber.Controls.Add(this.labelChamberHumiditySetPoint);
            this.groupBoxChamber.Controls.Add(this.labelChamberPTSetPoint);
            this.groupBoxChamber.Controls.Add(this.labelChamberATSetPoint);
            this.groupBoxChamber.Controls.Add(this.labelChamberHumidityThrottle);
            this.groupBoxChamber.Controls.Add(this.labelChamberHumidity);
            this.groupBoxChamber.Controls.Add(this.label13);
            this.groupBoxChamber.Controls.Add(this.label14);
            this.groupBoxChamber.Controls.Add(this.labelChamberPTThrottle);
            this.groupBoxChamber.Controls.Add(this.labelChamberProductTemp);
            this.groupBoxChamber.Controls.Add(this.label9);
            this.groupBoxChamber.Controls.Add(this.label10);
            this.groupBoxChamber.Controls.Add(this.labelChamberATThrottle);
            this.groupBoxChamber.Controls.Add(this.labelChamberAirTemp);
            this.groupBoxChamber.Controls.Add(this.label5);
            this.groupBoxChamber.Controls.Add(this.label6);
            this.groupBoxChamber.Location = new System.Drawing.Point(6, 120);
            this.groupBoxChamber.Name = "groupBoxChamber";
            this.groupBoxChamber.Size = new System.Drawing.Size(204, 185);
            this.groupBoxChamber.TabIndex = 1;
            this.groupBoxChamber.TabStop = false;
            this.groupBoxChamber.Text = "Chamber";
            // 
            // labelChamberHumiditySetPoint
            // 
            this.labelChamberHumiditySetPoint.BackColor = System.Drawing.Color.White;
            this.labelChamberHumiditySetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberHumiditySetPoint.Location = new System.Drawing.Point(142, 125);
            this.labelChamberHumiditySetPoint.Name = "labelChamberHumiditySetPoint";
            this.labelChamberHumiditySetPoint.Size = new System.Drawing.Size(56, 23);
            this.labelChamberHumiditySetPoint.TabIndex = 20;
            this.labelChamberHumiditySetPoint.Text = "9999.999";
            this.labelChamberHumiditySetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelChamberPTSetPoint
            // 
            this.labelChamberPTSetPoint.BackColor = System.Drawing.Color.White;
            this.labelChamberPTSetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberPTSetPoint.Location = new System.Drawing.Point(142, 71);
            this.labelChamberPTSetPoint.Name = "labelChamberPTSetPoint";
            this.labelChamberPTSetPoint.Size = new System.Drawing.Size(56, 23);
            this.labelChamberPTSetPoint.TabIndex = 18;
            this.labelChamberPTSetPoint.Text = "9999.999";
            this.labelChamberPTSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelChamberATSetPoint
            // 
            this.labelChamberATSetPoint.BackColor = System.Drawing.Color.White;
            this.labelChamberATSetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberATSetPoint.Location = new System.Drawing.Point(142, 18);
            this.labelChamberATSetPoint.Name = "labelChamberATSetPoint";
            this.labelChamberATSetPoint.Size = new System.Drawing.Size(56, 23);
            this.labelChamberATSetPoint.TabIndex = 16;
            this.labelChamberATSetPoint.Text = "9999.999";
            this.labelChamberATSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelChamberHumidityThrottle
            // 
            this.labelChamberHumidityThrottle.BackColor = System.Drawing.Color.White;
            this.labelChamberHumidityThrottle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberHumidityThrottle.Location = new System.Drawing.Point(83, 152);
            this.labelChamberHumidityThrottle.Name = "labelChamberHumidityThrottle";
            this.labelChamberHumidityThrottle.Size = new System.Drawing.Size(56, 23);
            this.labelChamberHumidityThrottle.TabIndex = 15;
            this.labelChamberHumidityThrottle.Text = "9999.999";
            this.labelChamberHumidityThrottle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelChamberHumidity
            // 
            this.labelChamberHumidity.BackColor = System.Drawing.Color.White;
            this.labelChamberHumidity.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberHumidity.Location = new System.Drawing.Point(83, 125);
            this.labelChamberHumidity.Name = "labelChamberHumidity";
            this.labelChamberHumidity.Size = new System.Drawing.Size(56, 23);
            this.labelChamberHumidity.TabIndex = 14;
            this.labelChamberHumidity.Text = "9999.999";
            this.labelChamberHumidity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "RH Throttle:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(28, 130);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "Humidity:";
            // 
            // labelChamberPTThrottle
            // 
            this.labelChamberPTThrottle.BackColor = System.Drawing.Color.White;
            this.labelChamberPTThrottle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberPTThrottle.Location = new System.Drawing.Point(83, 98);
            this.labelChamberPTThrottle.Name = "labelChamberPTThrottle";
            this.labelChamberPTThrottle.Size = new System.Drawing.Size(56, 23);
            this.labelChamberPTThrottle.TabIndex = 11;
            this.labelChamberPTThrottle.Text = "9999.999";
            this.labelChamberPTThrottle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelChamberProductTemp
            // 
            this.labelChamberProductTemp.BackColor = System.Drawing.Color.White;
            this.labelChamberProductTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberProductTemp.Location = new System.Drawing.Point(83, 71);
            this.labelChamberProductTemp.Name = "labelChamberProductTemp";
            this.labelChamberProductTemp.Size = new System.Drawing.Size(56, 23);
            this.labelChamberProductTemp.TabIndex = 10;
            this.labelChamberProductTemp.Text = "9999.999";
            this.labelChamberProductTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "PT Throttle:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Product Temp:";
            // 
            // labelChamberATThrottle
            // 
            this.labelChamberATThrottle.BackColor = System.Drawing.Color.White;
            this.labelChamberATThrottle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberATThrottle.Location = new System.Drawing.Point(83, 45);
            this.labelChamberATThrottle.Name = "labelChamberATThrottle";
            this.labelChamberATThrottle.Size = new System.Drawing.Size(56, 23);
            this.labelChamberATThrottle.TabIndex = 7;
            this.labelChamberATThrottle.Text = "9999.999";
            this.labelChamberATThrottle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelChamberAirTemp
            // 
            this.labelChamberAirTemp.BackColor = System.Drawing.Color.White;
            this.labelChamberAirTemp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelChamberAirTemp.Location = new System.Drawing.Point(83, 18);
            this.labelChamberAirTemp.Name = "labelChamberAirTemp";
            this.labelChamberAirTemp.Size = new System.Drawing.Size(56, 23);
            this.labelChamberAirTemp.TabIndex = 6;
            this.labelChamberAirTemp.Text = "9999.999";
            this.labelChamberAirTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "AT Throttle:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Air Temp:";
            // 
            // groupBoxPressure
            // 
            this.groupBoxPressure.Controls.Add(this.label4);
            this.groupBoxPressure.Controls.Add(this.labelPressureChangeSetPoint);
            this.groupBoxPressure.Controls.Add(this.labelPressureSetPoint);
            this.groupBoxPressure.Controls.Add(this.label3);
            this.groupBoxPressure.Controls.Add(this.labelPressureChange);
            this.groupBoxPressure.Controls.Add(this.labelCurrentPressure);
            this.groupBoxPressure.Controls.Add(this.label2);
            this.groupBoxPressure.Controls.Add(this.label1);
            this.groupBoxPressure.Location = new System.Drawing.Point(6, 19);
            this.groupBoxPressure.Name = "groupBoxPressure";
            this.groupBoxPressure.Size = new System.Drawing.Size(204, 95);
            this.groupBoxPressure.TabIndex = 0;
            this.groupBoxPressure.TabStop = false;
            this.groupBoxPressure.Text = "Pressure";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(144, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Set Point";
            // 
            // labelPressureChangeSetPoint
            // 
            this.labelPressureChangeSetPoint.BackColor = System.Drawing.Color.White;
            this.labelPressureChangeSetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelPressureChangeSetPoint.Location = new System.Drawing.Point(142, 62);
            this.labelPressureChangeSetPoint.Name = "labelPressureChangeSetPoint";
            this.labelPressureChangeSetPoint.Size = new System.Drawing.Size(56, 23);
            this.labelPressureChangeSetPoint.TabIndex = 6;
            this.labelPressureChangeSetPoint.Text = "9999.999";
            this.labelPressureChangeSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPressureSetPoint
            // 
            this.labelPressureSetPoint.BackColor = System.Drawing.Color.White;
            this.labelPressureSetPoint.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelPressureSetPoint.Location = new System.Drawing.Point(142, 35);
            this.labelPressureSetPoint.Name = "labelPressureSetPoint";
            this.labelPressureSetPoint.Size = new System.Drawing.Size(56, 23);
            this.labelPressureSetPoint.TabIndex = 5;
            this.labelPressureSetPoint.Text = "9999.999";
            this.labelPressureSetPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(88, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Current";
            // 
            // labelPressureChange
            // 
            this.labelPressureChange.BackColor = System.Drawing.Color.White;
            this.labelPressureChange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelPressureChange.Location = new System.Drawing.Point(83, 62);
            this.labelPressureChange.Name = "labelPressureChange";
            this.labelPressureChange.Size = new System.Drawing.Size(56, 23);
            this.labelPressureChange.TabIndex = 3;
            this.labelPressureChange.Text = "9999.999";
            this.labelPressureChange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCurrentPressure
            // 
            this.labelCurrentPressure.BackColor = System.Drawing.Color.White;
            this.labelCurrentPressure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCurrentPressure.Location = new System.Drawing.Point(83, 35);
            this.labelCurrentPressure.Name = "labelCurrentPressure";
            this.labelCurrentPressure.Size = new System.Drawing.Size(56, 23);
            this.labelCurrentPressure.TabIndex = 2;
            this.labelCurrentPressure.Text = "9999.999";
            this.labelCurrentPressure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "mbara/sec:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pressure:";
            // 
            // panelChamberDisplay
            // 
            this.panelChamberDisplay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelChamberDisplay.Controls.Add(this.labelLoadingDisplay);
            this.panelChamberDisplay.Location = new System.Drawing.Point(12, 27);
            this.panelChamberDisplay.Name = "panelChamberDisplay";
            this.panelChamberDisplay.Size = new System.Drawing.Size(540, 386);
            this.panelChamberDisplay.TabIndex = 3;
            // 
            // labelLoadingDisplay
            // 
            this.labelLoadingDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLoadingDisplay.Location = new System.Drawing.Point(159, 151);
            this.labelLoadingDisplay.Name = "labelLoadingDisplay";
            this.labelLoadingDisplay.Size = new System.Drawing.Size(221, 83);
            this.labelLoadingDisplay.TabIndex = 1;
            this.labelLoadingDisplay.Text = "Loading";
            this.labelLoadingDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxCurrentCalibration
            // 
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationStepRunTime);
            this.groupBoxCurrentCalibration.Controls.Add(this.label22);
            this.groupBoxCurrentCalibration.Controls.Add(this.panelCurrentMode);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCurrentGroup);
            this.groupBoxCurrentCalibration.Controls.Add(this.label18);
            this.groupBoxCurrentCalibration.Controls.Add(this.label16);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationTHCount);
            this.groupBoxCurrentCalibration.Controls.Add(this.label15);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationPressureRate);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationATRate);
            this.groupBoxCurrentCalibration.Controls.Add(this.label21);
            this.groupBoxCurrentCalibration.Controls.Add(this.label20);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationElementStability);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationATStability);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationElementRange);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationATRange);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationElementOption);
            this.groupBoxCurrentCalibration.Controls.Add(this.label12);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationTotalSteps);
            this.groupBoxCurrentCalibration.Controls.Add(this.label11);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationCurrentStep);
            this.groupBoxCurrentCalibration.Controls.Add(this.label8);
            this.groupBoxCurrentCalibration.Controls.Add(this.labelCalibrationRunTime);
            this.groupBoxCurrentCalibration.Controls.Add(this.label7);
            this.groupBoxCurrentCalibration.Enabled = false;
            this.groupBoxCurrentCalibration.Location = new System.Drawing.Point(12, 419);
            this.groupBoxCurrentCalibration.Name = "groupBoxCurrentCalibration";
            this.groupBoxCurrentCalibration.Size = new System.Drawing.Size(540, 118);
            this.groupBoxCurrentCalibration.TabIndex = 4;
            this.groupBoxCurrentCalibration.TabStop = false;
            this.groupBoxCurrentCalibration.Text = "Calibraiton Status";
            // 
            // labelCalibrationStepRunTime
            // 
            this.labelCalibrationStepRunTime.AutoSize = true;
            this.labelCalibrationStepRunTime.Location = new System.Drawing.Point(472, 82);
            this.labelCalibrationStepRunTime.Name = "labelCalibrationStepRunTime";
            this.labelCalibrationStepRunTime.Size = new System.Drawing.Size(55, 13);
            this.labelCalibrationStepRunTime.TabIndex = 23;
            this.labelCalibrationStepRunTime.Text = "HH:mm:ss";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(359, 82);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(114, 13);
            this.label22.TabIndex = 22;
            this.label22.Text = "Calibration Step Clock:";
            // 
            // panelCurrentMode
            // 
            this.panelCurrentMode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelCurrentMode.Controls.Add(this.labelCurrentMode);
            this.panelCurrentMode.Location = new System.Drawing.Point(363, 16);
            this.panelCurrentMode.Name = "panelCurrentMode";
            this.panelCurrentMode.Size = new System.Drawing.Size(168, 63);
            this.panelCurrentMode.TabIndex = 21;
            // 
            // labelCurrentMode
            // 
            this.labelCurrentMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentMode.Location = new System.Drawing.Point(3, 17);
            this.labelCurrentMode.Name = "labelCurrentMode";
            this.labelCurrentMode.Size = new System.Drawing.Size(158, 27);
            this.labelCurrentMode.TabIndex = 0;
            this.labelCurrentMode.Text = "Loading";
            this.labelCurrentMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCurrentGroup
            // 
            this.labelCurrentGroup.AutoSize = true;
            this.labelCurrentGroup.Location = new System.Drawing.Point(288, 99);
            this.labelCurrentGroup.Name = "labelCurrentGroup";
            this.labelCurrentGroup.Size = new System.Drawing.Size(27, 13);
            this.labelCurrentGroup.TabIndex = 20;
            this.labelCurrentGroup.Text = "N/A";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(206, 99);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "Current Group:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(270, 21);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "TH Rate";
            // 
            // labelCalibrationTHCount
            // 
            this.labelCalibrationTHCount.BackColor = System.Drawing.Color.White;
            this.labelCalibrationTHCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationTHCount.Location = new System.Drawing.Point(256, 39);
            this.labelCalibrationTHCount.Name = "labelCalibrationTHCount";
            this.labelCalibrationTHCount.Size = new System.Drawing.Size(82, 23);
            this.labelCalibrationTHCount.TabIndex = 17;
            this.labelCalibrationTHCount.Text = "12345";
            this.labelCalibrationTHCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(208, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Rate";
            // 
            // labelCalibrationPressureRate
            // 
            this.labelCalibrationPressureRate.BackColor = System.Drawing.Color.White;
            this.labelCalibrationPressureRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationPressureRate.Location = new System.Drawing.Point(194, 65);
            this.labelCalibrationPressureRate.Name = "labelCalibrationPressureRate";
            this.labelCalibrationPressureRate.Size = new System.Drawing.Size(56, 23);
            this.labelCalibrationPressureRate.TabIndex = 15;
            this.labelCalibrationPressureRate.Text = "12345";
            this.labelCalibrationPressureRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalibrationATRate
            // 
            this.labelCalibrationATRate.BackColor = System.Drawing.Color.White;
            this.labelCalibrationATRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationATRate.Location = new System.Drawing.Point(194, 39);
            this.labelCalibrationATRate.Name = "labelCalibrationATRate";
            this.labelCalibrationATRate.Size = new System.Drawing.Size(56, 23);
            this.labelCalibrationATRate.TabIndex = 14;
            this.labelCalibrationATRate.Text = "12345";
            this.labelCalibrationATRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(140, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(43, 13);
            this.label21.TabIndex = 13;
            this.label21.Text = "Stability";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(77, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(39, 13);
            this.label20.TabIndex = 12;
            this.label20.Text = "Range";
            // 
            // labelCalibrationElementStability
            // 
            this.labelCalibrationElementStability.BackColor = System.Drawing.Color.White;
            this.labelCalibrationElementStability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationElementStability.Location = new System.Drawing.Point(132, 65);
            this.labelCalibrationElementStability.Name = "labelCalibrationElementStability";
            this.labelCalibrationElementStability.Size = new System.Drawing.Size(56, 23);
            this.labelCalibrationElementStability.TabIndex = 11;
            this.labelCalibrationElementStability.Text = "12345";
            this.labelCalibrationElementStability.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalibrationATStability
            // 
            this.labelCalibrationATStability.BackColor = System.Drawing.Color.White;
            this.labelCalibrationATStability.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationATStability.Location = new System.Drawing.Point(132, 39);
            this.labelCalibrationATStability.Name = "labelCalibrationATStability";
            this.labelCalibrationATStability.Size = new System.Drawing.Size(56, 23);
            this.labelCalibrationATStability.TabIndex = 10;
            this.labelCalibrationATStability.Text = "12345";
            this.labelCalibrationATStability.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalibrationElementRange
            // 
            this.labelCalibrationElementRange.BackColor = System.Drawing.Color.White;
            this.labelCalibrationElementRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationElementRange.Location = new System.Drawing.Point(70, 65);
            this.labelCalibrationElementRange.Name = "labelCalibrationElementRange";
            this.labelCalibrationElementRange.Size = new System.Drawing.Size(56, 23);
            this.labelCalibrationElementRange.TabIndex = 9;
            this.labelCalibrationElementRange.Text = "12345";
            this.labelCalibrationElementRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalibrationATRange
            // 
            this.labelCalibrationATRange.BackColor = System.Drawing.Color.White;
            this.labelCalibrationATRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelCalibrationATRange.Location = new System.Drawing.Point(70, 39);
            this.labelCalibrationATRange.Name = "labelCalibrationATRange";
            this.labelCalibrationATRange.Size = new System.Drawing.Size(56, 23);
            this.labelCalibrationATRange.TabIndex = 8;
            this.labelCalibrationATRange.Text = "12345";
            this.labelCalibrationATRange.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCalibrationElementOption
            // 
            this.labelCalibrationElementOption.AutoSize = true;
            this.labelCalibrationElementOption.Location = new System.Drawing.Point(13, 70);
            this.labelCalibrationElementOption.Name = "labelCalibrationElementOption";
            this.labelCalibrationElementOption.Size = new System.Drawing.Size(51, 13);
            this.labelCalibrationElementOption.TabIndex = 7;
            this.labelCalibrationElementOption.Text = "Pressure:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Air Temp:";
            // 
            // labelCalibrationTotalSteps
            // 
            this.labelCalibrationTotalSteps.AutoSize = true;
            this.labelCalibrationTotalSteps.Location = new System.Drawing.Point(179, 99);
            this.labelCalibrationTotalSteps.Name = "labelCalibrationTotalSteps";
            this.labelCalibrationTotalSteps.Size = new System.Drawing.Size(21, 13);
            this.labelCalibrationTotalSteps.TabIndex = 5;
            this.labelCalibrationTotalSteps.Text = "XX";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(156, 99);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "of:";
            // 
            // labelCalibrationCurrentStep
            // 
            this.labelCalibrationCurrentStep.AutoSize = true;
            this.labelCalibrationCurrentStep.Location = new System.Drawing.Point(132, 99);
            this.labelCalibrationCurrentStep.Name = "labelCalibrationCurrentStep";
            this.labelCalibrationCurrentStep.Size = new System.Drawing.Size(21, 13);
            this.labelCalibrationCurrentStep.TabIndex = 3;
            this.labelCalibrationCurrentStep.Text = "XX";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Current Calibration Step";
            // 
            // labelCalibrationRunTime
            // 
            this.labelCalibrationRunTime.AutoSize = true;
            this.labelCalibrationRunTime.Location = new System.Drawing.Point(472, 99);
            this.labelCalibrationRunTime.Name = "labelCalibrationRunTime";
            this.labelCalibrationRunTime.Size = new System.Drawing.Size(55, 13);
            this.labelCalibrationRunTime.TabIndex = 1;
            this.labelCalibrationRunTime.Text = "HH:mm:ss";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(357, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Total Calibration Clock:";
            // 
            // groupBoxControls
            // 
            this.groupBoxControls.Controls.Add(this.checkBoxValidate);
            this.groupBoxControls.Controls.Add(this.comboBoxMode);
            this.groupBoxControls.Controls.Add(this.comboBoxCalibrationSettings);
            this.groupBoxControls.Controls.Add(this.buttonStartCalibration);
            this.groupBoxControls.Location = new System.Drawing.Point(561, 419);
            this.groupBoxControls.Name = "groupBoxControls";
            this.groupBoxControls.Size = new System.Drawing.Size(216, 118);
            this.groupBoxControls.TabIndex = 5;
            this.groupBoxControls.TabStop = false;
            this.groupBoxControls.Text = "Controls";
            // 
            // checkBoxValidate
            // 
            this.checkBoxValidate.AutoSize = true;
            this.checkBoxValidate.Checked = true;
            this.checkBoxValidate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxValidate.Location = new System.Drawing.Point(143, 63);
            this.checkBoxValidate.Name = "checkBoxValidate";
            this.checkBoxValidate.Size = new System.Drawing.Size(64, 17);
            this.checkBoxValidate.TabIndex = 3;
            this.checkBoxValidate.Text = "Validate";
            this.checkBoxValidate.UseVisualStyleBackColor = true;
            this.checkBoxValidate.CheckedChanged += new System.EventHandler(this.checkBoxValidate_CheckedChanged);
            // 
            // comboBoxMode
            // 
            this.comboBoxMode.FormattingEnabled = true;
            this.comboBoxMode.Items.AddRange(new object[] {
            "Calibration",
            "Test",
            "Retest"});
            this.comboBoxMode.Location = new System.Drawing.Point(16, 61);
            this.comboBoxMode.Name = "comboBoxMode";
            this.comboBoxMode.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMode.TabIndex = 2;
            this.comboBoxMode.Text = "Calibration";
            // 
            // comboBoxCalibrationSettings
            // 
            this.comboBoxCalibrationSettings.FormattingEnabled = true;
            this.comboBoxCalibrationSettings.Location = new System.Drawing.Point(12, 35);
            this.comboBoxCalibrationSettings.Name = "comboBoxCalibrationSettings";
            this.comboBoxCalibrationSettings.Size = new System.Drawing.Size(192, 21);
            this.comboBoxCalibrationSettings.TabIndex = 1;
            // 
            // buttonStartCalibration
            // 
            this.buttonStartCalibration.Enabled = false;
            this.buttonStartCalibration.Location = new System.Drawing.Point(71, 89);
            this.buttonStartCalibration.Name = "buttonStartCalibration";
            this.buttonStartCalibration.Size = new System.Drawing.Size(75, 23);
            this.buttonStartCalibration.TabIndex = 0;
            this.buttonStartCalibration.Text = "Start";
            this.buttonStartCalibration.UseVisualStyleBackColor = true;
            this.buttonStartCalibration.Click += new System.EventHandler(this.buttonStartCalibration_Click);
            // 
            // textBoxDebug
            // 
            this.textBoxDebug.Location = new System.Drawing.Point(12, 543);
            this.textBoxDebug.Multiline = true;
            this.textBoxDebug.Name = "textBoxDebug";
            this.textBoxDebug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDebug.Size = new System.Drawing.Size(759, 76);
            this.textBoxDebug.TabIndex = 0;
            // 
            // updateValidateToolStripMenuItem
            // 
            this.updateValidateToolStripMenuItem.Name = "updateValidateToolStripMenuItem";
            this.updateValidateToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.updateValidateToolStripMenuItem.Text = "Update Validate";
            this.updateValidateToolStripMenuItem.Click += new System.EventHandler(this.updateValidateToolStripMenuItem_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 644);
            this.Controls.Add(this.textBoxDebug);
            this.Controls.Add(this.groupBoxControls);
            this.Controls.Add(this.groupBoxCurrentCalibration);
            this.Controls.Add(this.panelChamberDisplay);
            this.Controls.Add(this.groupBoxSensors);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.menuStripMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "frmMain";
            this.Text = "600084.2000 Radiosonde Pressure Calibration";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.groupBoxSensors.ResumeLayout(false);
            this.groupBoxRadiosondeEXData.ResumeLayout(false);
            this.groupBoxRadiosondeEXData.PerformLayout();
            this.groupBoxChamber.ResumeLayout(false);
            this.groupBoxChamber.PerformLayout();
            this.groupBoxPressure.ResumeLayout(false);
            this.groupBoxPressure.PerformLayout();
            this.panelChamberDisplay.ResumeLayout(false);
            this.groupBoxCurrentCalibration.ResumeLayout(false);
            this.groupBoxCurrentCalibration.PerformLayout();
            this.panelCurrentMode.ResumeLayout(false);
            this.groupBoxControls.ResumeLayout(false);
            this.groupBoxControls.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMain;
        private System.Windows.Forms.GroupBox groupBoxSensors;
        private System.Windows.Forms.Panel panelChamberDisplay;
        private System.Windows.Forms.GroupBox groupBoxCurrentCalibration;
        private System.Windows.Forms.GroupBox groupBoxControls;
        private System.Windows.Forms.GroupBox groupBoxPressure;
        private System.Windows.Forms.Label labelPressureChange;
        private System.Windows.Forms.Label labelCurrentPressure;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxChamber;
        private System.Windows.Forms.Label labelChamberHumidityThrottle;
        private System.Windows.Forms.Label labelChamberHumidity;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelChamberPTThrottle;
        private System.Windows.Forms.Label labelChamberProductTemp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelChamberATThrottle;
        private System.Windows.Forms.Label labelChamberAirTemp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelChamberHumiditySetPoint;
        private System.Windows.Forms.Label labelChamberPTSetPoint;
        private System.Windows.Forms.Label labelChamberATSetPoint;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelPressureChangeSetPoint;
        private System.Windows.Forms.Label labelPressureSetPoint;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonStartCalibration;
        private System.Windows.Forms.Label labelCalibrationRunTime;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelCalibrationTotalSteps;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelCalibrationCurrentStep;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label labelCalibrationElementStability;
        private System.Windows.Forms.Label labelCalibrationATStability;
        private System.Windows.Forms.Label labelCalibrationElementRange;
        private System.Windows.Forms.Label labelCalibrationATRange;
        private System.Windows.Forms.Label labelCalibrationElementOption;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxCalibrationSettings;
        private System.Windows.Forms.TextBox textBoxDebug;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelCalibrationPressureRate;
        private System.Windows.Forms.Label labelCalibrationATRate;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelCalibrationTHCount;
        private System.Windows.Forms.ToolStripMenuItem offLineCalculationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem radiosondesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadCoefsToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxMode;
        private System.Windows.Forms.Label labelCurrentGroup;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBoxRadiosondeEXData;
        private System.Windows.Forms.Label labelRadiosondeCount;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panelCurrentMode;
        private System.Windows.Forms.Label labelCurrentMode;
        private System.Windows.Forms.Label labelCalibrationStepRunTime;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label labelLoadingDisplay;
        private System.Windows.Forms.CheckBox checkBoxValidate;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopChamberToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testRadiosondesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validateRadiosondeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem genDetailReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem genSimpleReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualPressureControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testRetestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateValidateToolStripMenuItem;

    }
}