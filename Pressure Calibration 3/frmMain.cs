﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pressure_Calibration_3
{

    public struct settingsEquipment
    {
        //Pressure Sensor
        public string pressureSensorIP;
        public int pressureSensorPort;

        //Relay System Equipment
        public RelayController.relaySettings relaySystem;


        //Relay Pressure Manifolds
        public List<RelayController.relaySettings> relayManifolds;

        //Chamber
        public string chamberIP;
        public int chamberPort;

        //Rack settings. These settings include the rack id, runnerids, and the radiosonde ports on the runner.
        public List<rackCalibration.rackSettings> racks;

        //Log loc
        public string LOGLOC;

        //coef storage location
        public string coefLoc;

    }

    #region Delegates for invoking back into the main thread

    //Data to be displayed from sensor thread to main thread
    delegate void UpdateDisplay(string desiredUpdate);

    delegate void UpdateCalibrationDisplay(string toUpdate, object updateData);

    delegate void updateMainPanel(object objectToAdd);

    delegate void sendABool(bool thatDarnBool);

    #endregion

    public partial class frmMain : Form
    {
        //Public objects needed for calibration

        //Logic controllers
        public Calibration.calibrationPressure processPressure;
        public List<Calibration.calibraitonPoint> currentCalibration; //All the steps the calibration will go through.
        public ValidateCalibration.validatePressureCalibration curValidation; //If a validation is running this will not be null and have properties.

        //Pressure Manager.
        public ManagerPresssure.pressureManager managerPressure;

        //Sensors and chamber
        //public TestEquipment.Paroscientific sensorPressure; //Old Referance Sensor class
        public equipmentTCPIP.equipmentParoTCPIP sensorPressure;    //New com class for parosensor
        //public TestEquipment.BBModule relaySystemEquipment; //Relay controller for pressure up/down and any other thing not related to manifold switching.
        //public TestEquipment.BBModule[] relayPressureManifold; //Relay controllers dirrectly relayed to controlling the pressure manifolds.
        public RelayController.relayControllerSystem relaySystemEquipment;
        public RelayController.relayControllerSystem[] relayPressureManifold;

        public TestEquipment.Thermotron chamber;

        //Now for the wonderful and great radiosonde stuff.
        public rackCalibration.rackPressure[] racks;

        public settingsEquipment settingsEquipment = new settingsEquipment();

        public updateCreater.objectUpdate programReady = new updateCreater.objectUpdate();

        //Screen Display objects.
        displayCalibration.pCalDisplay pressureDisplay = new displayCalibration.pCalDisplay();

        //Private objects
        System.Timers.Timer collectGarbage = new System.Timers.Timer(360000); //Timer to clean up trashed data.

        //Email address to contact with errors.
        string emailAdminAddress = "";

        #region Background Workers

        private System.ComponentModel.BackgroundWorker backgroundWorkerGetReferancePressure;
        private System.ComponentModel.BackgroundWorker backgroundWorkerGetChamberStatus;

        #endregion


        public frmMain()
        {
            InitializeComponent();

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            contorlControls(false); //startMode); //Allowing the user access to the start calibration button.
            programReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(programReady_PropertyChange);

            //Loading in settings from file.
            loadSettingFromFile();

            //Setting up and connecting to all the components. Runners, Sensors, Relays.
            //Setting the run/stand by mode.

            bool startMode = false;

            DialogResult startupMode = MessageBox.Show("Connect to Hardware?", "Mode Option", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

            if (startupMode == DialogResult.Yes)
            {
                configurePressureSensor();
                configureChamber();
                configureRelaySystems();
                configureManifoldRelaySystem();
                configurePressureManager();

                //Spinning up a thread to start up the radiosondes.
                //Otherwise the program sit there forever and then will crash as other event try to access items not yet created.
                System.Threading.Thread startRadiosondes = new System.Threading.Thread(new System.Threading.ThreadStart(configureRadiosondeRacks));

                startRadiosondes.Start(); //This will setup the racks, the runners, and the radiosonde ports.
                startMode = true; //allowing buttons.
            }
            //Setting up display
            loadAllCalibrations();//loading all calibrations that can be run.

            //All Systems loaded and Ready.
            updataMainStatus("Started");

            //Starting the timer to clean up the memory
            collectGarbage.Elapsed += new System.Timers.ElapsedEventHandler(collectGarbage_Elapsed);
            collectGarbage.Enabled = true;

            //Getting the version number to be displayed.
            System.Reflection.Assembly ass = System.Reflection.Assembly.GetExecutingAssembly();

            string version = "1.0.2.12";

            try
            {
                BeginInvoke(new UpdateDisplay(this.updateFrmName), new object[] { version });
            }
            catch { }


#if DEBUG
            debugToolStripMenuItem.Visible = true;

#endif
        }

        private void updateFrmName(string buildValue)
        {
            this.Text = this.Text + " " + buildValue;
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        #region Methods related to starting up calibration equipment

        /// <summary>
        /// Method loads all the settings into object in the program as we get ready to init equipment.
        /// </summary>
        private void loadSettingFromFile()
        {
            //Inilizing the realy settings lists.
            settingsEquipment.relaySystem = new RelayController.relaySettings(); //System level relays. Things like pressure up/down and other stuff like lights, power anything like that.
            settingsEquipment.relayManifolds = new List<RelayController.relaySettings>(); //Manifold relays. These connect and disconnect the runners from the main pressure system.



            //Loading all of the config lines into memory
            string[] settingsInFile = System.IO.File.ReadAllLines("pSettings.cfg");

            for (int x = 0; x < settingsInFile.Length; x++)
            {
            restartReading:

                //If the line starts with a # we are going to skip it
                if (settingsInFile[x].Contains("#"))
                {
                    x++;
                    goto restartReading;
                }         

                #region Setting up the pressure referance sensor
                if (settingsInFile[x].Contains("sensorPressure"))
                {
                    char[] splitter = new char[2] {'=', ','};
                    string[] sensorSettings = settingsInFile[x].Split(splitter);
                    
                    //Leaving space for the pressure sensor to be replace with some kind of different sensor.
                    switch (sensorSettings[1])
                    {
                        case "Paro":
                            settingsEquipment.pressureSensorIP = sensorSettings[2];
                            settingsEquipment.pressureSensorPort = Convert.ToInt16(sensorSettings[3]);
                            break;
                    }
                }
                #endregion

                #region Settings for the chamber
                if (settingsInFile[x].Contains("chamberConnection"))
                {
                    char[] splitter = new char[3] { '=', ',', ':' };
                    string[] chamberSettings = settingsInFile[x].Split(splitter);

                    switch (chamberSettings[1])
                    {
                        case "Thermotron":
                            settingsEquipment.chamberIP = chamberSettings[2];
                            settingsEquipment.chamberPort = Convert.ToInt16(chamberSettings[3]);
                            break;
                    }
                }
                #endregion

                #region Setting for the system level relays
                if (settingsInFile[x].Contains("relaySystemConnection")) //Breeking down the relay information for the system level relays and loading into the settings object.
                {
                    //Breaking down the line and getting the information for each element needed.
                    char[] splitter = new char[2] { '=', ',' };
                    string[] relaySystemSettings = settingsInFile[x].Split(splitter);

                    settingsEquipment.relaySystem.relayID = "System"; //Name
                    settingsEquipment.relaySystem.relaySystemEquipmentIP = relaySystemSettings[1]; //ipAddress
                    settingsEquipment.relaySystem.relaySystemEquipmentPort = Convert.ToInt16(relaySystemSettings[2]); //Port
                    

                }

                //Loading the addresses and functions for the relays into the system level relay.
                if (settingsInFile[x].Contains("relaySystemAddress"))
                {
                    //Making sure the list is there.
                    if (settingsEquipment.relaySystem.relaySystemEquipmentAddresses == null)
                    {
                        settingsEquipment.relaySystem.relaySystemEquipmentAddresses = new List<string>();
                    }
                    string[] relaySystemSettings = settingsInFile[x].Split('=');

                    if (!settingsEquipment.relaySystem.relaySystemEquipmentAddresses.Contains(relaySystemSettings[0].Substring(relaySystemSettings[0].Length-2,2) + "," + relaySystemSettings[1])) //Load the relay address if not found.
                    {
                        settingsEquipment.relaySystem.relaySystemEquipmentAddresses.Add(relaySystemSettings[0].Substring(relaySystemSettings[0].Length-2,2) + "," + relaySystemSettings[1]);
                    }

                }
                #endregion

                #region Setting for pressure manifolds relays.
                //If a connection is found that create the settings object and call it the ID that follows "relayManifoldConnnection"
                if (settingsInFile[x].Contains("relayManifoldConnnection"))
                {
                    //Breaking down the line and getting the information for each element needed.
                    char[] splitter = new char[2] { '=', ',' };
                    string[] relaySettings = settingsInFile[x].Split(splitter);

                    RelayController.relaySettings currentFound = new RelayController.relaySettings(); //Creating the temp object that will be added to the list.
                    currentFound.relayID = relaySettings[0].Substring(relaySettings[0].Length - 2, 2);
                    currentFound.relaySystemEquipmentIP = relaySettings[1];
                    currentFound.relaySystemEquipmentPort = Convert.ToInt16(relaySettings[2]);
                    currentFound.relaySystemEquipmentAddresses = new List<string>(); //Init the list of relay settings.
                    currentFound.relayType = relaySettings[3];      //Adding the type of relay controller to the settings.

                    //Loading all relays connected with the current relay id.
                    foreach (string data in settingsInFile)
                    {
                        if (data.Contains("relayManifold" + currentFound.relayID + "Address") && !data.Contains('#'))
                        {
                            string[] addressLine = data.Split(splitter);

                            if(!currentFound.relaySystemEquipmentAddresses.Contains(addressLine[0].Substring(addressLine[0].Length-2,2) + "," + addressLine[1] + "," + addressLine[2]))
                            {
                                currentFound.relaySystemEquipmentAddresses.Add(addressLine[0].Substring(addressLine[0].Length - 2, 2) + "," + addressLine[1] + "," + addressLine[2]);
                            }
                        }
                    }

                    settingsEquipment.relayManifolds.Add(currentFound); //Adding to overall settings.

                }
                #endregion

                #region Settings for racks, runners, and radiosonde ports.

                if (settingsInFile[x].Contains("rackID"))
                {
                    //init rack list if needed.
                    if (settingsEquipment.racks == null)
                    {
                        settingsEquipment.racks = new List<rackCalibration.rackSettings>();
                    }

                    char[] splitter = new char[2] { '=', ','};
                    string[] rackSettings = settingsInFile[x].Split(splitter);

                    //Setting up temp object
                    rackCalibration.rackSettings currentRack = new rackCalibration.rackSettings();
                    currentRack.rackID = rackSettings[1];
                    currentRack.runnerSettings = new List<string>();

                    //Loading runner information pertaining to this rack.
                    foreach (string data in settingsInFile)
                    {
                        if (data.Contains("runner" + currentRack.rackID) && !data.Contains('#'))
                        {
                            string[] line = data.Split('=');
                            if (!currentRack.runnerSettings.Contains(line[1]))
                            {
                                currentRack.runnerSettings.Add(line[0].Substring(line[0].Length-2,2) + "," +line[1] + "=" + line[2]);
                            }
                        }
                    }

                    //Adding that fully composed rack object to global settings.
                    settingsEquipment.racks.Add(currentRack);

                }

                #endregion

                #region Setting the log loc. and cal loc

                if (settingsInFile[x].Contains("LOGLOC"))
                {
                    char[] splitter = new char[2] { '=', ',' };
                    string[] logLOCSettings = settingsInFile[x].Split(splitter);

                    settingsEquipment.LOGLOC = logLOCSettings[1];
                }

                if (settingsInFile[x].Contains("calLOC"))
                {
                    string[] coefLocSettings = settingsInFile[x].Split('=');
                    settingsEquipment.coefLoc = coefLocSettings[1];
                }

                #endregion

                if (settingsInFile[x].Contains("email="))
                {
                    char[] splitter = new char[2] { '=', ',' };
                    string[] logLOCSettings = settingsInFile[x].Split(splitter);

                    emailAdminAddress = logLOCSettings[1];
                }
            }

        }

        public void configurePressureSensor()
        {
            //Checking to see if the sensor has already been started, and if so null it.
            if (sensorPressure == null)
            {
                sensorPressure = null;
            }

            //Connecting to the Paro via TCP/IP
            //sensorPressure = new TestEquipment.Paroscientific(settingsEquipment.pressureSensorIP, settingsEquipment.pressureSensorPort);
            sensorPressure = new equipmentTCPIP.equipmentParoTCPIP(settingsEquipment.pressureSensorIP, settingsEquipment.pressureSensorPort);
            Program.sensorPressure = sensorPressure;

            //sensorPressure.connect();   //Need for serial connection.
            //sensorPressure.Initiate();
            sensorPressure.updatedPressureValue.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(updatedPressureValue_PropertyChange);
            
            //Setting up the pressure referance data collection. <=might want to move this to the referacnce pressure object. I don't know.
            
            //backgroundWorkerGetReferancePressure = new BackgroundWorker();
            //backgroundWorkerGetReferancePressure.WorkerSupportsCancellation = true;
            //backgroundWorkerGetReferancePressure.DoWork += new DoWorkEventHandler(backgroundWorkerGetReferancePressure_DoWork);
            //backgroundWorkerGetReferancePressure.RunWorkerAsync();

            sensorPressure.startCollect(); 
            
        }

        private void configureRelaySystems()
        {
            relaySystemEquipment = new RelayController.relayControllerSystem();
            relaySystemEquipment.relayController(settingsEquipment.relaySystem.relaySystemEquipmentIP, settingsEquipment.relaySystem.relaySystemEquipmentPort);
            foreach (string device in settingsEquipment.relaySystem.relaySystemEquipmentAddresses)
            {
                string[] line = device.Split(',');
                relaySystemEquipment.setRelayFunction(Convert.ToInt16(line[0]), line[1], line[2]);
            }


        }

        private void configureManifoldRelaySystem()
        {
            //Starting up the array of relay controllers for the pressure manifolds.
            relayPressureManifold = new RelayController.relayControllerSystem[settingsEquipment.relayManifolds.Count];

            //Building the relay controller objects
            for (int st = 0; st < settingsEquipment.relayManifolds.Count; st++)
            {
                RelayController.relayControllerSystem tempRelay = new RelayController.relayControllerSystem();
                tempRelay.setRelayType(settingsEquipment.relayManifolds[st].relayType);
                tempRelay.relayController(settingsEquipment.relayManifolds[st].relaySystemEquipmentIP, settingsEquipment.relayManifolds[st].relaySystemEquipmentPort);
                foreach (string device in settingsEquipment.relayManifolds[st].relaySystemEquipmentAddresses)
                {
                    string[] line = device.Split(',');
                    tempRelay.setRelayFunction(Convert.ToInt16(line[0]), line[1], line[2]);
                }

                //Moving temprelay setup to the global.
                relayPressureManifold[st] = tempRelay;
                //Moving to a program level object
                Program.pRelay = tempRelay;
            }

            //Some test code.
            /*
            foreach (RelayController.relayControllerSystem rl in relayPressureManifold)
            {
                string[] ports = rl.RelayFunction();

                foreach (string port in ports)
                {
                    rl.commandRelayBoard(port, true);
                    System.Threading.Thread.Sleep(3000);
                    rl.commandRelayBoard(port, false);
                    
                }
            }
            */

            //Closing all manifold ports
            //Needs to close any open manifild ports.
            foreach (RelayController.relayControllerSystem re in relayPressureManifold)
            {
                string[] relayFunctions = re.RelayFunction();
                foreach (string relay in relayFunctions)
                {
                    re.commandRelayBoard(relay, false);
                }
            }


            //Telling the system that the manifold is realy for processing

        }

        private void configureRadiosondeRacks()
        {
            //Starting the main rack.
            racks = new rackCalibration.rackPressure[settingsEquipment.racks.Count];

            char[] splitter = new char[4] { '=', ',', '|', ':'};

            for (int r = 0; r < racks.Length; r++)
            {
                //Setting up the rack.
                racks[r] = new rackCalibration.rackPressure(settingsEquipment.racks[r].rackID);

                //Looping through the runners.
                for (int rCount = 0; rCount < settingsEquipment.racks[r].runnerSettings.Count; rCount++)
                {
                    //Breaking down the runner line
                    string[] rackLine = settingsEquipment.racks[r].runnerSettings[rCount].Split(splitter);
                    
                    List<string> ipAddress = new List<string>();
                    List<int> port = new List<int>();

                    //Looping through the items for the runner.
                    for (int items = 0; items < rackLine.Length; items++)
                    {
                        if (items > 2)
                        {
                            if (items % 2 == 0)
                            {
                                port.Add(Convert.ToInt16(rackLine[items]));
                            }
                            else
                            {
                                ipAddress.Add(rackLine[items]);
                            }
                        }
                    }

                    //Sending the settings out to the rack objects.
                    try
                    {
                        BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { (rackLine[0] + "-" + rackLine[1] + "Added") });
                    }
                    catch
                    {
                        System.Threading.Thread.Sleep(250);
                        BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { (rackLine[0] + "-" + rackLine[1] + "Added") });

                    }
                    racks[r].addRunner(rackLine[1], ipAddress, port);
                    
                }

            }

            //Starting listening for radiosondes coming online and disconnecting on all the runners
            foreach (rackCalibration.rackPressure r in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure cuRunner in r.runners)
                {
                    cuRunner.radiosondePresent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(radiosondePresent_PropertyChange);


                    foreach (ipRadiosonde.ipUniRadiosonde thePorts in cuRunner.runnerRadiosondePorts)
                    {
                        //if (thePorts != null)
                        //{
                        //thePorts.bootingRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(bootingRadiosonde_PropertyChange);
                        thePorts.readyRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(readyRadiosonde_PropertyChange);
                        thePorts.disconnectRadiosonde.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(disconnectRadiosonde_PropertyChange);
                        //thePorts.CalDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(CalDataUpdate_PropertyChange);
                        thePorts.error.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(error_PropertyChange);
                        
                        //Enableing the ptu and gps packets events.
                        thePorts.GPSDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(GPSDataUpdate_PropertyChange);
                        thePorts.PTUDataUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(PTUDataUpdate_PropertyChange);

                        thePorts.autoStartCalMode(true);

                        //}
                    }
                }
            }


            //Debugging stuff
            System.ComponentModel.BackgroundWorker Checking = new BackgroundWorker();
            Checking.DoWork += new DoWorkEventHandler(Checking_DoWork);
            Checking.RunWorkerAsync();

            //Converting the rack from array to list
            List<rackCalibration.rackPressure> outRack = new List<rackCalibration.rackPressure>();
            foreach (rackCalibration.rackPressure r in racks)
            {
                outRack.Add(r);
            }

            pressureDisplay.addToMain.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(addToMain_PropertyChange);

            //setupRadiosondeDisplay(); 
            pressureDisplay.reloadCalDisplay(panelChamberDisplay, outRack, relayPressureManifold);

            //Setting up the manifold.
            //addManToDisplay(); //Doing this casuse the software to crash in XP

            
            //Program loaded and ready.
            programReady.UpdatingObject = (object)"I doesn't matter this data is thrown away.";
        }

        /// <summary>
        /// Method adds manifold to the display. However in XP it causing the system to crash.
        /// </summary>
        /*
        private void addManToDisplay()
        {
            for (int pr = 0; pr < relayPressureManifold.Length; pr++)
            {
                List<string> portNames = new List<string>(0);
                string[] relayFunction = relayPressureManifold[pr].RelayFunction();

                for (int fu = 0; fu < relayFunction.Length; fu++)
                {
                    if (relayFunction != null)
                    {
                        portNames.Add(relayFunction[fu]);
                    }
                }

                pressureDisplay.portManifold.addManifoldBlock(portNames, relayPressureManifold);

            }
        }
        */

        void addToMain_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { (rackLine[1] + "Added") });
            BeginInvoke(new updateMainPanel(this.addControlToMain), new object[] { (object)data.NewValue });
        }

        private void addControlToMain(object incomingItem)
        {
            panelChamberDisplay.Controls.Add((System.Windows.Forms.Panel)incomingItem);
        }

        void Checking_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                BeginInvoke(new sendABool(this.updateRadiosondeCount), new object[] { true });
                System.Threading.Thread.Sleep(20000);
            }
        }



        #region Methods for interacting with the radiosondes

        void radiosondePresent_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            runnerPressureAssembly.dataPacketPressureRunner incomingData = (runnerPressureAssembly.dataPacketPressureRunner)data.NewValue;

            string outgoingMessage = getFormattedTime() + " - " +  incomingData.runnerID + " : " + incomingData.message;
            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { outgoingMessage });
        }

        void error_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            SystemException incomingError = (SystemException)data.NewValue;
            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { getFormattedTime() + " - " + incomingError.Message });
        }

        void CalDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            ipRadiosonde.CalData incomingData = (ipRadiosonde.CalData)data.NewValue;

            string outgoingMessage = getFormattedTime() + " - " + incomingData.SerialNumber + " " + incomingData.PacketNumber.ToString() + " " + incomingData.Temperature.ToString();

            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { outgoingMessage });
        }

        /// <summary>
        /// Event fires when radiosonde disconnects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        void disconnectRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            if (processPressure != null)    //checking to see if a calibration object is created.
            {
                //calibrationUnderWay, loggingData, currentCalibrationStep, startOfCalibration, startOfCalibrationPoint
                object[] status = processPressure.getCalStatus();   //Getting the calibration status
                if (Convert.ToBoolean(status[0]))   //Checking to see if the calibration is underway.
                {



                    ipRadiosonde.radiosondeDataPacket incomingData = (ipRadiosonde.radiosondeDataPacket)data.NewValue;
                    string outgoingMessage = getFormattedTime() + " - " + incomingData.ipAddress.ToString() + ":" + incomingData.port.ToString() + "  " + incomingData.packetMessage;
                    BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { outgoingMessage });

                    List<string> currentRadiosondes = new List<string>();

                    //Getting the radiosondes serial numbers currently online.
                    foreach (rackCalibration.rackPressure r in racks)
                    {
                        foreach (runnerPressureAssembly.runnerPressure run in r.runners)
                        {
                            foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)
                            {
                                if (sonde.CalData.SerialNumber != "")
                                {
                                    currentRadiosondes.Add(sonde.CalData.SerialNumber);
                                }
                            }
                        }
                    }

                    string[] groupFiles = System.IO.Directory.GetFiles(processPressure.CalibrationLogLOC, "*.glog");    //Loading in group files.

                    List<GroupFileData> groupInfo = new List<GroupFileData>();

                    for (int i = 0; i < groupFiles.Length; i++)
                    {
                        GroupFileData tempData = new GroupFileData();
                        string[] fileData = System.IO.File.ReadAllLines(groupFiles[i]);
                        tempData.groupFileName = groupFiles[i];

                        for (int d = 0; d < fileData.Length; d++)
                        {
                            if (fileData[d].Contains(".p_raw"))
                            {
                                tempData.rawFiles.Add(fileData[d]);
                            }

                            if (fileData[d].Contains(".p_ref"))
                            {
                                tempData.referenceFile = fileData[d];
                            }
                        }

                        groupInfo.Add(tempData);
                    }

                    foreach (GroupFileData gd in groupInfo)
                    {
                        for (int i = 0; i < gd.rawFiles.Count(); i++)
                        {
                            for (int s = 0; s < currentRadiosondes.Count(); s++)
                            {
                                if (gd.rawFiles[i].Contains(currentRadiosondes[s]))
                                {
                                    gd.rawFiles[i] = "";
                                }
                            }
                        }
                    }

                    //Removing the remaining files from the glog files.
                    for (int i = 0; i < groupFiles.Length; i++)
                    {
                        string[] dataLines = System.IO.File.ReadAllLines(groupFiles[i]);   //Loading the data.
                        List<string> outPutData = new List<string>();       //Output container.
                        
                        for(int d = 0; d< dataLines.Length; d++)
                        {
                            for(int rf = 0; rf< groupInfo[i].rawFiles.Count(); rf++)
                            {
                                if(dataLines[d] == groupInfo[i].rawFiles[rf])
                                {
                                    dataLines[d] = "";
                                }
                            }
                        }

                        foreach(string dl in dataLines)
                        {
                            if (dl != "")
                            {
                                System.Diagnostics.Debug.WriteLine("Removing: " + dl);
                                outPutData.Add(dl);
                            }
                        }

                        System.IO.File.WriteAllLines(groupFiles[i], outPutData.ToArray());

                    }


                    System.Diagnostics.Debug.WriteLine("Done");
                }
            }
           
        }


        void bootingRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            ipRadiosonde.radiosondeDataPacket incomingData = (ipRadiosonde.radiosondeDataPacket)data.NewValue;

            string outgoingMessage = getFormattedTime() + " - " + incomingData.ipAddress.ToString() + ":" + incomingData.port.ToString() + "  " + incomingData.packetMessage;

            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { outgoingMessage });
        }

        void readyRadiosonde_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            ipRadiosonde.radiosondeDataPacket incomingData = (ipRadiosonde.radiosondeDataPacket)data.NewValue;

            string outgoingMessage = getFormattedTime() + " - " + incomingData.ipAddress.ToString() + ":" + incomingData.port.ToString() + "  " + incomingData.packetMessage;

            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { outgoingMessage });

            //Setting the newly connected radiosonde to calmode=on           
            
            foreach (rackCalibration.rackPressure r in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure run in r.runners)
                {
                    foreach (ipRadiosonde.ipUniRadiosonde radio in run.runnerRadiosondePorts)
                    {
                        if (radio.ipAddress == incomingData.ipAddress && radio.port == incomingData.port)
                        {
                            System.Threading.Thread.Sleep(5000);
                            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { (getFormattedTime() + " - " + "Calmode started on: " + incomingData.ipAddress + ":" + incomingData.port.ToString()) });
                            radio.setManufacturingMode(true);
                        }
                    }
                }

            }
            
        }

        void PTUDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            updateCreater.objectUpdate incoming = (updateCreater.objectUpdate)sender;

            string[] id = incoming.objectID.Split(':');

            //Starting listening for radiosondes coming online and disconnecting on all the runners
            foreach (rackCalibration.rackPressure r in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure cuRunner in r.runners)
                {
                    cuRunner.radiosondePresent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(radiosondePresent_PropertyChange);

                    foreach (ipRadiosonde.ipUniRadiosonde thePorts in cuRunner.runnerRadiosondePorts)
                    {
                        if (thePorts.ipAddress == id[0] && thePorts.port == Convert.ToInt16(id[1]))
                        {
                            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { (getFormattedTime() + " - " + "Radiosonde PTU Packet Detected on: " + incoming.objectID) });
                            thePorts.setPTUMode(false);
                        }
                    }
                }
            }
        }

        void GPSDataUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            updateCreater.objectUpdate incoming = (updateCreater.objectUpdate)sender;

            string[] id = incoming.objectID.Split(':');

            //Starting listening for radiosondes coming online and disconnecting on all the runners
            foreach (rackCalibration.rackPressure r in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure cuRunner in r.runners)
                {
                    cuRunner.radiosondePresent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(radiosondePresent_PropertyChange);

                    foreach (ipRadiosonde.ipUniRadiosonde thePorts in cuRunner.runnerRadiosondePorts)
                    {
                        if (thePorts.ipAddress == id[0] && thePorts.port == Convert.ToInt16(id[1]))
                        {
                            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { (getFormattedTime() + " - " + "Radiosonde GPS Packet Detected on: " + incoming.objectID ) });
                            thePorts.setGPSMode(false);
                        }
                    }
                }
            }
        }




        private string getFormattedTime()
        {
            return DateTime.Now.ToString("HH:mm:ss.ffff");
        }

        #endregion

        private void configurePressureManager()
        {
            //init the manager object
            managerPressure = new ManagerPresssure.pressureManager();
            Program.pManager = managerPressure;
            managerPressure.setReferancePressureSensor(sensorPressure); //Sending the sensor that will be used.
            managerPressure.setPressureManagerRelay(relaySystemEquipment); //Sending the relays needed to contol the pressure enviroment.
            managerPressure.setDesiredPressureEnviroment(3, 1050, 800, 60); //Most basic starting point.
            managerPressure.setDesiredPressureStability(-0.010, 0.010, 0, 60); //Normal pressure stablilty I look for.

            //The following two lines connect events the manager does produce. I might want to use them but unsure right now.
            //managerPressure.gotoPressureEvent.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(gotoPressureEvent_PropertyChange);
        }

        private void configureChamber()
        {
            if (chamber != null)
            {
                
                chamber.statusPacket.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusPacket_PropertyChange);
                chamber.chamberDataError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(chamberDataError_PropertyChange);

                chamber = null;

                if (backgroundWorkerGetChamberStatus.IsBusy)
                {
                    backgroundWorkerGetChamberStatus.CancelAsync();
                }
                System.Threading.Thread.Sleep(1000);
            }

            //Connecting to Chamber.
            if (settingsEquipment.chamberIP == null)    //Checking to see if the chamber is disabled.
            {
                MessageBox.Show("Chamber disabled", "Chamber Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                chamber = new TestEquipment.Thermotron(settingsEquipment.chamberIP, settingsEquipment.chamberPort);
                chamber.statusPacket.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPacket_PropertyChange);
                chamber.chamberDataError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(chamberDataError_PropertyChange);

                //setting chamber to the right mode
                chamber.setOptions(33); //Setting the chamber to product temp control with the cascade cooler on.

                //Starting background worker to run request to the chamber
                backgroundWorkerGetChamberStatus = new BackgroundWorker();
                backgroundWorkerGetChamberStatus.WorkerSupportsCancellation = true;
                backgroundWorkerGetChamberStatus.DoWork += new DoWorkEventHandler(backgroundWorkerGetChamberStatus_DoWork);
                backgroundWorkerGetChamberStatus.RunWorkerAsync();
            }
        }

        #endregion

        #region Support equipment threads
        
        /*
        void backgroundWorkerGetReferancePressure_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!backgroundWorkerGetReferancePressure.CancellationPending)
            {
                sensorPressure.Read();

                System.Threading.Thread.Sleep(3000);

                sensorPressure.triggerReceiveData();

            }

            try
            {
                BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { "Get Referance Pressure Background Worker Shutting Down." });
            }
            catch
            {
            }

        }
        */
        
        
        

        void updatedPressureValue_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new UpdateDisplay(this.updateDisplay), new object[] { "PressureRef" });
        }

        void backgroundWorkerGetChamberStatus_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!backgroundWorkerGetChamberStatus.CancellationPending)
            {
                chamber.getChamberStatus();
                System.Threading.Thread.Sleep(500);
            }
        }

        void statusPacket_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new UpdateDisplay(this.updateDisplay), new object[] { "Chamber" });

            if (managerPressure != null)
            {
                //Changing this to product temp.
                //managerPressure.setPressureSystemTemp(chamber.CurrentChamberC);
                managerPressure.setPressureSystemTemp(chamber.CurrentChamberPC);
            }
        }

        void chamberDataError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            SystemException incomingError = (SystemException)data.NewValue;

            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { incomingError.Message });
            

            //MessageBox.Show(incomingError.Message, "Chamber Error", MessageBoxButtons.OK);

            //Resetting the chamber.... This might cause sub-objects that use the chamber to crash I am not sure.
            //configureChamber();
        }

        void manifoldGoupChanged_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            int incomingData = (int)data.NewValue;
            string currentMan;
            if (incomingData > 0)
            {
                currentMan = (incomingData + 1).ToString();
            }
            else
            {
                currentMan = "N/A";
            }

            BeginInvoke(new UpdateDisplay(this.updateManifoldStatus), new object[] { currentMan });
        }

        void programReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new sendABool(this.contorlControls), new object[] { true });
            BeginInvoke(new sendABool(this.updateRadiosondeCount), new object[] { true });
        }

        #endregion

        #region Methods related to updating the display with data, conditions, alerts, and anything related to events happening that needed to be displayed.


        /// <summary>
        /// Method counts up the number of radiosondes currently connect and displays that on screen.
        /// </summary>
        /// <param name="visable"></param>
        private void updateRadiosondeCount(bool visable)
        {
            //Making things show up.
            label19.Visible = visable;
            labelRadiosondeCount.Visible = visable;

            int radiosondeCount = 0;
            foreach (rackCalibration.rackPressure rack in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure run in rack.runners)
                {
                    foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)
                    {
                        if (sonde.getRadiosondeConnectionStatus())
                        {
                            radiosondeCount++;
                        }
                    }
                }
            }

            labelRadiosondeCount.Text = radiosondeCount.ToString();

        }

        private void updateDisplay(string desiredUpdate)
        {
            switch (desiredUpdate)
            {
                case "PressureRef":
                    labelCurrentPressure.Text = sensorPressure.getCurrentPressure().ToString("0.000");
                    labelPressureChange.Text = managerPressure.getPressureDifferancePerSec().ToString("0.0000");
                    labelPressureSetPoint.Text = managerPressure.getDesiredPressureEnviromentSP().ToString("0.000");
                    labelPressureChangeSetPoint.Text = managerPressure.getDesiredPressureStabilitySP().ToString("0.000");
                    break;

                case "Chamber":
                    //Air Temp
                    labelChamberAirTemp.Text = chamber.CurrentChamberC.ToString("0.00");
                    labelChamberATSetPoint.Text = chamber.CurrentChamberSPC.ToString("0.00");
                    labelChamberATThrottle.Text = chamber.CurrentChamberTempTHTL.ToString("0.00") + "%";

                    //Product Temp
                    labelChamberProductTemp.Text = chamber.CurrentChamberPC.ToString("0.00");
                    labelChamberPTSetPoint.Text = chamber.CurrentChamberSPPC.ToString("0.00");
                    labelChamberPTThrottle.Text = chamber.CurrentChamberPTHTL.ToString("0.00");

                    //Humidity
                    labelChamberHumidity.Text = chamber.CurrentChamberRH.ToString("0.00") + "%";
                    labelChamberHumiditySetPoint.Text = chamber.CurrentChamberSPRH.ToString("0.00") + "%";
                    labelChamberHumidityThrottle.Text = chamber.CurrentChamberRHTHTL.ToString("0.00") + "%";
                    break;

            }
        }   

        private void updateCalibrationDisplay(string toUpdata, object data)
        {
            //Bringing in data.
            object[] calibrationStatusData = null;

            //This should stop the null object crashes.
            try
            {
                calibrationStatusData = processPressure.getCalStatus(); //Getting the current status from calibration.
            }
            catch
            {
                if (curValidation != null)
                {
                    calibrationStatusData = new object[5] { false, false, 2, DateTime.Now, DateTime.Now };
                }
                else
                {
                    calibrationStatusData = new object[5] { false, false, 0, DateTime.Now, DateTime.Now };
                }
            }

            object[] incomingData = (object[])data;

            //Enable group box if calibration is running.
            groupBoxCurrentCalibration.Enabled = true; //(bool)calibrationStatusData[0]; //Enabling the calibration gorup box if the calibration is running.

            //Updating the time.
            TimeSpan timePassed = DateTime.Now - (DateTime)calibrationStatusData[3];
            TimeSpan timePassedOnPoint = DateTime.Now - (DateTime)calibrationStatusData[4];
            labelCalibrationRunTime.Text = timePassed.Hours.ToString("00") + ":" + timePassed.Minutes.ToString("00") + ":" + timePassed.Seconds.ToString("00");
            labelCalibrationStepRunTime.Text = timePassedOnPoint.Hours.ToString("00") + ":" + timePassedOnPoint.Minutes.ToString("00") + ":" + timePassedOnPoint.Seconds.ToString("00");

            //Sending calibration step to the screen.
            labelCalibrationCurrentStep.Text = (Convert.ToInt16(calibrationStatusData[2]) + 1).ToString();

            if (currentCalibration != null)
            {
                labelCalibrationTotalSteps.Text = currentCalibration.Count.ToString();
            }

            switch (toUpdata)
            {
                case "ChamberStability":
                    labelCalibrationATStability.Text = Convert.ToInt16(incomingData[1]).ToString();
                    try
                    {
                        labelCalibrationATRate.Text = processPressure.managerChamber.getCurrentStabilityAvg().ToString("0.000");
                    }
                    catch
                    {
                        labelCalibrationATRate.Text = "N/A";
                    }
                    break;

                case "ChamberCondition":
                    labelCalibrationATRange.Text = Convert.ToInt16(incomingData[1]).ToString();
                    break;

                case "PressureStability":
                    labelCalibrationElementStability.Text = Convert.ToInt16(incomingData[1]).ToString();
                    labelCalibrationPressureRate.Text = managerPressure.getPressureDifferancePerSec().ToString("0.000");
                    break;

                case "PressureCondition":
                    labelCalibrationElementRange.Text = Convert.ToInt16(incomingData[1]).ToString();
                    break;

                case "ChamberTH":
                    try
                    {
                        labelCalibrationTHCount.Text = processPressure.managerChamber.thBoxCar.getCurrentBoxCar().ToString("0.000") + "/" + Convert.ToInt16(incomingData[1]).ToString();
                    }
                    catch
                    {
                        labelCalibrationTHCount.Text = "N/A";
                    }
                    break;

            }

            int mode = 0;
            mode = Convert.ToInt16(calibrationStatusData[2]);
            

            //Updating the mode panel.
            updateCalibrationMode(mode);

        }

        private void updateCalibrationMode(int mode)
        {
            //Updating the mode panel.
            switch (mode)
            {
                case -1:
                    labelCurrentMode.Text = "Pre-Cal";
                    panelCurrentMode.BackColor = System.Drawing.Color.Yellow;
                    break;

                case -2:
                    labelCurrentMode.Text = "Stopped";
                    panelCurrentMode.BackColor = System.Drawing.Color.Red;
                    break;

                case 0:
                    labelCurrentMode.Text = "Calibration";
                    panelCurrentMode.BackColor = System.Drawing.Color.Green;
                    break;

                case 2:
                    labelCurrentMode.Text = "Post-Cal";
                    panelCurrentMode.BackColor = System.Drawing.Color.LightBlue;
                    break;
            }
        }

        private void updataMainStatus(string message)
        {
            toolStripStatusLabelMain.Text = message;

            //Passing updates to main status display to debug window.
            updateCreater.PropertyChangeEventArgs passingData = new updateCreater.PropertyChangeEventArgs(message, (object)message, (object)message);
            statusUpdate_PropertyChange(null, passingData);
        }

        private void updateDebug(string line)
        {
            textBoxDebug.AppendText(line + "\n");
            System.IO.File.AppendAllText("debug.log", line + "\r\n");

            if (processPressure != null)
            {
                if (processPressure.CalibrationLogLOC != "")
                {
                    string whereToWrite = "";
                    if (!processPressure.CalibrationLogLOC.EndsWith("\\"))
                    {
                        whereToWrite = processPressure.CalibrationLogLOC + "\\debug.log";
                    }
                    else
                    {
                        whereToWrite = processPressure.CalibrationLogLOC + "debug.log";
                    }

                    System.IO.File.AppendAllText(whereToWrite, line + "\r\n");
                }
            }

            toolStripStatusLabelMain.Text = line;
        }

        private void updateManifoldStatus(string data)
        {
            labelCurrentGroup.Text = data;
        }

        /// <summary>
        /// Method for enable and disableing controls.
        /// </summary>
        /// <param name="desiredState"></param>
        private void contorlControls(bool desiredState)
        {
            buttonStartCalibration.Enabled = desiredState;
            
            
            if (desiredState)
            {
                labelLoadingDisplay.Visible = false;
                labelCurrentMode.Text = "Ready";
            }
        }

        #endregion

        #region Methods for controling the program. Events and methods supporting those events.

        /// <summary>
        /// This button starts the own process of the calibration. It will setup all the calibration points to be met and the items that will watch the calibration and monior the system.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStartCalibration_Click(object sender, EventArgs e)
        {
            if (buttonStartCalibration.Text == "Start")
            {
                //Starting the calibration
                startCalibration();
                buttonStartCalibration.Text = "Cancel";
            }
            else
            {
                //Manual shutdown

                DialogResult shutdown = MessageBox.Show("Are you sure?", "Shutdown", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2);

                if (shutdown == DialogResult.No)
                {
                    return;
                }

                processPressure.setErrorMode(); //Setting the current calibration error state to true.
                stopCalibration();  //Stopping the calibration connections and sending the message to the calibration process.

                buttonStartCalibration.Text = "Start";
            }


        }

        private void startCalibration()
        {
            //Loading selected calibration into memory
            loadCalibrationSPFile(AppDomain.CurrentDomain.BaseDirectory + comboBoxCalibrationSettings.Text);
            processPressure.statusUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange); //Connecting to status for calibration.
            //processPressure.Start("test", null, settingsEquipment.LOGLOC); //This is for testing pre-caibration
            processPressure.Start(comboBoxMode.Text.ToLower(), currentCalibration, settingsEquipment.LOGLOC);   //Starting calibration
            processPressure.vaildateCalibration = checkBoxValidate.Checked;     //Sending where a validation is requested or not.

            //Connecting to the chamber status monitoring.
            processPressure.managerChamber.statusChamberCondition.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberCondition_PropertyChange);
            processPressure.managerChamber.statusChamberStability.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberStability_PropertyChange);
            processPressure.managerChamber.statusChamberTH.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberTH_PropertyChange);

            //connecting to pressure status monitoring.
            managerPressure.statusPressureCondition.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureCondition_PropertyChange);
            managerPressure.statusPressureStability.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureStability_PropertyChange);
            managerPressure.managerError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange);

            //Hooking up to the manifold change event
            processPressure.manifoldGoupChanged.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(manifoldGoupChanged_PropertyChange);

            //subscribing to the end of the calibration event/
            processPressure.calibrationComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(calibrationComplete_PropertyChange);

            //Graphic events.
            processPressure.manifoldGoupUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(manifoldGoupUpdate_PropertyChange);
            processPressure.preCalReport.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(preCalReport_PropertyChange);
            processPressure.calibrationError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(calibrationError_PropertyChange);
        }

        private void stopCalibration()
        {
            //Stopping any pressure move.
            managerPressure.stopGoToPressure();

            processPressure.statusUpdate.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange); //Connecting to status for calibration.
            processPressure.Stop();

            //Connecting to the chamber status monitoring.
            processPressure.managerChamber.statusChamberCondition.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberCondition_PropertyChange);
            processPressure.managerChamber.statusChamberStability.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberStability_PropertyChange);
            processPressure.managerChamber.statusChamberTH.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberTH_PropertyChange);

            //connecting to pressure status monitoring.
            managerPressure.statusPressureCondition.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureCondition_PropertyChange);
            managerPressure.statusPressureStability.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureStability_PropertyChange);
            managerPressure.managerError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange);

            //Disconnecting from them manifold change event.
            processPressure.manifoldGoupChanged.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(manifoldGoupChanged_PropertyChange);

            //Disconnecting from the end of calibration event
            processPressure.calibrationComplete.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(calibrationComplete_PropertyChange);

            //Graphic events.
            processPressure.manifoldGoupUpdate.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(manifoldGoupUpdate_PropertyChange);
            processPressure.preCalReport.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(preCalReport_PropertyChange);

            //Killing the current presser calibration and cleaning up. This should make it ready for the next calibration.
            processPressure = null;
            GC.Collect();

            //Resetting the user interface.

        }

        /// <summary>
        /// Method loads the calibrations settings from the givin file and loads them into the current calibration list.
        /// </summary>
        /// <param name="fileName"></param>
        private List<Calibration.calibraitonPoint> loadCalibrationSPFile(string fileName)
        {
            //Checking and resetting list array if needed.
            if (currentCalibration != null)
            {
                currentCalibration = null;
            }

            currentCalibration = new List<Calibration.calibraitonPoint>();

            //Loading all the lines of the raw calibration config into an array for processing.
            string[] rawCalData = System.IO.File.ReadAllLines(fileName);

            List<string> AirTemps = new List<string>();
            List<string> Element = new List<string>();

            for (int x = 0; x < rawCalData.Length; x++)
            {
            restartReading:

                //If the line starts with a # we are going to skip it
                if (rawCalData[x].Contains("#"))
                {
                    if (x + 1 < rawCalData.Length)
                    {
                        x++;
                        goto restartReading;
                    }
                }

                //Loading in all the pressure set points.
                if (rawCalData[x].Contains("PSP"))
                {
                    if (!Element.Contains(rawCalData[x]))
                    {
                        Element.Add(rawCalData[x]);
                    }
                }

                //Setting up the air temp points. These are the main points that break up the calibration points.
                if (rawCalData[x].Contains("TSP"))
                {
                    if (!AirTemps.Contains(rawCalData[x]))
                    {
                        AirTemps.Add(rawCalData[x]);
                    }
                }

            }

            //Compiling the points into the full cal point

            int indexNumber = 0;

            foreach (string AT in AirTemps)
            {
                

                foreach (string elm in Element)
                {
                    string[] atLine = AT.Split(',');
                    string[] elementLine = elm.Split(',');
                    Calibration.calibraitonPoint newPoint = new Calibration.calibraitonPoint();

                    //Air Temp Setpoint
                    newPoint.index = indexNumber;
                    newPoint.airTempSetPoint = Convert.ToDouble(atLine[2]);
                    newPoint.airTempLower = Convert.ToDouble(atLine[3]);
                    newPoint.airTempUpper = Convert.ToDouble(atLine[4]);
                    newPoint.packetCountAT = Convert.ToInt16(atLine[5]);

                    //Air Temp Throutl
                    newPoint.airTHSetPoint = Convert.ToDouble(atLine[6]);
                    newPoint.airTHLower = Convert.ToDouble(atLine[7]);
                    newPoint.airTHUpper = Convert.ToDouble(atLine[8]);
                    newPoint.packetCountTH = Convert.ToInt16(atLine[9]);

                    //Pressure/Element point
                    newPoint.pressureSetPoint = Convert.ToDouble(elementLine[2]);
                    newPoint.pressureLower = Convert.ToDouble(elementLine[3]);
                    newPoint.pressureUpper = Convert.ToDouble(elementLine[4]);
                    newPoint.packetCountPressure = Convert.ToInt16(elementLine[5]);

                    //Pressure stability settings.
                    newPoint.pressureStabilitySetPoint = Convert.ToDouble(elementLine[6]);
                    newPoint.pressureStabilityLower = Convert.ToDouble(elementLine[7]);
                    newPoint.pressureStabilityUpper = Convert.ToDouble(elementLine[8]);
                    newPoint.packetCountPressureStability = Convert.ToInt16(elementLine[9]);

                    //Adding the point to the master.
                    currentCalibration.Add(newPoint);

                    //Moving the counter.
                    indexNumber++;
                }

            }

            //Now that everthing is loaded and sorted. It is time to setup the calibration objects and load in all the cal points.
            processPressure = new Calibration.calibrationPressure(sensorPressure, managerPressure, chamber, racks, relayPressureManifold);
            processPressure.calPoints = currentCalibration;

            return currentCalibration;

        }

        /// <summary>
        /// Method loads the calibrations settings from the givin file and loads them into the current calibration list.
        /// </summary>
        /// <param name="fileName"></param>
        private List<ValidateCalibration.validatePoint> loadValidateSPFile(string fileName)
        {
            List<ValidateCalibration.validatePoint> curValidate = new List<ValidateCalibration.validatePoint>();

            //Loading all the lines of the raw calibration config into an array for processing.
            string[] rawCalData = System.IO.File.ReadAllLines(fileName);

            List<string> AirTemps = new List<string>();
            List<string> Element = new List<string>();

            for (int x = 0; x < rawCalData.Length; x++)
            {
            restartReading:

                //If the line starts with a # we are going to skip it
                if (rawCalData[x].Contains("#"))
                {
                    x++;
                    goto restartReading;
                }

                //Loading in all the pressure set points.
                if (rawCalData[x].Contains("PSP"))
                {
                    if (!Element.Contains(rawCalData[x]))
                    {
                        Element.Add(rawCalData[x]);
                    }
                }

                //Setting up the air temp points. These are the main points that break up the calibration points.
                if (rawCalData[x].Contains("TSP"))
                {
                    if (!AirTemps.Contains(rawCalData[x]))
                    {
                        AirTemps.Add(rawCalData[x]);
                    }
                }

            }

            //Compiling the points into the full cal point

            int indexNumber = 0;

            foreach (string AT in AirTemps)
            {


                foreach (string elm in Element)
                {
                    string[] atLine = AT.Split(',');
                    string[] elementLine = elm.Split(',');
                    ValidateCalibration.validatePoint newPoint = new ValidateCalibration.validatePoint();

                    //Air Temp Setpoint
                    newPoint.index = indexNumber;
                    newPoint.airTempSetPoint = Convert.ToDouble(atLine[2]);
                    newPoint.airTempLower = Convert.ToDouble(atLine[3]);
                    newPoint.airTempUpper = Convert.ToDouble(atLine[4]);
                    newPoint.packetCountAT = Convert.ToInt16(atLine[5]);

                    //Air Temp Throutl
                    newPoint.airTHSetPoint = Convert.ToDouble(atLine[6]);
                    newPoint.airTHLower = Convert.ToDouble(atLine[7]);
                    newPoint.airTHUpper = Convert.ToDouble(atLine[8]);
                    newPoint.packetCountTH = Convert.ToInt16(atLine[9]);

                    

                    //Pressure/Element point
                    newPoint.pressureSetPoint = Convert.ToDouble(elementLine[2]);
                    newPoint.pressureLower = Convert.ToDouble(elementLine[3]);
                    newPoint.pressureUpper = Convert.ToDouble(elementLine[4]);
                    newPoint.packetCountPressure = Convert.ToInt16(elementLine[5]);

                    //Pressure stability settings.
                    newPoint.pressureStabilitySetPoint = Convert.ToDouble(elementLine[6]);
                    newPoint.pressureStabilityLower = Convert.ToDouble(elementLine[7]);
                    newPoint.pressureStabilityUpper = Convert.ToDouble(elementLine[8]);
                    newPoint.packetCountPressureStability = Convert.ToInt16(elementLine[9]);

                    //Tolerance to be compared to.
                    newPoint.tolPressure = Convert.ToDouble(elementLine[10]);
                    newPoint.tolAirTemp = Convert.ToDouble(atLine[10]);
                    newPoint.tolHumidity = Convert.ToDouble(atLine[11]);



                    //Adding the point to the master.
                    curValidate.Add(newPoint);

                    //Moving the counter.
                    indexNumber++;
                }

            }

            return curValidate;

        }


        /// <summary>
        /// Loads all avaiable calibraiton configs to the display.
        /// </summary>
        private void loadAllCalibrations()
        {
            string[] calFiles = System.IO.Directory.GetFiles(System.IO.Path.GetDirectoryName(Application.ExecutablePath), "calibration*.cfg");
            
            foreach(string files in calFiles)
            {
                string[] line = files.Split('\\');
                comboBoxCalibrationSettings.Items.Add(line[line.Length-1]);
            }

            comboBoxCalibrationSettings.Text = (string)comboBoxCalibrationSettings.Items[0];
        }

        #endregion

        #region Methods for displaying status of current Calibrations.


        void statusChamberTH_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new UpdateCalibrationDisplay(this.updateCalibrationDisplay), new object[] { "ChamberTH", data.NewValue });
        }

        void statusChamberStability_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new UpdateCalibrationDisplay(this.updateCalibrationDisplay), new object[] { "ChamberStability", data.NewValue });
        }

        void statusChamberCondition_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new UpdateCalibrationDisplay(this.updateCalibrationDisplay), new object[] { "ChamberCondition", data.NewValue });

            //Sending the air temp information to the pressure manager for logging reasons.

        }

        void statusPressureStability_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new UpdateCalibrationDisplay(this.updateCalibrationDisplay), new object[] { "PressureStability", data.NewValue });
        }

        void statusPressureCondition_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            BeginInvoke(new UpdateCalibrationDisplay(this.updateCalibrationDisplay), new object[] { "PressureCondition", data.NewValue });
        }

        void calibrationComplete_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            object[] incomingData = (object[])data.NewValue;

            string mode = (string)incomingData[0];
            string incomingCalFolder = (string)incomingData[1];

            switch (mode)
            {
                case "cal":
                    {
                        BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Calibration Complete. Starting Post Process." });

                        //Loading the group log files.
                        string[] gLogFiles = System.IO.Directory.GetFiles(incomingCalFolder, "*.glog");

                        //Crearting the cal files.
                        processCalFiles(gLogFiles);

                        //Load radiosondes with coef's
                        loadAllCoef(incomingCalFolder);

                        //Test radiosondes to make sure coef received, and that the output matchs current sensor reading.
                        if (checkBoxValidate.Checked)
                        {
                            //Disconnecting from the calibration events.
                            processPressure.statusUpdate.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange); //Connecting to status for calibration.

                            //Connecting to the chamber status monitoring.
                            processPressure.managerChamber.statusChamberCondition.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberCondition_PropertyChange);
                            processPressure.managerChamber.statusChamberStability.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberStability_PropertyChange);
                            processPressure.managerChamber.statusChamberTH.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberTH_PropertyChange);

                            //connecting to pressure status monitoring.
                            managerPressure.statusPressureCondition.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureCondition_PropertyChange);
                            managerPressure.statusPressureStability.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureStability_PropertyChange);
                            managerPressure.managerError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange);

                            //Hooking up to the manifold change event
                            processPressure.manifoldGoupChanged.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(manifoldGoupChanged_PropertyChange);

                            //subscribing to the end of the calibration event/
                            processPressure.calibrationComplete.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(calibrationComplete_PropertyChange);

                            //Graphic events.
                            processPressure.manifoldGoupUpdate.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(manifoldGoupUpdate_PropertyChange);
                            processPressure.preCalReport.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(preCalReport_PropertyChange);
                            processPressure.calibrationError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(calibrationError_PropertyChange);






                            //setting up the validate objects.

                            List<ValidateCalibration.validatePoint> valRun = loadValidateSPFile(AppDomain.CurrentDomain.BaseDirectory + @"validate.cfg"); //Setting up points that will be checked.

                            //Creating val object and subbing to the event.
                            curValidation = new ValidateCalibration.validatePressureCalibration(sensorPressure, managerPressure, chamber, racks, relayPressureManifold);
                            curValidation.statusUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange); //Connecting to status for calibration.

                            //connecting to pressure status monitoring.
                            managerPressure.statusPressureCondition.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureCondition_PropertyChange);
                            managerPressure.statusPressureStability.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureStability_PropertyChange);
                            managerPressure.managerError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange);

                            curValidation.managerChamber.statusChamberCondition.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberCondition_PropertyChange);
                            curValidation.managerChamber.statusChamberStability.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberStability_PropertyChange);
                            curValidation.managerChamber.statusChamberTH.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberTH_PropertyChange);

                            //Subing to the end of the validation event.
                            curValidation.processComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(processComplete_PropertyChange);

                            curValidation.start(processPressure.calibrationGroups, valRun);


                            //Moving coef files to storage location.
                            copyCoefToNetwork(incomingCalFolder);



                        }
                        else
                        {
                            //Moving coef files to storage location.
                            copyCoefToNetwork(incomingCalFolder);

                            //Disconnecting form all the calibration events, resetting the display for the next run.
                            stopCalibration();

                            //Stopping chamber.
                            chamber.stopChamberManualMode();
                        }

                        

                        BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "All Processes Complete." });
                        break;
                    }
                case "precal":
                    {
                        BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Pre-Calibration Complete." });

                        //Disconnecting form all the calibration events, resetting the display for the next run.
                        stopCalibration();
                        break;
                    }
            }

            //Reset display.
            buttonStartCalibration.Text = "Start";

        }

        void statusUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            string incomingMessage = (string)data.NewValue; 
            DateTime time = DateTime.Now;

            BeginInvoke(new UpdateDisplay(this.updateDebug), new object[] { (time.ToString("HH:mm:ss.ffff") + " - " + incomingMessage) });


            if (incomingMessage == "Pressure move error. System will not stabilize.")
            {
                

            }
        }


        void collectGarbage_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //Cleaning up the data.
            GC.Collect();
        }

        void manifoldGoupUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            List<Calibration.runnerGroup> incomingGroup = (List<Calibration.runnerGroup>)data.NewValue;
            pressureDisplay.updateGroup(processPressure.calibrationGroups);
        }

        void preCalReport_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            object[] incomingData = (object[])data.NewValue;
            List<Calibration.runnerGroup> groups = (List<Calibration.runnerGroup>)incomingData[0];
            rackCalibration.rackPressure[] incomingRacks = (rackCalibration.rackPressure[])incomingData[1];

            string messageTotal = "Pre-Calibration Report\n";
                                
        }

        void calibrationError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incomingError = (Exception)data.NewValue;

            //Alerting operators that a calibration stopping error has occured.
            if(incomingError.Message.Contains("Calibration Error:"))
            {
                Alerts.errorDisplay calError = new Alerts.errorDisplay();
                calError.updateText("Error on: " + DateTime.Now.ToString("HH:mm:ss") + "\n" + incomingError.Message);

                Alerts.alertsEmail crashEmail = new Alerts.alertsEmail();

                //Building the message
                string outMessage = DateTime.Now.ToString("yyyyMMdd-HH:mm:ss") + " iMet-X Error" + "\r\n";
                outMessage += Application.ProductName + " : " + Application.ProductVersion.ToString() + "\r\n";
                outMessage += incomingError.Message + "\r\n";
                outMessage += incomingError.StackTrace;

                crashEmail.alertsSendEmail(emailAdminAddress, "Pressure Calibration 3 Error", outMessage, "programreport@intermetsystems.com", "mail.intermetsystems.com", "587", "programreport@intermetsystems.com", "problem@3854", false, true);

            }
        }

        #endregion

        #region Methods relateing to the menu controls.
        private void offLineCalculationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread staThread = new System.Threading.Thread(new System.Threading.ThreadStart(selectCalFiles));
            staThread.SetApartmentState(System.Threading.ApartmentState.STA);
            staThread.Start();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Loads coefficients to connected radiosondes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadCoefsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread manualLoad = new System.Threading.Thread(threadLoaderSelectFile);   //Setting up a thread to load coef.
            manualLoad.IsBackground = true;     //Setting background.
            manualLoad.SetApartmentState(System.Threading.ApartmentState.STA); //Setting to a single thread apartment (STA) mode
            manualLoad.Start();     //Starting the tread.
        }

        void threadLoaderSelectFile()
        {
            OpenFileDialog coefFiles = new OpenFileDialog();    //Open File dialog
            coefFiles.Multiselect = true;   //Allowing multipul files to be selected.
            coefFiles.Filter = "Coef Files (*.p_cal)|*.p_cal|All files (*.*)|*.*";  //Filtering to the p cal files... and all just as a catch all.

            if (DialogResult.OK == coefFiles.ShowDialog())  //Showing the open dialog.
            {
                loadAllCoef(coefFiles.FileNames);       //Sending to loader method to have the task broken up and send to the hardware.
            }
            else
            {
                MessageBox.Show("No Coefficients Loaded.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        #endregion

        #region Methods related to processing data and loading to radiosondes.

        private void selectCalFiles()
        {
            OpenFileDialog calGrounpLog = new OpenFileDialog();
            calGrounpLog.InitialDirectory = settingsEquipment.LOGLOC;
            calGrounpLog.Multiselect = true;
            calGrounpLog.Title = "Select group Log Files.";
            calGrounpLog.Filter = "Cal Group Files (*.glog)|*.glog";
            DialogResult calFileWin = calGrounpLog.ShowDialog();

            if (calFileWin == DialogResult.OK)
            {
                //Taking in the glog files selected
                string[] incomingLogFiles = calGrounpLog.FileNames;
                processCalFiles(incomingLogFiles);
            }
            else
            {
                BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Process Canceled" });
            }
        }

        void processCalFiles(string[] calLogFiles)
        {
            //Compile the list of radiosonde raw files and the referance that was used for it's cal.
            string[][] filesCompiled = sortRawFiles(calLogFiles);

            //Checking to make sure that the files exsist.
            if (!checkForRawFiles(filesCompiled))
            {
                BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Calibration files are not acceptable." });
                return;
            }

            //Processing the data.
            List<ipRadiosonde.radiosondeCalData> finalSonde = new List<ipRadiosonde.radiosondeCalData>(processRawFiles(filesCompiled));

            //Making a report.
            createCalReport(finalSonde);

            BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Process Complete" });  
        }

        private bool checkForRawFiles(string[][] filesDesired)
        {
            bool isItGood = true;
            for (int x = 0; x < filesDesired.Length; x++)
            {
                if (!System.IO.File.Exists(filesDesired[x][0]) || !System.IO.File.Exists(filesDesired[x][1]))
                {
                    isItGood = false;
                    break;
                }
            }
            return isItGood;
        }


        private string[][] sortRawFiles(string[] desiredGLog)
        {
            //Output doul array.
            List<string[]> rawFiles = new List<string[]>();

            //Getting the log files.
            foreach (string log in desiredGLog)
            {
                string[] fileData = System.IO.File.ReadAllLines(log);
                for (int x = 0; x < fileData.Length - 1; x++)
                {
                    string[] newLine = new string[2] { fileData[x], fileData[fileData.Length - 1] };
                    rawFiles.Add(newLine);
                }
            }

            //Then end product.
            return rawFiles.ToArray();
        }

        private List<ipRadiosonde.radiosondeCalData> processRawFiles(string[][] rawData)
        {
            //Setting up the processor
            Pressure_Calculator.Pressure_Calculator procCalFiles = new Pressure_Calculator.Pressure_Calculator();

            List<ipRadiosonde.radiosondeCalData> compiledResults = new List<ipRadiosonde.radiosondeCalData>();

            for (int rf = 0; rf < rawData.Length; rf++)
            {
                //Breaking down the raw file name to get the sondeID Name.
                string[] sondeIDRaw = rawData[rf][0].Split('\\');

                BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Processing: " + sondeIDRaw[sondeIDRaw.Length-1] });

                double[] coefData = procCalFiles.calculatePressureCoefficients(rawData[rf][0], rawData[rf][1]);

                char[] sliper = new char[3] { '-', '=', ',' };

                //"PASS--RMS="+RMSerror.ToString("0.000")+","+TempRMSError.ToString("0.000"); //Return example.


                string[] analyze = procCalFiles.analyzeCalibrationCoefficients(coefData, rawData[rf][0], rawData[rf][1]).Split(sliper);

                //Debug point. This will produce a ton of data for indepth analysis.
                procCalFiles.analyzeCalibrationCoefficients2(coefData, rawData[rf][0], rawData[rf][1]);

                if (analyze.Length == 5)
                {

                    //compiling the data.
                    ipRadiosonde.radiosondeCalData sondeData = new ipRadiosonde.radiosondeCalData();
                    sondeData.sondeID = sondeIDRaw[sondeIDRaw.Length - 1];
                    sondeData.sondeRawFile = rawData[rf][0];
                    sondeData.sondeRefFile = rawData[rf][1];
                    sondeData.coefData = coefData;
                    sondeData.status = analyze[0];
                    sondeData.RMSPressure = Convert.ToDouble(analyze[3]);
                    sondeData.RMSTemp = Convert.ToDouble(analyze[4]);
                    compiledResults.Add(sondeData);
                }
                else
                {
                    ipRadiosonde.radiosondeCalData sondeData = new ipRadiosonde.radiosondeCalData();
                    sondeData.sondeID = sondeIDRaw[sondeIDRaw.Length - 1];
                    sondeData.sondeRawFile = rawData[rf][0];
                    sondeData.sondeRefFile = rawData[rf][1];
                    sondeData.coefData = coefData;
                    sondeData.status = analyze[0];
                    sondeData.RMSPressure = 999.99;
                    sondeData.RMSTemp = 999.999;
                    compiledResults.Add(sondeData);
                }

                BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Processing Complete On: " + sondeIDRaw[sondeIDRaw.Length - 1] });

                string outputFile = "";

                for (int id = 0; id < sondeIDRaw.Length - 1; id++)
                {
                    outputFile += sondeIDRaw[id] + "\\";
                }

                outputFile += sondeIDRaw[sondeIDRaw.Length - 1].Substring(1, 6) + "p_cal";

                try
                {
                    procCalFiles.writePressureCalFile(rawData[rf][0], outputFile, coefData);
                    BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Writing to: " + outputFile });
                }
                catch (SystemException writeError)
                {
                    BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Unable to write: " + outputFile + " - " + writeError.Message });
                }
                
                    
            }

            return compiledResults;
        }

        private void createCalReport(List<ipRadiosonde.radiosondeCalData> incomingRadiosondes)
        {
            //Building the dirbase.
            string[] dirBuild = incomingRadiosondes[0].sondeRawFile.Split('\\');
            string outputFile = "";

            for (int x = 0; x < dirBuild.Length - 1; x++)
            {
                outputFile = outputFile + dirBuild[x] + "\\";
            }
            outputFile = outputFile + "Pressure_Report_" + DateTime.Now.ToString("dd-HHmmss") +  ".csv";

            System.IO.TextWriter tw = new System.IO.StreamWriter(outputFile);

            //Writing the lines.
            tw.WriteLine("SondeID,Status,RMS P, RMS T,"); //Heading information

            foreach (ipRadiosonde.radiosondeCalData da in incomingRadiosondes) //Radiosonde Data.
            {
                tw.WriteLine(da.sondeID + "," + da.status + "," + da.RMSPressure.ToString() + "," + da.RMSTemp.ToString() + ",");
            }

            tw.Close(); //Closing the file.

            //open Cal Report in excel.
            System.Diagnostics.Process.Start(outputFile);

        }

        private void copyCoefToNetwork(string desiredDir)
        {
            string[] files = System.IO.Directory.GetFiles(desiredDir,"*.p_cal");

            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                foreach (string f in files)
                {
                    string[] breakDown = f.Split('\\');

                    string targetLoc = settingsEquipment.coefLoc + breakDown[breakDown.Length - 1];

                    System.IO.File.Copy(f, targetLoc, true);    //Coping the coefficients to the storage loaction.

                    /*  This was found to be unneeded.
                    if (System.IO.File.Exists(targetLoc))
                    {
                        DialogResult fileCopy = MessageBox.Show(breakDown[breakDown.Length - 1] + " found in storage location.\nOverwrite?", "Copy Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

                        if (fileCopy == DialogResult.Yes)
                        {
                            System.IO.File.Copy(f, targetLoc, true);
                        }
                    }
                    else
                    {
                        System.IO.File.Copy(f, targetLoc);
                    } 
                     */ 
                }
            }
            else
            {

            }
        }

        #region Methods dealing with loading coef's to the radiosondes.

        private void selectFilesToLoad()
        {
            
        }

        System.Threading.AutoResetEvent waitTillLoaded = new System.Threading.AutoResetEvent(false);

        /// <summary>
        /// //////////////////////////////////////////////
        /// This needs to be modified to load more then one sonde at a time.
        /// //////////////////////////////////////////
        /// </summary>
        /// <param name="dirLOC"></param>
        private void loadAllCoef(string dirLOC)
        {
            int numberOfThreads = 5;

            //locating all the pressure cal files in the directory
            string[] coefFileList = System.IO.Directory.GetFiles(dirLOC, "*.p_cal");
            int count = coefFileList.Length / numberOfThreads;

            List<string[]> brDown = new List<string[]>();

            //Breaking down the list to numberOfThreads groups.
            for (int i = 0; i < numberOfThreads; i++)
            {
                if (i != numberOfThreads-1)
                {
                    string[] temp = new string[count];

                    for (int ct = 0; ct < count; ct++)
                    {
                        temp[ct] = coefFileList[ct + (count * i)];
                    }

                    brDown.Add(temp);
                }
                else
                {
                    string[] temp = new string[coefFileList.Length - (count * (numberOfThreads-1))];

                    for (int ct = 0; ct < coefFileList.Length - (count * (numberOfThreads-1)); ct++)
                    {
                        temp[ct] = coefFileList[ct + (count * i)];
                    }

                    brDown.Add(temp);
                }
            }

            BackgroundWorker[] loadCoefArray = new BackgroundWorker[numberOfThreads];

            for (int work = 0; work < loadCoefArray.Length; work++)
            {
                loadCoefArray[work] = new BackgroundWorker();
                loadCoefArray[work].DoWork += new DoWorkEventHandler(backgroudWorkerLoadToRadiosondes_DoWork);
                loadCoefArray[work].RunWorkerAsync(brDown[work]);
            }
        }

        /// <summary>
        /// Loads input files and starts upload to radiosondes.
        /// </summary>
        /// <param name="coefFiles"></param>
        private void loadAllCoef(string[] coefFiles)
        {
            int numberOfThreads = 5;

            //locating all the pressure cal files in the directory
            string[] coefFileList = coefFiles;
            int count = coefFileList.Length / numberOfThreads;

            List<string[]> brDown = new List<string[]>();

            //Breaking down the list to numberOfThreads groups.
            for (int i = 0; i < numberOfThreads; i++)
            {
                if (i != numberOfThreads - 1)
                {
                    string[] temp = new string[count];

                    for (int ct = 0; ct < count; ct++)
                    {
                        temp[ct] = coefFileList[ct + (count * i)];
                    }

                    brDown.Add(temp);
                }
                else
                {
                    string[] temp = new string[coefFileList.Length - (count * (numberOfThreads - 1))];

                    for (int ct = 0; ct < coefFileList.Length - (count * (numberOfThreads - 1)); ct++)
                    {
                        temp[ct] = coefFileList[ct + (count * i)];
                    }

                    brDown.Add(temp);
                }
            }

            BackgroundWorker[] loadCoefArray = new BackgroundWorker[numberOfThreads];

            for (int work = 0; work < loadCoefArray.Length; work++)
            {
                loadCoefArray[work] = new BackgroundWorker();
                loadCoefArray[work].DoWork += new DoWorkEventHandler(backgroudWorkerLoadToRadiosondes_DoWork);
                loadCoefArray[work].RunWorkerAsync(brDown[work]);
            }
        }

        void backgroudWorkerLoadToRadiosondes_DoWork(object sender, DoWorkEventArgs e)
        {
            string[] coefFileList = (string[])e.Argument;

            foreach (string file in coefFileList)
            {
                loadToSonde(file);
            }
        }

        private void loadToSonde(string file)
        {
            string[] fileData = System.IO.File.ReadAllLines(file);

            //Getting the sondeID from the file.
            string[] rawID = fileData[0].Split('\\');
            string sondeID = rawID[rawID.Length - 1].Substring(1, rawID[rawID.Length - 1].Length - 7);

            //finding the radiosonde in the system.
            foreach (rackCalibration.rackPressure r in racks)
            {
                foreach (runnerPressureAssembly.runnerPressure run in r.runners)
                {
                    foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)
                    {
                        //Looking for the sonde on the system that matches the file.
                        if (sonde.CalData.SondeID == sondeID)
                        {
                            sonde.autoStartCalMode(false);
                            sonde.setManufacturingMode(false);

                            BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Sending: " + file + " to " + sonde.CalData.SondeID });
                            //Loading the coef's to the sonde.
                            for (int x = 0; x < 25; x++)
                            {
                                //Loading 0-24 coef's
                                sonde.setCoefficient(x, Convert.ToDouble(fileData[x + 1]));
                                BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Writing: [" + x.ToString() + "] " + fileData[x + 1] });
                                System.Threading.Thread.Sleep(200);
                            }

                            sonde.autoStartCalMode(true);
                        }
                    }
                }
            }

            BeginInvoke(new UpdateDisplay(this.updataMainStatus), new object[] { "Coef writing complete." });
        }

        #endregion

        private void testRadiosondesToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

            loadAllCoef(@"\\sifalcon\users\Wjones\Projects\Pressure Calibration 3\Eval\20140512-1643");

            /*
            //Reruning data with corrected tol's
            List<ValidateCalibration.validatePoint> valRun = loadValidateSPFile(AppDomain.CurrentDomain.BaseDirectory + @"validate.cfg"); //Setting up points that will be checked.


            //Loading data
            List<object> localList;

            System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.Stream stream = new System.IO.FileStream(@"D:\InterMet Systems Work\IMS Software Projects\Pressure Calibration 3\Pressure Calibration 3\bin\Debug\VRunData\20140513-050333-Vrun.bin", System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
            localList = (List<object>)formatter.Deserialize(stream);
            stream.Close();


            //Sorting data
            if (curValidation == null)
            {
                curValidation = new ValidateCalibration.validatePressureCalibration();
            }
            localList = curValidation.sort(localList);


            foreach (object sp in localList)
            {
                List<ValidateCalibration.reportData> curSP = (List<ValidateCalibration.reportData>)sp;
                
                foreach (ValidateCalibration.reportData rp in curSP)
                {
                    foreach(ValidateCalibration.validatePoint val in valRun)
                    {
                        if(rp.sondeData[0].currentPoint.airTempSetPoint == val.airTempSetPoint)
                        {
                            /*
                            if (rp.sondeData[0].currentPoint.airTempSetPoint == 20)
                            {
                                System.Threading.Thread.Sleep(1000);
                            }
                             */ 
            /*
                            rp.sondeData[0].currentPoint.tolAirTemp = val.tolAirTemp;
                            if (Math.Abs(rp.sondeData[0].dataResults.ATDiff) < rp.sondeData[0].currentPoint.tolAirTemp) { rp.sondeData[0].dataResults.airTemp = true; }
                        }
                    }
                }
                
            }


            string outFileName = AppDomain.CurrentDomain.BaseDirectory + "VRunData\\" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + "-Vrun-Fix";


            System.Runtime.Serialization.IFormatter formatterA = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.Stream streamA = new System.IO.FileStream(outFileName + ".bin", System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);
            formatter.Serialize(streamA, localList);
            streamA.Close();



            */

        }


        //Report tool bar controls.
        private void genDetailReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread genDetail = new System.Threading.Thread(reportThread);
            genDetail.Name = "genDetailReport";
            genDetail.SetApartmentState(System.Threading.ApartmentState.STA);
            genDetail.Start();
        }

        [STAThread]
        void reportThread()
        {
            OpenFileDialog reportDetail = new OpenFileDialog();
            reportDetail.Title = "Load Bin";
            reportDetail.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "VRunData\\";
            reportDetail.Filter = "Bin|*.bin";

            if (DialogResult.OK == reportDetail.ShowDialog())
            {
                //Loading data
                List<object> localList;

                System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                System.IO.Stream stream = new System.IO.FileStream(reportDetail.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                localList = (List<object>)formatter.Deserialize(stream);
                stream.Close();

                //Sorting data
                if (curValidation == null)
                {
                    curValidation = new ValidateCalibration.validatePressureCalibration();
                }

                //Upadating the bin file data to the latest test points.

                List<ValidateCalibration.validatePoint> curSettings = loadValidateSPFile(AppDomain.CurrentDomain.BaseDirectory + @"validate.cfg");

                for (int i = 0; i < localList.Count; i++)
                {
                    List<ValidateCalibration.reportData> resetTestPoints = (List<ValidateCalibration.reportData>)localList[i];  //Converting the object to the reportData
                    for (int p = 0; p < resetTestPoints.Count; p++)
                    {
                        resetTestPoints[p].sondeData[0].currentPoint.tolAirTemp = curSettings[i].tolAirTemp;    //Updating the Air Temp Tolerance
                        resetTestPoints[p].sondeData[0].currentPoint.tolHumidity = curSettings[i].tolHumidity;  //Updating the Humidity Tolerance
                    }

                    localList[i] = (object)resetTestPoints;
                    
                    localList[i] = (object)curValidation.processReportData((List<ValidateCalibration.reportData>)localList[i]);
                }

                localList = curValidation.sort(localList);

                //Report lines
                List<string> report = new List<string>();

                string header = "";

                foreach (object sp in localList)
                {
                    List<ValidateCalibration.reportData> curSP = (List<ValidateCalibration.reportData>)sp;

                    foreach (ValidateCalibration.reportData rp in curSP)
                    {
                        //Setting up the data containers.
                        if (report.Count == 0)
                        {
                            header = "SondeID,";
                            

                            foreach (ValidateCalibration.reportData rp1 in curSP)
                            {
                                string tempSetup = rp1.sondeID + ",";
                                report.Add(tempSetup);
                            }
                        }//End of settin gup data containers.
                    }

                    header += curSP[0].sondeData[0].currentPoint.airTempSetPoint.ToString() + "@" + curSP[0].sondeData[0].currentPoint.pressureSetPoint.ToString() + "," + "PResult,";
                    header += "TDiff,TResult,UDiff,UResult,";

                    for (int i = 0; i < report.Count; i++)
                    {
                        report[i] += curSP[i].sondeData[0].dataResults.pDiff.ToString("0.000") + "," + curSP[i].sondeData[0].dataResults.pressure.ToString() + "," +
                            curSP[i].sondeData[0].dataResults.ATDiff.ToString("0.000") + "," + curSP[i].sondeData[0].dataResults.airTemp.ToString() + "," +
                            curSP[i].sondeData[0].dataResults.uDiff.ToString("0.000") + "," + curSP[i].sondeData[0].dataResults.humidity.ToString() + ",";
                    }


                }

                report.Insert(0, header);   //Putting the header on the report.

                /*
                foreach (object sp in localList)
                {
                    List<ValidateCalibration.reportData> curSP = (List<ValidateCalibration.reportData>)sp;
                    string temp = curSP[0].sondeData[0].currentPoint.airTempSetPoint.ToString("0.0") + "," + curSP[0].sondeData[0].currentPoint.pressureSetPoint.ToString("0.0") + ",";

                    if (header == "")
                    {
                        header += "SPAT,SPP,";
                    }

                    foreach (ValidateCalibration.reportData rp in curSP)
                    {
                        if (report.Count == 0)
                        {
                            header += "SondeID,PDiff,ATDiff,UDiff,";
                        }

                        temp += rp.sondeID + ",";
                        temp += rp.sondeData[0].dataResults.pDiff.ToString("0.000") + ",";
                        temp += rp.sondeData[0].dataResults.ATDiff.ToString("0.000") + ",";
                        temp += rp.sondeData[0].dataResults.uDiff.ToString("0.000") + ",";
                    }

                    if (report.Count == 0)
                    {
                        report.Add(header);
                    }

                    report.Add(temp);
                }
                 */ 

                //Saving the file.
                System.IO.File.WriteAllLines(reportDetail.FileName.Substring(0, reportDetail.FileName.Length - 3) + "csv", report.ToArray());

                System.Diagnostics.Process.Start(reportDetail.FileName.Substring(0, reportDetail.FileName.Length - 3) + "csv");
            }

        }

        private void genSimpleReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread genDetail = new System.Threading.Thread(reportSimpleThread);
            genDetail.Name = "genSimpleReport";
            genDetail.SetApartmentState(System.Threading.ApartmentState.STA);
            genDetail.Start();
        }

        [STAThread]
        void reportSimpleThread()
        {
            OpenFileDialog reportDetail = new OpenFileDialog();
            reportDetail.Title = "Load Bin";
            reportDetail.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "VRunData\\";
            reportDetail.Filter = "Bin|*.bin";

            if (DialogResult.OK == reportDetail.ShowDialog())
            {
                //Loading data
                List<object> localList;

                System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                System.IO.Stream stream = new System.IO.FileStream(reportDetail.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                localList = (List<object>)formatter.Deserialize(stream);
                stream.Close();

                //Sorting data
                if (curValidation == null)
                {
                    curValidation = new ValidateCalibration.validatePressureCalibration();
                }

                //Upadating the bin file data to the latest test points.

                List<ValidateCalibration.validatePoint> curSettings = loadValidateSPFile(AppDomain.CurrentDomain.BaseDirectory + @"validate.cfg");

                for (int i = 0; i < localList.Count; i++)
                {
                    List<ValidateCalibration.reportData> resetTestPoints = (List<ValidateCalibration.reportData>)localList[i];  //Converting the object to the reportData
                    for (int p = 0; p < resetTestPoints.Count; p++)
                    {
                        resetTestPoints[p].sondeData[0].currentPoint.tolAirTemp = curSettings[i].tolAirTemp;    //Updating the Air Temp Tolerance
                        resetTestPoints[p].sondeData[0].currentPoint.tolHumidity = curSettings[i].tolHumidity;  //Updating the Humidity Tolerance
                    }

                    localList[i] = (object)resetTestPoints;

                    localList[i] = (object)curValidation.processReportData((List<ValidateCalibration.reportData>)localList[i]);
                }

                localList = curValidation.sort(localList);

                curValidation.generateReport(localList, reportDetail.FileName.Substring(0, reportDetail.FileName.Length - 3) + "html");
                System.Diagnostics.Process.Start(reportDetail.FileName.Substring(0, reportDetail.FileName.Length - 3) + "html");
            }
        }



        #endregion

        private void checkBoxValidate_CheckedChanged(object sender, EventArgs e)
        {
            if (processPressure != null)
            {
                processPressure.vaildateCalibration = checkBoxValidate.Checked;
            }
        }

        private void stopChamberToolStripMenuItem_Click(object sender, EventArgs e)
        {
            chamber.stopChamberManualMode();
        }

        private void validateRadiosondeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<ValidateCalibration.validatePoint> valRun = loadValidateSPFile(AppDomain.CurrentDomain.BaseDirectory + @"validate.cfg"); //Setting up points that will be checked.

            //Building groups.
            List<Calibration.runnerGroup> testGroups;

            System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.Stream stream = new System.IO.FileStream("curGroup.grp", System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
            testGroups = (List<Calibration.runnerGroup>)formatter.Deserialize(stream);
            stream.Close();


            for (int tg = 0; tg < testGroups.Count; tg++)
            {
                for (int tgRun = 0; tgRun < testGroups[tg].runners.Count; tgRun++)
                {
                    for (int rk = 0; rk < racks.Length; rk++)
                    {
                        for (int rkRun = 0; rkRun < racks[rk].runners.Count; rkRun++)
                        {
                            string tempLoad = testGroups[tg].runners[tgRun].manifoldPos;

                            if (testGroups[tg].runners[tgRun].runnerID == racks[rk].runners[rkRun].runnerID)
                            {
                                testGroups[tg].runners[tgRun] = racks[rk].runners[rkRun];
                                testGroups[tg].runners[tgRun].manifoldPos = tempLoad;
                            }
                        }
                    }

                }
            }


            //setting up the validate objects.
            curValidation = new ValidateCalibration.validatePressureCalibration(sensorPressure, managerPressure, chamber, racks, relayPressureManifold);
            curValidation.statusUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange); //Connecting to status for calibration.

            //connecting to pressure status monitoring.
            managerPressure.statusPressureCondition.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureCondition_PropertyChange);
            managerPressure.statusPressureStability.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusPressureStability_PropertyChange);
            managerPressure.managerError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange);

            curValidation.managerChamber.statusChamberCondition.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberCondition_PropertyChange);
            curValidation.managerChamber.statusChamberStability.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberStability_PropertyChange);
            curValidation.managerChamber.statusChamberTH.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusChamberTH_PropertyChange);

            //Subing to the end of the validation event.
            curValidation.processComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(processComplete_PropertyChange);

            curValidation.start(testGroups, valRun);
        }

        private void testRetestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Testing the disconnect message
            //192.168.1.22:8008

            //Finding the radiosonde.
            foreach (rackCalibration.rackPressure r in racks)   //Racks
            {
                foreach (runnerPressureAssembly.runnerPressure run in r.runners)    //Runners in Rack
                {
                    foreach (ipRadiosonde.ipUniRadiosonde sonde in run.runnerRadiosondePorts)   //Sondes on Runners.
                    {
                        if (sonde.ipAddress == "192.168.1.22" && sonde.port == 8008)
                        {
                            sonde.triggerDisconnectEvent();     //Triggering the radiosonde disconnect message.
                        }
                    }
                }
            }
        }

        void threadRetest()
        {
            //Building groups.
            List<Calibration.runnerGroup> testGroups;

            System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            System.IO.Stream stream = new System.IO.FileStream("curGroup.grp", System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
            testGroups = (List<Calibration.runnerGroup>)formatter.Deserialize(stream);
            stream.Close();

            processPressure = new Calibration.calibrationPressure(sensorPressure, managerPressure, chamber, racks, relayPressureManifold);
            processPressure.statusUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(statusUpdate_PropertyChange); //Connecting to status for calibration.

            processPressure.testReCheck(testGroups, 3);
        }

        /// <summary>
        /// Event queing the end of the validation.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        void processComplete_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //Clearing the pressure process.
            processPressure = null;

            //Stopping chamber.
            chamber.stopChamberManualMode();
        }

        private void manualPressureControlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Program.pManager != null && Program.sensorPressure != null && Program.pRelay != null)
            {
                frmPressureControl frmManualPressure = new frmPressureControl();
                frmManualPressure.Show();
            }
            else
            {
                MessageBox.Show("Unable to open. Hardware not connected.", "Pressure Control Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateValidateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Threading.Thread updateVal = new System.Threading.Thread(threadUpdateValidateData);
            updateVal.Name = "Update Data.";
            updateVal.SetApartmentState(System.Threading.ApartmentState.STA);
            updateVal.Start();
        }

        void threadUpdateValidateData()
        {
            
            List<object> localList;     //Container to hold bin data.

            OpenFileDialog reportUpdate = new OpenFileDialog(); //Opening up the val data open dialog.
            reportUpdate.Title = "Load Bin";        //Title,,, for title sake.
            reportUpdate.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory + "VRunData\\";   //Starting location.
            reportUpdate.Filter = "Bin|*.bin";      //Filtering to bin file.

            if (DialogResult.OK == reportUpdate.ShowDialog())   //Opening dialog, and if ok process data.
            {
                System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();   //Data Formatter.
                System.IO.Stream stream = new System.IO.FileStream(reportUpdate.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);    //Stream Reader.
                localList = (List<object>)formatter.Deserialize(stream);        //Deserializer
                stream.Close();     //Closing the stream.
            }
            else
            {
                MessageBox.Show("Update Canceled.", "Update Stopped", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            OpenFileDialog coefFiles = new OpenFileDialog();    //Open File dialog
            coefFiles.Multiselect = true;   //Allowing multipul files to be selected.
            coefFiles.Filter = "Coef Files (*.p_cal)|*.p_cal|All files (*.*)|*.*";  //Filtering to the p cal files... and all just as a catch all.

            if (DialogResult.OK == coefFiles.ShowDialog())  //Showing the open dialog.
            {

            }
            else
            {
                MessageBox.Show("No Coefficients Loaded.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Pressure_Calculator.Pressure_Calculator pProcessor = new Pressure_Calculator.Pressure_Calculator();     //Pressure Calc Processor.
            ValidateCalibration.validatePressureCalibration valProcessor = new ValidateCalibration.validatePressureCalibration();       //Validataion processor.

            for(int s = 0; s< localList.Count; s++)
            {
                List<ValidateCalibration.reportData> curSP = (List<ValidateCalibration.reportData>)localList[s];  //Converting the sp object to the report data list it is.

                foreach (string coef in coefFiles.FileNames)
                {
                    string[] rawID = coef.Split('\\');
                    string sondeID = rawID[rawID.Length - 1].Replace(".p_cal", "");

                    string[] calFileData = System.IO.File.ReadAllLines(coef);   //Reading in all the cal file data.
                    List<double> coefficients = new List<double>();     //Container for the converting double.
                    for (int i = 1; i < calFileData.Length; i++)
                    {
                        coefficients.Add(Convert.ToDouble(calFileData[i]));     //Converting the line to a double and adding it to the list.
                    }

                    for(int d = 0; d< curSP.Count(); d++)    //Going through the report data for each radiosonde.
                    {
                        if (sondeID == curSP[d].sondeID)  //Looking for the matching file to the validataion run.
                        {
                            curSP[d].sondeData[0] = new ValidateCalibration.currentDataMoment
                            {
                                dataPointDateTime = curSP[d].sondeData[0].dataPointDateTime,    
                                currentPoint = curSP[d].sondeData[0].currentPoint,
                                sondeData = new ipRadiosonde.CalData    //Remaking the sonde data object.
                                {
                                    FirmwareVersion = curSP[d].sondeData[0].sondeData.FirmwareVersion,
                                    Humidity = curSP[d].sondeData[0].sondeData.Humidity,
                                    HumidityFrequency = curSP[d].sondeData[0].sondeData.HumidityFrequency,
                                    InternalTemp = curSP[d].sondeData[0].sondeData.InternalTemp,
                                    PacketNumber = curSP[d].sondeData[0].sondeData.PacketNumber,
                                    PressureCounts = curSP[d].sondeData[0].sondeData.PressureCounts,
                                    PressureRefCounts = curSP[d].sondeData[0].sondeData.PressureRefCounts,
                                    PressureTempCounts = curSP[d].sondeData[0].sondeData.PressureTempCounts,
                                    PressureTempRefCounts = curSP[d].sondeData[0].sondeData.PressureTempRefCounts,
                                    ProbeID = curSP[d].sondeData[0].sondeData.ProbeID,
                                    SerialNumber = curSP[d].sondeData[0].sondeData.SerialNumber,
                                    SondeID = curSP[d].sondeData[0].sondeData.SondeID,
                                    Temperature = curSP[d].sondeData[0].sondeData.Temperature,
                                    TemperatureCounts = curSP[d].sondeData[0].sondeData.TemperatureCounts,
                                    TemperatureRefCounts = curSP[d].sondeData[0].sondeData.TemperatureRefCounts,

                                    //Updating the pressure air temp and pressure reading.
                                    PressureTemperature = pProcessor.calcPressureTemp(curSP[d].sondeData[0].sondeData.PressureTempCounts, curSP[d].sondeData[0].sondeData.PressureTempRefCounts),
                                    Pressure = pProcessor.calcPressure(curSP[d].sondeData[0].sondeData.PressureCounts, curSP[d].sondeData[0].sondeData.PressureRefCounts, pProcessor.calcPressureTemp(curSP[d].sondeData[0].sondeData.PressureTempCounts, curSP[d].sondeData[0].sondeData.PressureTempRefCounts), coefficients.ToArray()),

                                },

                                refAirTemp = curSP[d].sondeData[0].refAirTemp,
                                refHumidity = curSP[d].sondeData[0].refHumidity,
                                refPressure = curSP[d].sondeData[0].refPressure,

                                dataResults = new ValidateCalibration.results(),    //Init data results.

                            };

                        }
                    }

                }

                curSP = valProcessor.processReportData(curSP);      //Retesting the corrected data.
                localList[s] = curSP;

            }

            string[] fileNameBreak = reportUpdate.FileName.Split('\\');     //Breaking the bin file name down.
            string fileName = reportUpdate.FileName.Replace(fileNameBreak.Last(), "re" + fileNameBreak.Last());

            valProcessor.saveBin(fileName, localList);

        }

    }

    class GroupFileData
    {
        public List<string> rawFiles = new List<string>();
        public string referenceFile { get; set; }
        public string groupFileName { get; set; }
    }

}
