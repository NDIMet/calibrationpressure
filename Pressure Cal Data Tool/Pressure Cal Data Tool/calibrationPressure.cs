﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calibration
{
    /// <summary>
    /// This class contains the methods for running the calibration.
    /// </summary>


    /// <summary>
    /// Calibration point information object.
    /// </summary>
    public class calibraitonPoint
    {
        //Calibration point.
        public int index;

        //Air Temp
        public double airTempSetPoint;
        public double airTempLower;
        public double airTempUpper;
        public int packetCountAT;

        //Air Temp Theratal... 
        public double airTHSetPoint;
        public double airTHLower;
        public double airTHUpper;
        public int packetCountTH;

        //Pressure Point
        public double pressureSetPoint;
        public double pressureLower;
        public double pressureUpper;
        public int packetCountPressure;

        public double pressureStabilitySetPoint;
        public double pressureStabilityLower;
        public double pressureStabilityUpper;
        public int packetCountPressureStability;

        //Humidity Point
        public double humidtySetPoint;
        public double humidityLower;
        public double humidityUpper;
        public int packetCountHumidity;

    }

    /// <summary>
    /// This is a group of runners that can be calibrated as the same time.
    /// </summary>
    public class runnerGroup
    {
        public List<runnerPressureAssembly.runnerPressure> runners = new List<runnerPressureAssembly.runnerPressure>(); //Runners that can be calibrated together.
        public List<DateTime[]> calibrationStableTime = new List<DateTime[]>(); //Times when the group meets stable conditions.

    }

    public class reports
    {
        //Date the report is created.
        public DateTime reportData;



        void report()
        {
            reportData = DateTime.Now;
        }

    }

}
