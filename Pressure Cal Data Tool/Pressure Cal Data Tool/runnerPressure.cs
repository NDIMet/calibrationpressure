﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace runnerPressureAssembly
{
    [Serializable]
    public class runnerPressure
    {
        //Public Veriables
        //public ipRadiosonde.ipUniRadiosonde[] runnerRadiosondePorts = new ipRadiosonde.ipUniRadiosonde[8];

        public List<int> currentRadiosondes = new List<int>();

        //Event when the count of radiosondes change.
        //public updateCreater.objectUpdate radiosondePresent = new updateCreater.objectUpdate();

        #region Public Veriables relateding to the Runner, ID, Current Pressure things like that.
        /// <summary>
        /// This is the current stored Pressure. This could change as the leak may change it.
        /// </summary>
        public double currentRunnerPressure = 9999.99; //May not not be useful.

        /// <summary>
        /// This is the current stored pressure average.
        /// </summary>
        public double currentPressureStabilityAvg = 9999.99;

        /// <summary>
        /// Does the runner have a leak or not.
        /// </summary>
        public bool leakStatus = false;


        /// <summary>
        /// Method get or sets the current manifold the runner is connected to.
        /// </summary>
        public string manifold = "XXXX";


        /// <summary>
        /// Method get/sets manifold position it is connected to. 
        /// </summary>
        public string manifoldPos = "XXXX";

        public string runnerID = "";


        #endregion

        //Private Veriables


        /// <summary>
        /// Starting up the runner.
        /// </summary>
        public runnerPressure()
        {
            runnerID = "XXXX";
        }

        /// <summary>
        /// Setup connection to the radiosonde.
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="ports"></param>e
        /// 
    }

    [Serializable]
    public class dataPacketPressureRunner
    {
        public string runnerID;
        public string message;
    }

    

}
