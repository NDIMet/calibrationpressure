﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Pressure_Cal_Data_Tool
{
    static class Program
    {
        public static string version = "1.0.0";


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            
            

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());


        }


        public static ValidateCalibration.reportData[] loadSessionXML(string fileName)
        {
            ValidateCalibration.reportData[] instance = default(ValidateCalibration.reportData[]);
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(ValidateCalibration.reportData[]));
            using (System.IO.TextReader r = new System.IO.StreamReader(fileName, System.Text.Encoding.UTF8))
            {
                instance = (ValidateCalibration.reportData[])serializer.Deserialize(r);
            }

            return instance;
        }

    }

    
}
