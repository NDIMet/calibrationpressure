﻿namespace Pressure_Cal_Data_Tool
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.groupBoxValCalFiles = new System.Windows.Forms.GroupBox();
            this.groupBoxDEBFiles = new System.Windows.Forms.GroupBox();
            this.buttonOutputLoc = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxOutputLoc = new System.Windows.Forms.TextBox();
            this.labelDEBStatus = new System.Windows.Forms.Label();
            this.buttonDEBProcess = new System.Windows.Forms.Button();
            this.buttonSelectDir = new System.Windows.Forms.Button();
            this.labelDEBFolderLOC = new System.Windows.Forms.Label();
            this.textBoxDEBFolder = new System.Windows.Forms.TextBox();
            this.buttonSaveSondeIDs = new System.Windows.Forms.Button();
            this.buttonLoadSondeIDs = new System.Windows.Forms.Button();
            this.buttonRemoveID = new System.Windows.Forms.Button();
            this.buttonAddSondeID = new System.Windows.Forms.Button();
            this.listBoxSondeIDs = new System.Windows.Forms.ListBox();
            this.groupBoxHumidityCal = new System.Windows.Forms.GroupBox();
            this.labelHumidityStatus = new System.Windows.Forms.Label();
            this.buttonHCalProcess = new System.Windows.Forms.Button();
            this.buttonHCalOutputSelectDir = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxHCalOutputLoc = new System.Windows.Forms.TextBox();
            this.buttonHCalSelectDir = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxHCalDataLoc = new System.Windows.Forms.TextBox();
            this.buttonHCalSaveID = new System.Windows.Forms.Button();
            this.buttonLoadHCalSondeIDs = new System.Windows.Forms.Button();
            this.buttonHCalRemoveID = new System.Windows.Forms.Button();
            this.buttonHCalAddID = new System.Windows.Forms.Button();
            this.listBoxHCalSondeID = new System.Windows.Forms.ListBox();
            this.groupBoxValCalFiles.SuspendLayout();
            this.groupBoxDEBFiles.SuspendLayout();
            this.groupBoxHumidityCal.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "File:";
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(38, 19);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(542, 20);
            this.textBoxFileName.TabIndex = 1;
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(586, 16);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(75, 23);
            this.buttonLoad.TabIndex = 2;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // groupBoxValCalFiles
            // 
            this.groupBoxValCalFiles.Controls.Add(this.label1);
            this.groupBoxValCalFiles.Controls.Add(this.buttonLoad);
            this.groupBoxValCalFiles.Controls.Add(this.textBoxFileName);
            this.groupBoxValCalFiles.Location = new System.Drawing.Point(12, 12);
            this.groupBoxValCalFiles.Name = "groupBoxValCalFiles";
            this.groupBoxValCalFiles.Size = new System.Drawing.Size(668, 54);
            this.groupBoxValCalFiles.TabIndex = 3;
            this.groupBoxValCalFiles.TabStop = false;
            this.groupBoxValCalFiles.Text = "Calibration Validation Files";
            // 
            // groupBoxDEBFiles
            // 
            this.groupBoxDEBFiles.Controls.Add(this.buttonOutputLoc);
            this.groupBoxDEBFiles.Controls.Add(this.label2);
            this.groupBoxDEBFiles.Controls.Add(this.textBoxOutputLoc);
            this.groupBoxDEBFiles.Controls.Add(this.labelDEBStatus);
            this.groupBoxDEBFiles.Controls.Add(this.buttonDEBProcess);
            this.groupBoxDEBFiles.Controls.Add(this.buttonSelectDir);
            this.groupBoxDEBFiles.Controls.Add(this.labelDEBFolderLOC);
            this.groupBoxDEBFiles.Controls.Add(this.textBoxDEBFolder);
            this.groupBoxDEBFiles.Controls.Add(this.buttonSaveSondeIDs);
            this.groupBoxDEBFiles.Controls.Add(this.buttonLoadSondeIDs);
            this.groupBoxDEBFiles.Controls.Add(this.buttonRemoveID);
            this.groupBoxDEBFiles.Controls.Add(this.buttonAddSondeID);
            this.groupBoxDEBFiles.Controls.Add(this.listBoxSondeIDs);
            this.groupBoxDEBFiles.Location = new System.Drawing.Point(12, 72);
            this.groupBoxDEBFiles.Name = "groupBoxDEBFiles";
            this.groupBoxDEBFiles.Size = new System.Drawing.Size(668, 274);
            this.groupBoxDEBFiles.TabIndex = 4;
            this.groupBoxDEBFiles.TabStop = false;
            this.groupBoxDEBFiles.Text = "Pressure Calibration _deb.csv Files";
            // 
            // buttonOutputLoc
            // 
            this.buttonOutputLoc.Location = new System.Drawing.Point(586, 120);
            this.buttonOutputLoc.Name = "buttonOutputLoc";
            this.buttonOutputLoc.Size = new System.Drawing.Size(75, 23);
            this.buttonOutputLoc.TabIndex = 12;
            this.buttonOutputLoc.Text = "Select Dir";
            this.buttonOutputLoc.UseVisualStyleBackColor = true;
            this.buttonOutputLoc.Click += new System.EventHandler(this.buttonOutputLoc_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(271, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Output Loc:";
            // 
            // textBoxOutputLoc
            // 
            this.textBoxOutputLoc.Location = new System.Drawing.Point(274, 94);
            this.textBoxOutputLoc.Name = "textBoxOutputLoc";
            this.textBoxOutputLoc.Size = new System.Drawing.Size(387, 20);
            this.textBoxOutputLoc.TabIndex = 10;
            // 
            // labelDEBStatus
            // 
            this.labelDEBStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelDEBStatus.Location = new System.Drawing.Point(6, 242);
            this.labelDEBStatus.Name = "labelDEBStatus";
            this.labelDEBStatus.Size = new System.Drawing.Size(655, 23);
            this.labelDEBStatus.TabIndex = 9;
            this.labelDEBStatus.Text = "Idle..";
            this.labelDEBStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonDEBProcess
            // 
            this.buttonDEBProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDEBProcess.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonDEBProcess.Image = ((System.Drawing.Image)(resources.GetObject("buttonDEBProcess.Image")));
            this.buttonDEBProcess.Location = new System.Drawing.Point(332, 120);
            this.buttonDEBProcess.Name = "buttonDEBProcess";
            this.buttonDEBProcess.Size = new System.Drawing.Size(218, 119);
            this.buttonDEBProcess.TabIndex = 8;
            this.buttonDEBProcess.Text = "Process Them!";
            this.buttonDEBProcess.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.buttonDEBProcess.UseVisualStyleBackColor = true;
            this.buttonDEBProcess.Click += new System.EventHandler(this.buttonDEBProcess_Click);
            // 
            // buttonSelectDir
            // 
            this.buttonSelectDir.Location = new System.Drawing.Point(586, 62);
            this.buttonSelectDir.Name = "buttonSelectDir";
            this.buttonSelectDir.Size = new System.Drawing.Size(75, 23);
            this.buttonSelectDir.TabIndex = 7;
            this.buttonSelectDir.Text = "Select Dir";
            this.buttonSelectDir.UseVisualStyleBackColor = true;
            this.buttonSelectDir.Click += new System.EventHandler(this.buttonSelectDir_Click);
            // 
            // labelDEBFolderLOC
            // 
            this.labelDEBFolderLOC.AutoSize = true;
            this.labelDEBFolderLOC.Location = new System.Drawing.Point(271, 19);
            this.labelDEBFolderLOC.Name = "labelDEBFolderLOC";
            this.labelDEBFolderLOC.Size = new System.Drawing.Size(75, 13);
            this.labelDEBFolderLOC.TabIndex = 6;
            this.labelDEBFolderLOC.Text = "_deb.csv Loc:";
            // 
            // textBoxDEBFolder
            // 
            this.textBoxDEBFolder.Location = new System.Drawing.Point(274, 36);
            this.textBoxDEBFolder.Name = "textBoxDEBFolder";
            this.textBoxDEBFolder.Size = new System.Drawing.Size(387, 20);
            this.textBoxDEBFolder.TabIndex = 5;
            // 
            // buttonSaveSondeIDs
            // 
            this.buttonSaveSondeIDs.Location = new System.Drawing.Point(190, 106);
            this.buttonSaveSondeIDs.Name = "buttonSaveSondeIDs";
            this.buttonSaveSondeIDs.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveSondeIDs.TabIndex = 4;
            this.buttonSaveSondeIDs.Text = "Save";
            this.buttonSaveSondeIDs.UseVisualStyleBackColor = true;
            this.buttonSaveSondeIDs.Click += new System.EventHandler(this.buttonSaveSondeIDs_Click);
            // 
            // buttonLoadSondeIDs
            // 
            this.buttonLoadSondeIDs.Location = new System.Drawing.Point(190, 19);
            this.buttonLoadSondeIDs.Name = "buttonLoadSondeIDs";
            this.buttonLoadSondeIDs.Size = new System.Drawing.Size(75, 23);
            this.buttonLoadSondeIDs.TabIndex = 3;
            this.buttonLoadSondeIDs.Text = "Load";
            this.buttonLoadSondeIDs.UseVisualStyleBackColor = true;
            this.buttonLoadSondeIDs.Click += new System.EventHandler(this.buttonLoadSondeIDs_Click);
            // 
            // buttonRemoveID
            // 
            this.buttonRemoveID.Location = new System.Drawing.Point(190, 77);
            this.buttonRemoveID.Name = "buttonRemoveID";
            this.buttonRemoveID.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoveID.TabIndex = 2;
            this.buttonRemoveID.Text = "Remove";
            this.buttonRemoveID.UseVisualStyleBackColor = true;
            this.buttonRemoveID.Click += new System.EventHandler(this.buttonRemoveID_Click);
            // 
            // buttonAddSondeID
            // 
            this.buttonAddSondeID.Location = new System.Drawing.Point(190, 48);
            this.buttonAddSondeID.Name = "buttonAddSondeID";
            this.buttonAddSondeID.Size = new System.Drawing.Size(75, 23);
            this.buttonAddSondeID.TabIndex = 1;
            this.buttonAddSondeID.Text = "Add";
            this.buttonAddSondeID.UseVisualStyleBackColor = true;
            this.buttonAddSondeID.Click += new System.EventHandler(this.buttonAddSondeID_Click);
            // 
            // listBoxSondeIDs
            // 
            this.listBoxSondeIDs.FormattingEnabled = true;
            this.listBoxSondeIDs.Location = new System.Drawing.Point(9, 21);
            this.listBoxSondeIDs.Name = "listBoxSondeIDs";
            this.listBoxSondeIDs.Size = new System.Drawing.Size(175, 160);
            this.listBoxSondeIDs.TabIndex = 0;
            // 
            // groupBoxHumidityCal
            // 
            this.groupBoxHumidityCal.Controls.Add(this.labelHumidityStatus);
            this.groupBoxHumidityCal.Controls.Add(this.buttonHCalProcess);
            this.groupBoxHumidityCal.Controls.Add(this.buttonHCalOutputSelectDir);
            this.groupBoxHumidityCal.Controls.Add(this.label3);
            this.groupBoxHumidityCal.Controls.Add(this.textBoxHCalOutputLoc);
            this.groupBoxHumidityCal.Controls.Add(this.buttonHCalSelectDir);
            this.groupBoxHumidityCal.Controls.Add(this.label4);
            this.groupBoxHumidityCal.Controls.Add(this.textBoxHCalDataLoc);
            this.groupBoxHumidityCal.Controls.Add(this.buttonHCalSaveID);
            this.groupBoxHumidityCal.Controls.Add(this.buttonLoadHCalSondeIDs);
            this.groupBoxHumidityCal.Controls.Add(this.buttonHCalRemoveID);
            this.groupBoxHumidityCal.Controls.Add(this.buttonHCalAddID);
            this.groupBoxHumidityCal.Controls.Add(this.listBoxHCalSondeID);
            this.groupBoxHumidityCal.Location = new System.Drawing.Point(12, 352);
            this.groupBoxHumidityCal.Name = "groupBoxHumidityCal";
            this.groupBoxHumidityCal.Size = new System.Drawing.Size(668, 222);
            this.groupBoxHumidityCal.TabIndex = 5;
            this.groupBoxHumidityCal.TabStop = false;
            this.groupBoxHumidityCal.Text = "Humidity Calibration";
            // 
            // labelHumidityStatus
            // 
            this.labelHumidityStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelHumidityStatus.Location = new System.Drawing.Point(6, 196);
            this.labelHumidityStatus.Name = "labelHumidityStatus";
            this.labelHumidityStatus.Size = new System.Drawing.Size(655, 23);
            this.labelHumidityStatus.TabIndex = 20;
            this.labelHumidityStatus.Text = "Idle..";
            this.labelHumidityStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonHCalProcess
            // 
            this.buttonHCalProcess.Location = new System.Drawing.Point(386, 146);
            this.buttonHCalProcess.Name = "buttonHCalProcess";
            this.buttonHCalProcess.Size = new System.Drawing.Size(75, 23);
            this.buttonHCalProcess.TabIndex = 19;
            this.buttonHCalProcess.Text = "Process";
            this.buttonHCalProcess.UseVisualStyleBackColor = true;
            this.buttonHCalProcess.Click += new System.EventHandler(this.buttonHCalProcess_Click);
            // 
            // buttonHCalOutputSelectDir
            // 
            this.buttonHCalOutputSelectDir.Location = new System.Drawing.Point(586, 117);
            this.buttonHCalOutputSelectDir.Name = "buttonHCalOutputSelectDir";
            this.buttonHCalOutputSelectDir.Size = new System.Drawing.Size(75, 23);
            this.buttonHCalOutputSelectDir.TabIndex = 18;
            this.buttonHCalOutputSelectDir.Text = "Select Dir";
            this.buttonHCalOutputSelectDir.UseVisualStyleBackColor = true;
            this.buttonHCalOutputSelectDir.Click += new System.EventHandler(this.buttonHCalOutputSelectDir_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(271, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Output Loc:";
            // 
            // textBoxHCalOutputLoc
            // 
            this.textBoxHCalOutputLoc.Location = new System.Drawing.Point(274, 91);
            this.textBoxHCalOutputLoc.Name = "textBoxHCalOutputLoc";
            this.textBoxHCalOutputLoc.Size = new System.Drawing.Size(387, 20);
            this.textBoxHCalOutputLoc.TabIndex = 16;
            this.textBoxHCalOutputLoc.Text = "\\\\eagle\\Users\\Wjones\\Logged Data\\iMet-1 RHOS Test Data\\5.2 Calibration Uncertaint" +
                "y Test - Humidity\\Processed Data";
            // 
            // buttonHCalSelectDir
            // 
            this.buttonHCalSelectDir.Location = new System.Drawing.Point(586, 59);
            this.buttonHCalSelectDir.Name = "buttonHCalSelectDir";
            this.buttonHCalSelectDir.Size = new System.Drawing.Size(75, 23);
            this.buttonHCalSelectDir.TabIndex = 15;
            this.buttonHCalSelectDir.Text = "Select Dir";
            this.buttonHCalSelectDir.UseVisualStyleBackColor = true;
            this.buttonHCalSelectDir.Click += new System.EventHandler(this.buttonHCalSelectDir_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(271, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "_deb.csv Loc:";
            // 
            // textBoxHCalDataLoc
            // 
            this.textBoxHCalDataLoc.Location = new System.Drawing.Point(274, 33);
            this.textBoxHCalDataLoc.Name = "textBoxHCalDataLoc";
            this.textBoxHCalDataLoc.Size = new System.Drawing.Size(387, 20);
            this.textBoxHCalDataLoc.TabIndex = 13;
            this.textBoxHCalDataLoc.Text = "\\\\eagle\\Users\\Wjones\\Logged Data\\iMet-1 RHOS Test Data\\5.2 Calibration Uncertaint" +
                "y Test - Humidity";
            // 
            // buttonHCalSaveID
            // 
            this.buttonHCalSaveID.Location = new System.Drawing.Point(190, 104);
            this.buttonHCalSaveID.Name = "buttonHCalSaveID";
            this.buttonHCalSaveID.Size = new System.Drawing.Size(75, 23);
            this.buttonHCalSaveID.TabIndex = 9;
            this.buttonHCalSaveID.Text = "Save";
            this.buttonHCalSaveID.UseVisualStyleBackColor = true;
            this.buttonHCalSaveID.Click += new System.EventHandler(this.buttonHCalSaveID_Click);
            // 
            // buttonLoadHCalSondeIDs
            // 
            this.buttonLoadHCalSondeIDs.Location = new System.Drawing.Point(190, 17);
            this.buttonLoadHCalSondeIDs.Name = "buttonLoadHCalSondeIDs";
            this.buttonLoadHCalSondeIDs.Size = new System.Drawing.Size(75, 23);
            this.buttonLoadHCalSondeIDs.TabIndex = 8;
            this.buttonLoadHCalSondeIDs.Text = "Load";
            this.buttonLoadHCalSondeIDs.UseVisualStyleBackColor = true;
            this.buttonLoadHCalSondeIDs.Click += new System.EventHandler(this.buttonLoadHCalSondeIDs_Click);
            // 
            // buttonHCalRemoveID
            // 
            this.buttonHCalRemoveID.Location = new System.Drawing.Point(190, 75);
            this.buttonHCalRemoveID.Name = "buttonHCalRemoveID";
            this.buttonHCalRemoveID.Size = new System.Drawing.Size(75, 23);
            this.buttonHCalRemoveID.TabIndex = 7;
            this.buttonHCalRemoveID.Text = "Remove";
            this.buttonHCalRemoveID.UseVisualStyleBackColor = true;
            this.buttonHCalRemoveID.Click += new System.EventHandler(this.buttonHCalRemoveID_Click);
            // 
            // buttonHCalAddID
            // 
            this.buttonHCalAddID.Location = new System.Drawing.Point(190, 46);
            this.buttonHCalAddID.Name = "buttonHCalAddID";
            this.buttonHCalAddID.Size = new System.Drawing.Size(75, 23);
            this.buttonHCalAddID.TabIndex = 6;
            this.buttonHCalAddID.Text = "Add";
            this.buttonHCalAddID.UseVisualStyleBackColor = true;
            this.buttonHCalAddID.Click += new System.EventHandler(this.buttonHCalAddID_Click);
            // 
            // listBoxHCalSondeID
            // 
            this.listBoxHCalSondeID.FormattingEnabled = true;
            this.listBoxHCalSondeID.Location = new System.Drawing.Point(9, 19);
            this.listBoxHCalSondeID.Name = "listBoxHCalSondeID";
            this.listBoxHCalSondeID.Size = new System.Drawing.Size(175, 160);
            this.listBoxHCalSondeID.TabIndex = 5;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(688, 580);
            this.Controls.Add(this.groupBoxHumidityCal);
            this.Controls.Add(this.groupBoxDEBFiles);
            this.Controls.Add(this.groupBoxValCalFiles);
            this.Name = "frmMain";
            this.Text = "600116 Cal Data Tool";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.groupBoxValCalFiles.ResumeLayout(false);
            this.groupBoxValCalFiles.PerformLayout();
            this.groupBoxDEBFiles.ResumeLayout(false);
            this.groupBoxDEBFiles.PerformLayout();
            this.groupBoxHumidityCal.ResumeLayout(false);
            this.groupBoxHumidityCal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.GroupBox groupBoxValCalFiles;
        private System.Windows.Forms.GroupBox groupBoxDEBFiles;
        private System.Windows.Forms.Button buttonDEBProcess;
        private System.Windows.Forms.Button buttonSelectDir;
        private System.Windows.Forms.Label labelDEBFolderLOC;
        private System.Windows.Forms.TextBox textBoxDEBFolder;
        private System.Windows.Forms.Button buttonSaveSondeIDs;
        private System.Windows.Forms.Button buttonLoadSondeIDs;
        private System.Windows.Forms.Button buttonRemoveID;
        private System.Windows.Forms.Button buttonAddSondeID;
        private System.Windows.Forms.ListBox listBoxSondeIDs;
        private System.Windows.Forms.Label labelDEBStatus;
        private System.Windows.Forms.Button buttonOutputLoc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxOutputLoc;
        private System.Windows.Forms.GroupBox groupBoxHumidityCal;
        private System.Windows.Forms.Button buttonHCalSaveID;
        private System.Windows.Forms.Button buttonLoadHCalSondeIDs;
        private System.Windows.Forms.Button buttonHCalRemoveID;
        private System.Windows.Forms.Button buttonHCalAddID;
        private System.Windows.Forms.ListBox listBoxHCalSondeID;
        private System.Windows.Forms.Button buttonHCalOutputSelectDir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxHCalOutputLoc;
        private System.Windows.Forms.Button buttonHCalSelectDir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxHCalDataLoc;
        private System.Windows.Forms.Label labelHumidityStatus;
        private System.Windows.Forms.Button buttonHCalProcess;
    }
}

