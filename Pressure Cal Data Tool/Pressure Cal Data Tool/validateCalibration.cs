﻿///William Jones
///4/15/2014
///InterMet Systems
///
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidateCalibration
{
    /// <summary>
    /// Class dedicated to collecting radiosonde and refreance data to QA check the radiosonde.
    /// This class was designed to be used with the Pressure Cablibration 3 software and hardware.
    /// </summary>
   

    /// <summary>
    /// Data object for radiosondes and referances with included time tag.
    /// </summary>
    [Serializable]
    public struct currentDataMoment
    {
        public DateTime dataPointDateTime;
        public ipRadiosonde.CalData sondeData;
        public double refPressure;
        public double refAirTemp;
        public double refHumidity;
        public validatePoint currentPoint;
        public results dataResults;
  
    }

    [Serializable]
    public class results
    {
        public bool pressure;
        public bool airTemp;
        public bool humidity;

        public double pDiff;
        public double ATDiff;
        public double uDiff;
    }

    [Serializable]
    public struct groupData
    {
        public List<currentDataMoment> groupDataItem;
        public Calibration.runnerGroup group;
    }

    [Serializable]
    public struct curActiveGroup
    {
        public List<groupData> conditionData;
    }

    [Serializable]
    public struct setPointData
    {
        public List<curActiveGroup> groups;
    }

    /// <summary>
    /// Validate point information object.
    /// </summary>
    [Serializable]
    public class validatePoint
    {
        //Calibration point.
        public int index;

        //Air Temp
        public double airTempSetPoint;
        public double airTempLower;
        public double airTempUpper;
        public int packetCountAT;

        //Air Temp Theratal... 
        public double airTHSetPoint;
        public double airTHLower;
        public double airTHUpper;
        public int packetCountTH;

        //Pressure Point
        public double pressureSetPoint;
        public double pressureLower;
        public double pressureUpper;
        public int packetCountPressure;

        public double pressureStabilitySetPoint;
        public double pressureStabilityLower;
        public double pressureStabilityUpper;
        public int packetCountPressureStability;

        //Humidity Point
        public double humidtySetPoint;
        public double humidityLower;
        public double humidityUpper;
        public int packetCountHumidity;

        //Tolerance setpoints
        public double tolPressure;
        public double tolAirTemp;
        public double tolHumidity;

    }

    [Serializable]
    public struct reportData
    {
        public string sondeID;
        public string runner;
        public string runnerPos;
        public List<currentDataMoment> sondeData;
    }
}
