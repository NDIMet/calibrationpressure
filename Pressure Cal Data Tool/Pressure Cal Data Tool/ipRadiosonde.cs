﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ipRadiosonde
{
    [Serializable]
    public struct CalData
    {
        public string ComPortName;
        public string FirmwareVersion;
        public int PacketNumber;
        public string SerialNumber;
        public string ProbeID;
        public string SondeID;
        public double Pressure;
        public int PressureCounts;
        public int PressureRefCounts;
        public double Temperature;
        public int TemperatureCounts;
        public int TemperatureRefCounts;
        public double Humidity;
        public double HumidityFrequency;
        public double PressureTemperature;
        public int PressureTempCounts;
        public int PressureTempRefCounts;
        public double InternalTemp;
    }

    [Serializable]
    public struct PTUData
    {
        public double Pressure;
        public double AirTemperature;
        public double RelativeHumidity;
        public double HumidityTemperature;
    }

    [Serializable]
    public struct StatData
    {
        public double BatteryVoltage;
        public double InternalTemperature;
        public string Firmware;
        public string TXMode;
    }

    [Serializable]
    public struct GPSData
    {
        public double Latitude;
        public double Longitude;
        public double Altitude;
        public int NumberOfSatellites;
        public string GPSTime;
    }

    [Serializable]
    public struct GPSXData
    {
        public double Latitude;
        public double Longitude;
        public double Altitude;
        public int NumberOfSatellites;
        public string GPSTime;
        public double EastVelocity;
        public double NorthVelocity;
        public double AscentVelocity;
    }


    /// <summary>
    /// Radioasonde data packets.
    /// </summary>
    [System.Xml.Serialization.XmlInclude(typeof(radiosondeDataPacket))]
    [Serializable]
    public class radiosondeDataPacket
    {
        public DateTime packetDate;
        public string packetMessage;
        public string ipAddress;
        public int port;
    }

    [System.Xml.Serialization.XmlInclude(typeof(radiosondeCalData))]
    [Serializable]
    public class radiosondeCalData
    {
        public string sondeID;
        public string sondeRawFile;
        public string sondeRefFile;
        public double[] coefData;
        public string status;
        public double meanPressureError;
        public double stDevPressure;
        public double RMSPressure;
        public double meanTempError;
        public double stDevTemp;
        public double RMSTemp;
    }
}
