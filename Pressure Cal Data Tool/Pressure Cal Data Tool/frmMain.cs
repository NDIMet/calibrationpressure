﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Pressure_Cal_Data_Tool
{
    public partial class frmMain : Form
    {
        #region Methods for calibration val data

        public frmMain()
        {
            InitializeComponent();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadLogFile = new OpenFileDialog();
            loadLogFile.Multiselect = false;

            ValidateCalibration.reportData[] fileData = null;

            if (loadLogFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBoxFileName.Text = loadLogFile.FileName;
                fileData = Program.loadSessionXML(loadLogFile.FileName);
            }

            List<sondeData> eachRadiosonde = sortData(fileData);
            exportCSV(eachRadiosonde);

        }

        void exportCSV(List<sondeData> data)
        {
            string dataHeader = "SondeP,SondeAT,SondeU,RefP,RefT,RefU,DiffP,DiffT,DiffU,";

            List<string> radiosondeOutput;

            for (int i = 0; i < data.Count; i++)
            {
  
                radiosondeOutput = new List<string>();
                radiosondeOutput.Add(data[i].sondeID);

                foreach (compiledData cd in data[i].dataFromSetPoint)
                {
                    radiosondeOutput.Add("Pressure," + cd.setPoint.pressureSetPoint.ToString("0.000") +
                                            ",Air Temp," + cd.setPoint.airTempSetPoint.ToString("0.000") +
                                            ",Humidity," + cd.setPoint.humidtySetPoint.ToString("0.000"));

                    radiosondeOutput.Add(dataHeader);

                    foreach (ValidateCalibration.currentDataMoment sd in cd.sondeData)
                    {
                        radiosondeOutput.Add(sd.sondeData.Pressure.ToString("0.000") + "," + sd.sondeData.Temperature.ToString("0.000") + "," +
                            sd.sondeData.Humidity.ToString("0.000") + "," + sd.refPressure.ToString("0.000") + "," +
                            sd.refAirTemp.ToString("0.000") + "," + sd.refHumidity.ToString("0.000") + "," +
                            (sd.refPressure - sd.sondeData.Pressure).ToString("0.000") + "," +
                            (sd.refAirTemp - sd.sondeData.Temperature).ToString("0.000") + "," +
                            (sd.refHumidity - sd.sondeData.Humidity).ToString("0.000"));

                    }
                    radiosondeOutput.Add("");
                }

                System.IO.File.WriteAllLines(AppDomain.CurrentDomain.BaseDirectory + "pExport-" + data[i].sondeID + ".csv", radiosondeOutput.ToArray());
            }

        }

        private List<sondeData> sortData(ValidateCalibration.reportData[] data)
        {
            List<sondeData> eachRadiosonde = new List<sondeData>();

            //Building the containers for each radiosonde.
            for (int i = 0; i < data.Length; i++)
            {
                bool match = false;

                for (int eR = 0; eR < eachRadiosonde.Count; eR++)
                {
                    if (eachRadiosonde[eR].sondeID == data[i].sondeID)
                    {
                        match = true;
                    }
                }

                if (!match)
                {
                    sondeData tempDataHolder = new sondeData();
                    tempDataHolder.sondeID = data[i].sondeID;
                    tempDataHolder.dataFromSetPoint = new List<compiledData>();
                    eachRadiosonde.Add(tempDataHolder);
                }

            }

            //Sorting out data for each radiosonde for each set point.

            for (int i = 0; i < data.Length; i++)
            {
                for (int d = 0; d < eachRadiosonde.Count; d++)  //Radiosonde data loop
                {
                    if (data[i].sondeID == eachRadiosonde[d].sondeID)   //Looking for the matching radiosonde.
                    {
                        compiledData tempCompiled = new compiledData();
                        tempCompiled.setPoint = data[i].sondeData[0].currentPoint;
                        tempCompiled.sondeData = data[i].sondeData;
                        eachRadiosonde[d].dataFromSetPoint.Add(tempCompiled);

                    } //if looking for matching radiosodne

                } //end of radiosonde data loop

            }   //End data loop

            return eachRadiosonde;
        }

        #endregion Methods for calibration val data

        #region Methods related to _deb.csv files

        private void buttonAddSondeID_Click(object sender, EventArgs e)
        {
            string newSondeID = "";

            if (DialogResult.OK == Bill.Interfaces.InputBox("Sonde ID", "Input Sonde ID", ref newSondeID))
            {
                listBoxSondeIDs.Items.Add(newSondeID);
            }

        }

        private void buttonRemoveID_Click(object sender, EventArgs e)
        {
            listBoxSondeIDs.Items.Remove(listBoxSondeIDs.SelectedItem);
        }

        private void buttonLoadSondeIDs_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadIDs = new OpenFileDialog();
            loadIDs.Filter = "Sonde Files (*.snd)|*.snd|All Files (*.*)|*.*";
            loadIDs.FilterIndex = 1;
            loadIDs.Multiselect = false;
            loadIDs.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (DialogResult.OK == loadIDs.ShowDialog())
            {
                string[] toLoadIDs = System.IO.File.ReadAllLines(loadIDs.FileName);

                listBoxSondeIDs.Items.Clear();

                for (int i = 0; i < toLoadIDs.Length; i++)
                {
                    if (toLoadIDs[i] != "")
                    {
                        listBoxSondeIDs.Items.Add(toLoadIDs[i]);
                    }
                }
            }


        }

        private void buttonSaveSondeIDs_Click(object sender, EventArgs e)
        {
            List<string> ids = new List<string>();

            for (int i = 0; i < listBoxSondeIDs.Items.Count; i++)
            {
                ids.Add(listBoxSondeIDs.Items[i].ToString() + "\n");
            }

            System.IO.File.WriteAllLines(AppDomain.CurrentDomain.BaseDirectory + "SondeIDs.snd", ids.ToArray());
            
        }

        private void buttonSelectDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowse = new FolderBrowserDialog();

            if (textBoxDEBFolder.Text != "")
            {
                folderBrowse.SelectedPath = textBoxDEBFolder.Text;
            }

            if (DialogResult.OK == folderBrowse.ShowDialog())
            {
                textBoxDEBFolder.Text = folderBrowse.SelectedPath;
            }

        }

        private void buttonOutputLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowse = new FolderBrowserDialog();

            if (textBoxDEBFolder.Text != "")
            {
                folderBrowse.SelectedPath = textBoxOutputLoc.Text;
            }

            if (DialogResult.OK == folderBrowse.ShowDialog())
            {
                textBoxOutputLoc.Text = folderBrowse.SelectedPath;
            }
        }

        private void buttonDEBProcess_Click(object sender, EventArgs e)
        {
            //processDEBFile(@"\\eagle\Users\Wjones\Logged Data\iMet-1 RHOS Test Data\5.1 Calibration Uncertainty Test - Pressure\20160524-2042\_43501_deb.csv", "43501,20160524-2042");

            if (listBoxSondeIDs.Items.Count == 0)
            {
                MessageBox.Show("No Radiosonde ID Entered.", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                updateDEBStatus("Error: No Radiosonde IDs Entered.");
                return;
            }


            List<string> rootDirs = new List<string>(); //List of all dir that will be searched.

            rootDirs.AddRange(System.IO.Directory.GetDirectories(textBoxDEBFolder.Text));

            if (rootDirs.Count == 0) //If there are no sub dir then use the current dir.
            {
                rootDirs.Add(textBoxDEBFolder.Text);
            }

            //Getting the list of ids requested.
            List<debSondeDataFile> sondes = new List<debSondeDataFile>();

            foreach (string IDs in listBoxSondeIDs.Items)
            {
                debSondeDataFile tempSondeData = new debSondeDataFile();
                tempSondeData.sondeID = IDs;
                tempSondeData.fileInfo = new List<string>();
                sondes.Add(tempSondeData);
            }

            for (int r = 0; r < rootDirs.Count; r++)
            {
                string[] curFiles = System.IO.Directory.GetFiles(rootDirs[r],"*deb.csv");

                foreach (string cf in curFiles)//Spinning through the files in the dir
                {
                    foreach (debSondeDataFile deb in sondes)
                    {
                        if (cf.Contains(deb.sondeID)) 
                        {
                            deb.fileInfo.Add(cf);
                            updateDEBStatus(cf + "Added");
                        }
                    }
                }
            }

            updateDEBStatus(sondes.Count.ToString() + " sondes with " + sondes[0].fileInfo.Count.ToString() + " files each loaded.");
            System.Threading.Thread.Sleep(1000);

            foreach (debSondeDataFile sDF in sondes)    //Each radiosonde id that was requested.
            {
                foreach (string fN in sDF.fileInfo)     //Each file name for each run found.
                {
                    string[] fileNameBreak = fN.Split('\\');

                    updateDEBStatus("Processing: " + sDF.sondeID + "," + fileNameBreak[fileNameBreak.Length - 2]);

                    Update();

                    sDF.finalData.Add(processDEBFile(fN, sDF.sondeID + ","  + fileNameBreak[fileNameBreak.Length-2]));
                }

            }

            //Calcing the average Add later???? MArk,,,,,, maybe,,,,,,, no ok thats fine :(




            foreach (debSondeDataFile sDF in sondes) //Outputting Data to file.
            {
                updateDEBStatus("Writing: " + sDF.sondeID + ",Average");

                Update();

                //outputDEBProcessedData(sDF.averageFinalData, sDF.sondeID + ",Average");

                for (int c = 0; c < sDF.finalData.Count; c++)
                {
                    string[] fileBreak = sDF.fileInfo[c].Split('\\');

                    updateDEBStatus("Writing: " + sDF.sondeID + "," + fileBreak[fileBreak.Length - 2]);

                    Update();

                    outputDEBProcessedData(sDF.finalData[c], sDF.sondeID + "," + fileBreak[fileBreak.Length - 2]);

                }
            }

            updateDEBStatus("Process Complete. Roger, Roger.");

        }

        /// <summary>
        /// Process deb outputed filed from the calibration to STDEV and Uncertainty
        /// </summary>
        /// <param name="fileName"></param>
        List<debFileSPResults> processDEBFile(string fileName, string ID)
        {
            string[] loadedData = System.IO.File.ReadAllLines(fileName);

            List<debFileData> processedLine = new List<debFileData>(); //Converted data that is ready to be processed.

            //Breaking the data line down into elements.
            for (int i = 1; i < loadedData.Length; i++)
            {
                string[] lineBreak = loadedData[i].Split(',');
                debFileData tempData = new debFileData();
                tempData.RefP = Convert.ToDouble(lineBreak[0]); //Ref Pressure
                tempData.RefT = Convert.ToDouble(lineBreak[1]); //Ref Temp
                tempData.pRatio = Convert.ToDouble(lineBreak[2]);   //Pressure ratio
                tempData.CalcTemp = Convert.ToDouble(lineBreak[3]); //Sonde calulated temp
                tempData.PError = Convert.ToDouble(lineBreak[4]);   //Sonde Pressure Error
                tempData.TError = Convert.ToDouble(lineBreak[5]);   //Sonde Temp Error
                tempData.SondeP = tempData.PError + tempData.RefP;  //Sonde Pressure
                processedLine.Add(tempData);
            }

            //Sorting data into setpoints.

            List<debFileSPResults> fileSetPoints = new List<debFileSPResults>();    //The list of setpoint and data for each setpoint

            debFileSPResults tempResults = new debFileSPResults();
            tempResults.pSetPoint = processedLine[0].RefP;
            tempResults.tSetPoint = processedLine[0].RefT;
            tempResults.setPointData.Add(processedLine[0]);
            tempResults.dataFile = fileName;
            fileSetPoints.Add(tempResults);

            for (int s = 1; s < processedLine.Count; s++)
            {
                if (processedLine[s].RefP < fileSetPoints.Last().pSetPoint + 10 && processedLine[s].RefP > fileSetPoints.Last().pSetPoint - 10 &&
                    processedLine[s].RefT < fileSetPoints.Last().tSetPoint + 2 && processedLine[s].RefT > fileSetPoints.Last().tSetPoint - 2)
                {
                    fileSetPoints.Last().setPointData.Add(processedLine[s]);
                }
                else
                {
                    tempResults = new debFileSPResults();
                    tempResults.pSetPoint = processedLine[s].RefP;
                    tempResults.tSetPoint = processedLine[s].RefT;
                    tempResults.setPointData.Add(processedLine[s]);
                    tempResults.dataFile = fileName;
                    fileSetPoints.Add(tempResults);
                }
            }


            //Getting the STDEV for each setpoint for the sample
            foreach (debFileSPResults spr in fileSetPoints)
            {
                //Getting the mean
                for (int m = 0; m < spr.setPointData.Count; m++)
                {
                    spr.meanRefP += spr.setPointData[m].RefP;
                    spr.meanSondeP += spr.setPointData[m].SondeP;
                }

                spr.meanRefP = spr.meanRefP / spr.setPointData.Count;   //Mean reference
                spr.meanSondeP = spr.meanSondeP / spr.setPointData.Count;   //Mean radiosonde pressure
                spr.avgMeanDiff = spr.meanRefP - spr.meanSondeP;            //Mea Diff.

                //Setting the Variance for each point.
                for (int m = 0; m < spr.setPointData.Count; m++)
                {
                    spr.setPointData[m].varRefP = spr.meanRefP - spr.setPointData[m].RefP;
                    spr.setPointData[m].varSondeP = spr.meanSondeP - spr.setPointData[m].SondeP;
                }

                double sqrtVarRefP = 0;
                double sqrtVarSondeP = 0;

                for (int m = 0; m < spr.setPointData.Count; m++)
                {
                    sqrtVarRefP += Math.Pow(spr.setPointData[m].varRefP, 2);
                    sqrtVarSondeP += Math.Pow(spr.setPointData[m].varSondeP, 2);
                }

                //Standed Deviation
                spr.refPSTDEV = Math.Sqrt(sqrtVarRefP / spr.setPointData.Count);      //Ref StDev
                spr.sondePSTDEV = Math.Sqrt(sqrtVarSondeP / spr.setPointData.Count);   //Sonde StDev

                //95% Uncertainty
                spr.refPUncertainty = (spr.refPSTDEV / Math.Sqrt(spr.setPointData.Count)) * 2;
                spr.sondePUncertainty = (spr.sondePSTDEV / Math.Sqrt(spr.setPointData.Count)) * 2;

                //95% Combined Uncertainty
                spr.combinedStdUncertaninty = Math.Sqrt(Math.Pow(spr.refPUncertainty, 2) + Math.Pow(spr.sondePUncertainty, 2));

            }

            
            //outputDEBProcessedData(fileSetPoints, ID);    //Writing to a file.

            return fileSetPoints;

        }

        /// <summary>
        /// Writes processed data to a file
        /// </summary>
        /// <param name="data"></param>
        void outputDEBProcessedData(List<debFileSPResults> data, string ID)
        {
            List<string> outputData = new List<string>();   //All data that will be write to output file.

            outputData.Add("Sonde ID, Run ID");
            outputData.Add(ID);
            outputData.Add("");
            outputData.Add("P Set Point,T Set Point,Mean Ref P,Mean Sonde P,Avg Diff Mean,Ref StDev,Sonde StDev,Ref Uncer,Sonde Uncer,Combined Uncer");

            double curTemp = data[0].tSetPoint;

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].tSetPoint + 2 < curTemp)
                {
                    outputData.Add("");
                    curTemp = data[i].tSetPoint;
                }

                outputData.Add(data[i].pSetPoint.ToString() + "," + data[i].tSetPoint.ToString() + "," +
                    data[i].meanRefP.ToString() + "," + data[i].meanSondeP.ToString() + "," + data[i].avgMeanDiff.ToString() + "," +
                    data[i].refPSTDEV.ToString() + "," + data[i].sondePSTDEV.ToString() + "," +
                    data[i].refPUncertainty.ToString() + "," + data[i].sondePUncertainty.ToString() + "," +
                    data[i].combinedStdUncertaninty.ToString());
            }

            string[] fileBreak = data[0].dataFile.Split('_');
            string dirLoc = "";

            if (textBoxOutputLoc.Text == "")
            {
                dirLoc = AppDomain.CurrentDomain.BaseDirectory;
            }
            else
            {
                dirLoc = textBoxOutputLoc.Text + "\\";
            }


            if (!System.IO.Directory.Exists(dirLoc + fileBreak[fileBreak.Length-2]))
            {
                System.IO.Directory.CreateDirectory(dirLoc + fileBreak[fileBreak.Length - 2]);
            }



            System.IO.File.WriteAllLines(dirLoc + fileBreak[fileBreak.Length - 2] + "\\" + ID + ".csv", outputData.ToArray());

        }

        void updateDEBStatus(string message)
        {
            labelDEBStatus.Text = message;
        }

        #endregion Methods related to _deb.csv files



        #region Methods related to processing Humidity Calibration Data

        private void buttonLoadHCalSondeIDs_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadIDs = new OpenFileDialog();
            loadIDs.Filter = "Sonde Files (*.snd)|*.snd|All Files (*.*)|*.*";
            loadIDs.FilterIndex = 1;
            loadIDs.Multiselect = false;
            loadIDs.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            if (DialogResult.OK == loadIDs.ShowDialog())
            {
                string[] toLoadIDs = System.IO.File.ReadAllLines(loadIDs.FileName);

                listBoxHCalSondeID.Items.Clear();

                for (int i = 0; i < toLoadIDs.Length; i++)
                {
                    if (toLoadIDs[i] != "")
                    {
                        listBoxHCalSondeID.Items.Add(toLoadIDs[i]);
                    }
                }
            }
        }

        private void buttonHCalAddID_Click(object sender, EventArgs e)
        {
            string newSondeID = "";

            if (DialogResult.OK == Bill.Interfaces.InputBox("Sonde ID", "Input Sonde ID", ref newSondeID))
            {
                listBoxHCalSondeID.Items.Add(newSondeID);
            }
        }

        private void buttonHCalRemoveID_Click(object sender, EventArgs e)
        {
            listBoxHCalSondeID.Items.Remove(listBoxHCalSondeID.SelectedItem);
        }

        private void buttonHCalSaveID_Click(object sender, EventArgs e)
        {
            List<string> ids = new List<string>();

            for (int i = 0; i < listBoxHCalSondeID.Items.Count; i++)
            {
                ids.Add(listBoxHCalSondeID.Items[i].ToString() + "\n");
            }

            System.IO.File.WriteAllLines(AppDomain.CurrentDomain.BaseDirectory + "SondeHCalIDs.snd", ids.ToArray());
        }

        private void buttonHCalSelectDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowse = new FolderBrowserDialog();

            if (textBoxHCalDataLoc.Text != "")
            {
                folderBrowse.SelectedPath = textBoxHCalDataLoc.Text;
            }

            if (DialogResult.OK == folderBrowse.ShowDialog())
            {
                textBoxHCalDataLoc.Text = folderBrowse.SelectedPath;
            }
        }

        private void buttonHCalOutputSelectDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowse = new FolderBrowserDialog();

            if ( textBoxHCalOutputLoc.Text != "")
            {
                folderBrowse.SelectedPath = textBoxHCalOutputLoc.Text;
            }

            if (DialogResult.OK == folderBrowse.ShowDialog())
            {
                textBoxHCalOutputLoc.Text = folderBrowse.SelectedPath;
            }
        }

        private void buttonHCalProcess_Click(object sender, EventArgs e)
        {
            if (listBoxHCalSondeID.Items.Count == 0)
            {
                MessageBox.Show("No Radiosonde ID Entered.", "Search Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                updateDEBStatus("Error: No Radiosonde IDs Entered.");
                return;
            }

            //Calc for humidity.
            E_E_Calculation_Project.EEHumidityCalculation calculon = new E_E_Calculation_Project.EEHumidityCalculation();

            List<string> rootDirs = new List<string>(); //List of all dir that will be searched.

            rootDirs.AddRange(System.IO.Directory.GetDirectories(textBoxHCalDataLoc.Text));

            if (rootDirs.Count == 0) //If there are no sub dir then use the current dir.
            {
                rootDirs.Add(textBoxHCalDataLoc.Text);
            }

            //Getting the list of ids requested.
            List<hCalSondeDataFile> sondes = new List<hCalSondeDataFile>();

            foreach (string IDs in listBoxHCalSondeID.Items)
            {
                hCalSondeDataFile tempSondeData = new hCalSondeDataFile();
                tempSondeData.sondeID = IDs;
                sondes.Add(tempSondeData);
            }

            for (int r = 0; r < rootDirs.Count; r++)
            {
                string[] rawFiles = System.IO.Directory.GetFiles(rootDirs[r], "*.u_raw");
                string[] coefFiles = System.IO.Directory.GetFiles(rootDirs[r], "*.u_cal");
                string[] refFiles = System.IO.Directory.GetFiles(rootDirs[r], "*Corrected.u_ref");

                for (int cr = 0; cr < sondes.Count; cr++)
                {
                    hCalRunItems tempSondeCalItems = new hCalRunItems();

                    foreach (string rf in rawFiles)
                    {
                        if(rf.Contains(sondes[cr].sondeID))
                        {
                            tempSondeCalItems.rawFileLoc = rf;
                            string[] loadedRawFile = System.IO.File.ReadAllLines(rf);

                            for (int i = 5; i < loadedRawFile.Length; i++)
                            {
                                calRawData tempRaw = new calRawData();
                                string[] rawLineBreak = loadedRawFile[i].Split(',');
                                tempRaw.timeStamp = Convert.ToDouble(rawLineBreak[0]);
                                tempRaw.humFreq = Convert.ToDouble(rawLineBreak[1]);
                                tempRaw.airTemp = Convert.ToDouble(rawLineBreak[2]);
                                tempRaw.internalTemp = Convert.ToDouble(rawLineBreak[3]);

                                tempSondeCalItems.rawData.Add(tempRaw);
                            }


                        }
                    }

                    foreach (string cf in coefFiles)
                    {
                        if (cf.Contains(sondes[cr].sondeID))
                        {
                            tempSondeCalItems.coefFileLoc = cf;

                            string[] loadedCoef = System.IO.File.ReadAllLines(cf);

                            tempSondeCalItems.coefData.ID = sondes[cr].sondeID;
                            tempSondeCalItems.coefData.coefs = new double[]{Convert.ToDouble(loadedCoef[1]), Convert.ToDouble(loadedCoef[2]),
                                                Convert.ToDouble(loadedCoef[3]), Convert.ToDouble(loadedCoef[4]), Convert.ToDouble(loadedCoef[5])};

                        }
                    }

                    if (refFiles.Count() > 0)
                    {
                        tempSondeCalItems.refFileLoc = refFiles.Last();

                        string[] loadedRefFile = System.IO.File.ReadAllLines(tempSondeCalItems.refFileLoc);

                        for (int i = 5; i < loadedRefFile.Length; i++)
                        {
                            calRefData tempRefData = new calRefData();
                            string[] loadedRefBreak = loadedRefFile[i].Split(',');
                            tempRefData.timeStamp = Convert.ToDouble(loadedRefBreak[0]);
                            tempRefData.airTemp = Convert.ToDouble(loadedRefBreak[1]);
                            tempRefData.humidity = Convert.ToDouble(loadedRefBreak[2]);

                            tempSondeCalItems.refData.Add(tempRefData);
                        }
                    }

                    sondes[cr].calRuns.Add(tempSondeCalItems);

                    //Getting the calculated humidity.
                    if (tempSondeCalItems.coefData.coefs != null)
                    {
                        for (int cl = 0; cl < tempSondeCalItems.rawData.Count; cl++)
                        {
                            tempSondeCalItems.rawData[cl].calcHumidity = calculon.calcRH(tempSondeCalItems.rawData[cl].humFreq,
                                                                                            tempSondeCalItems.rawData[cl].airTemp,
                                                                                            tempSondeCalItems.rawData[cl].internalTemp,
                                                                                            tempSondeCalItems.coefData.coefs);
                        }
                    }


                }

            }


            //Processing the looded data.
            foreach (hCalSondeDataFile s in sondes)
            {
                foreach (hCalRunItems cal in s.calRuns)
                {
                    if (cal.coefData.coefs != null)
                    {
                        List<hCalResults> curResults = getCalResults(cal);
                        cal.calResults.AddRange(curResults);
                    }
                }
            }

            //Getting the average for each radiosondes requested.
            foreach (hCalSondeDataFile s in sondes)
            {
                s.avgResults = new List<hCalResults>();

                s.avgResults.Add(new hCalResults());
                s.avgResults.Add(new hCalResults());
                s.avgResults.Add(new hCalResults());
                s.avgResults.Add(new hCalResults());


                int countRuns = 0;
                for (int i = 0; i < s.calRuns.Count; i++) //Each cal run.
                {
                    if (s.calRuns[i].coefFileLoc != null)
                    {
                        for (int x = 0; x < s.calRuns[i].calResults.Count; x++) //each set point of each cal.
                        {
                            s.avgResults[x].combinedHStdUncertaninty += s.calRuns[i].calResults[x].combinedHStdUncertaninty;
                            s.avgResults[x].combinedTStdUncertaninty += s.calRuns[i].calResults[x].combinedTStdUncertaninty;
                            s.avgResults[x].hSetPoint += s.calRuns[i].calResults[x].hSetPoint;
                            s.avgResults[x].meanRefH += s.calRuns[i].calResults[x].meanRefH;
                            s.avgResults[x].meanRefT += s.calRuns[i].calResults[x].meanRefT;
                            s.avgResults[x].meanSondeH += s.calRuns[i].calResults[x].meanSondeH;
                            s.avgResults[x].meanSondeT += s.calRuns[i].calResults[x].meanSondeT;
                            s.avgResults[x].refHSTDEV += s.calRuns[i].calResults[x].refHSTDEV;
                            s.avgResults[x].refHUncertainty += s.calRuns[i].calResults[x].refHUncertainty;
                            s.avgResults[x].refTSTDEV += s.calRuns[i].calResults[x].refTSTDEV;
                            s.avgResults[x].refTUncertainty += s.calRuns[i].calResults[x].refTUncertainty;
                            s.avgResults[x].sondeHSTDEV += s.calRuns[i].calResults[x].sondeHSTDEV;
                            s.avgResults[x].sondeHUncertainty += s.calRuns[i].calResults[x].sondeHUncertainty;
                            s.avgResults[x].sondeTSTDEV += s.calRuns[i].calResults[x].sondeTSTDEV;
                            s.avgResults[x].sondeTUncertainty += s.calRuns[i].calResults[x].sondeTUncertainty;
                            s.avgResults[x].tSetPoint += s.calRuns[i].calResults[x].tSetPoint;

                        
                        }

                        countRuns++;
                    }

                }

                for (int a = 0; a < s.avgResults.Count; a++)
                {
                    s.avgResults[a].combinedHStdUncertaninty = s.avgResults[a].combinedHStdUncertaninty / countRuns;
                    s.avgResults[a].combinedTStdUncertaninty = s.avgResults[a].combinedTStdUncertaninty / countRuns;
                    s.avgResults[a].hSetPoint = s.avgResults[a].hSetPoint / countRuns;
                    s.avgResults[a].meanRefH = s.avgResults[a].meanRefH / countRuns;
                    s.avgResults[a].meanRefT = s.avgResults[a].meanRefT / countRuns;
                    s.avgResults[a].meanSondeH = s.avgResults[a].meanSondeH / countRuns;
                    s.avgResults[a].meanSondeT = s.avgResults[a].meanSondeT / countRuns;
                    s.avgResults[a].refHSTDEV = s.avgResults[a].refHSTDEV / countRuns;
                    s.avgResults[a].refHUncertainty = s.avgResults[a].refHUncertainty / countRuns;
                    s.avgResults[a].refTSTDEV = s.avgResults[a].refTSTDEV / countRuns;
                    s.avgResults[a].refTUncertainty = s.avgResults[a].refTUncertainty / countRuns;
                    s.avgResults[a].sondeHSTDEV = s.avgResults[a].sondeHSTDEV / countRuns;
                    s.avgResults[a].sondeHUncertainty = s.avgResults[a].sondeHUncertainty / countRuns;
                    s.avgResults[a].sondeTSTDEV = s.avgResults[a].sondeTSTDEV / countRuns;
                    s.avgResults[a].sondeTUncertainty = s.avgResults[a].sondeTUncertainty / countRuns;
                    s.avgResults[a].tSetPoint = s.avgResults[a].tSetPoint / countRuns;
                }

            }



            //Outputing the data.
            foreach (hCalSondeDataFile s in sondes)
            {
                outPutHData(s);
            }
        }

        List<hCalResults> getCalResults(hCalRunItems calData)
        {

            List<alignedData> tempAllAligned = new List<alignedData>();

            for (int i = 0; i < calData.rawData.Count; i++)
            {
                for (int r = 0; r < calData.refData.Count; r++)
                {
                    if (calData.rawData[i].timeStamp > calData.refData[r].timeStamp - 0.4 &&
                            calData.rawData[i].timeStamp < calData.refData[r].timeStamp + 0.4)
                    {
                        alignedData tempAligned = new alignedData();
                        tempAligned.rawData = calData.rawData[i];
                        tempAligned.refData = calData.refData[r];
                        tempAllAligned.Add(tempAligned);
                    }
                }
            }

            //Breaking the total data set into setpoints crunching.
            List<hCalResults> compiledResults = new List<hCalResults>();    //object to be returned.
            hCalResults tempResults = new hCalResults();
            alignedData curSetpoint = tempAllAligned.First();

            for (int i = 0; i < tempAllAligned.Count; i++)
            {
                if (curSetpoint.rawData.airTemp > tempAllAligned[i].rawData.airTemp - 1 &&
                    curSetpoint.rawData.airTemp < tempAllAligned[i].rawData.airTemp + 1 &&
                    curSetpoint.rawData.calcHumidity > tempAllAligned[i].rawData.calcHumidity - 10 &&
                    curSetpoint.rawData.calcHumidity < tempAllAligned[i].rawData.calcHumidity + 10)
                {
                    alignedData tempAligned = new alignedData();
                    tempAligned.rawData = tempAllAligned[i].rawData;
                    tempAligned.refData = tempAllAligned[i].refData;

                    //Average Setpoints from reference
                    tempResults.tSetPoint += tempAligned.refData.airTemp;
                    tempResults.hSetPoint += tempAligned.refData.humidity;

                    tempResults.meanRefT += tempAligned.refData.airTemp;
                    tempResults.meanRefH += tempAligned.refData.humidity;

                    tempResults.meanSondeT += tempAligned.rawData.airTemp;
                    tempResults.meanSondeH += tempAligned.rawData.calcHumidity;

                    tempResults.comData.Add(tempAligned);
                }
                else
                {

                    calcOutput(ref tempResults);    //Sending out the data to be crunched.

                    compiledResults.Add(tempResults);   //Adding to the list

                    tempResults = new hCalResults();    //Resetting to the next loop.

                    curSetpoint = tempAllAligned[i + 1];    //setting the next compared set point.
                }
            }

            calcOutput(ref tempResults);//Sending out the data to be crunched.

            compiledResults.Add(tempResults); //Adding to the list

            return compiledResults;

        }

        void calcOutput(ref hCalResults tempResults)
        {
            tempResults.tSetPoint = tempResults.tSetPoint / tempResults.comData.Count;
            tempResults.hSetPoint = tempResults.hSetPoint / tempResults.comData.Count;

            tempResults.meanRefT = tempResults.meanRefT / tempResults.comData.Count;
            tempResults.meanRefH = tempResults.meanRefH / tempResults.comData.Count;

            tempResults.meanSondeT = tempResults.meanSondeT / tempResults.comData.Count;
            tempResults.meanSondeH = tempResults.meanSondeH / tempResults.comData.Count;

            double sqrtVarRefHumidity = 0;
            double sqrtVarRefAirTemp = 0;
            double sqrtVarSondeHumidity = 0;
            double sqrtVarSondeAirTemp = 0;

            for (int x = 0; x < tempResults.comData.Count; x++)
            {
                tempResults.comData[x].varRefHumidity = tempResults.meanRefH - tempResults.comData[x].refData.humidity;
                sqrtVarRefHumidity += Math.Pow(tempResults.comData[x].varRefHumidity, 2);

                tempResults.comData[x].varRefAirTemp = tempResults.meanRefT - tempResults.comData[x].refData.airTemp;
                sqrtVarRefAirTemp += Math.Pow(tempResults.comData[x].varRefAirTemp, 2);

                tempResults.comData[x].varSondeHumidity = tempResults.meanSondeH - tempResults.comData[x].rawData.calcHumidity;
                sqrtVarSondeHumidity += Math.Pow(tempResults.comData[x].varSondeHumidity, 2);

                tempResults.comData[x].varSondeAirTemp = tempResults.meanSondeT - tempResults.comData[x].rawData.airTemp;
                sqrtVarSondeAirTemp += Math.Pow(tempResults.comData[x].varSondeAirTemp, 2);

            }

            tempResults.refHSTDEV = Math.Sqrt(sqrtVarRefHumidity / (tempResults.comData.Count - 1));
            tempResults.refTSTDEV = Math.Sqrt(sqrtVarRefAirTemp / (tempResults.comData.Count - 1));

            tempResults.sondeHSTDEV = Math.Sqrt(sqrtVarSondeHumidity / (tempResults.comData.Count - 1));
            tempResults.sondeTSTDEV = Math.Sqrt(sqrtVarSondeAirTemp / (tempResults.comData.Count - 1));

            tempResults.refHUncertainty = tempResults.refHSTDEV * 2;
            tempResults.refTUncertainty = tempResults.refTSTDEV * 2;

            tempResults.sondeHUncertainty = tempResults.sondeHSTDEV * 2;
            tempResults.sondeTUncertainty = tempResults.sondeTSTDEV * 2;

            tempResults.combinedHStdUncertaninty = Math.Sqrt(Math.Pow(tempResults.refHUncertainty, 2) + Math.Pow(tempResults.sondeHUncertainty, 2));
            tempResults.combinedTStdUncertaninty = Math.Sqrt(Math.Pow(tempResults.refTUncertainty, 2) + Math.Pow(tempResults.sondeTUncertainty, 2));


        }

        void outPutHData(hCalSondeDataFile sondeData)
        {
            List<string> outputData = new List<string>();   //All data that will be write to output file.

            outputData.Add("Sonde ID, Run ID");
            outputData.Add(sondeData.sondeID + ",Average");

            outputData.Add("H Set Point, T Set Point, Ref H StDev, Ref T StDev, Sonde H StDev, Sonde T StDev, Ref H Uncer, Ref T Uncer, Sonde H Uncer, Sonde T Uncer, Combined H Uncer, Combined T Uncer");

            for (int sp = 0; sp < sondeData.avgResults.Count; sp++)
            {
                outputData.Add(sondeData.avgResults[sp].hSetPoint.ToString() + "," + sondeData.avgResults[sp].tSetPoint.ToString() + "," +
                    sondeData.avgResults[sp].refHSTDEV.ToString() + "," + sondeData.avgResults[sp].refTSTDEV.ToString() + "," +
                    sondeData.avgResults[sp].sondeHSTDEV.ToString() + "," + sondeData.avgResults[sp].sondeTSTDEV.ToString() + "," +
                    sondeData.avgResults[sp].refHUncertainty.ToString() + "," + sondeData.avgResults[sp].refTUncertainty.ToString() + "," +
                    sondeData.avgResults[sp].sondeHUncertainty.ToString() + "," + sondeData.avgResults[sp].sondeTUncertainty.ToString() + "," +
                    sondeData.avgResults[sp].combinedHStdUncertaninty.ToString() + "," + sondeData.avgResults[sp].combinedTStdUncertaninty.ToString());
            }
            outputData.Add("");





            for(int i = 0; i< sondeData.calRuns.Count; i++)
            {
                if (sondeData.calRuns[i].rawFileLoc != null)    //Protecting from non runs.
                {
                    string[] fileNameBreak = sondeData.calRuns[i].rawFileLoc.Split(new char[] { '_', '.', '\\' });

                    outputData.Add("Sonde ID, Run ID");
                    outputData.Add(sondeData.sondeID + "," + fileNameBreak[fileNameBreak.Length-5]);
                    
                    outputData.Add("H Set Point, T Set Point, Ref H StDev, Ref T StDev, Sonde H StDev, Sonde T StDev, Ref H Uncer, Ref T Uncer, Sonde H Uncer, Sonde T Uncer, Combined H Uncer, Combined T Uncer");

                    for (int sp = 0; sp < sondeData.calRuns[i].calResults.Count; sp++)
                    {
                        outputData.Add(sondeData.calRuns[i].calResults[sp].hSetPoint.ToString() + "," + sondeData.calRuns[i].calResults[sp].tSetPoint.ToString() + "," +
                            sondeData.calRuns[i].calResults[sp].refHSTDEV.ToString() + "," + sondeData.calRuns[i].calResults[sp].refTSTDEV.ToString() + "," +
                            sondeData.calRuns[i].calResults[sp].sondeHSTDEV.ToString() + "," + sondeData.calRuns[i].calResults[sp].sondeTSTDEV.ToString() + "," +
                            sondeData.calRuns[i].calResults[sp].refHUncertainty.ToString() + "," + sondeData.calRuns[i].calResults[sp].refTUncertainty.ToString() + "," +
                            sondeData.calRuns[i].calResults[sp].sondeHUncertainty.ToString() + "," + sondeData.calRuns[i].calResults[sp].sondeTUncertainty.ToString() + "," +
                            sondeData.calRuns[i].calResults[sp].combinedHStdUncertaninty.ToString() + "," + sondeData.calRuns[i].calResults[sp].combinedTStdUncertaninty.ToString());
                    }
                    outputData.Add("");
                }
            }

            string dirLoc = "";

            if (textBoxHCalOutputLoc.Text == "")
            {
                dirLoc = AppDomain.CurrentDomain.BaseDirectory;
            }
            else
            {
                dirLoc = textBoxHCalOutputLoc.Text + "\\";
            }


            System.IO.File.WriteAllLines(dirLoc + sondeData.sondeID + ".csv", outputData.ToArray());


        }


        #endregion End of Humiidity Calibration Data Processing

        private void frmMain_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " (" + Program.version + ")";
        }


    }

#region Data Types
    public class compiledData
    {
        public ValidateCalibration.validatePoint setPoint;
        public List<ValidateCalibration.currentDataMoment> sondeData;   //Radiosonde data for this point.

        public compiledData()
        {
            setPoint = new ValidateCalibration.validatePoint();
            sondeData = new List<ValidateCalibration.currentDataMoment>();
        }
    }

    public class sondeData
    {
        public string sondeID = "";
        public List<compiledData> dataFromSetPoint = new List<compiledData>();
    }

    #region deb Data Types

    public class debSondeDataFile
    {
        public string sondeID = "";
        public List<string> fileInfo = new List<string>();
        public List<List<debFileSPResults>> finalData = new List<List<debFileSPResults>>();
        public List<debFileSPResults> averageFinalData = new List<debFileSPResults>();
    }

    public class debFileData
    {
        public double RefP;
        public double RefT;
        public double pRatio;
        public double CalcTemp;
        public double PError;
        public double TError;
        public double SondeP;
        public double varRefP;
        public double varSondeP;
    }

    public class debFileSPResults
    {
        public string dataFile;     //File that the data was generated from.
        public double pSetPoint;
        public double tSetPoint;
        public double refPSTDEV;
        public double sondePSTDEV;
        public double refPUncertainty;          //Reference standard uncertainty
        public double sondePUncertainty;        //Radiosonde standard uncertainty
        public double combinedStdUncertaninty;   //combined standard uncertainty
        public double meanRefP;
        public double meanSondeP;
        public double avgMeanDiff;              //Differnce from mean from reference to sonde.
        public List<debFileData> setPointData = new List<debFileData>();

    }

#endregion

#region Humidity Calibraiton data types

    public class hCalSondeDataFile
    {
        public string sondeID = "";
        public List<hCalRunItems> calRuns = new List<hCalRunItems>();
        public List<hCalResults> avgResults = new List<hCalResults>();

    }

    public class hCalRunItems
    {
        public string rawFileLoc;
        public List<calRawData> rawData = new List<calRawData>();

        public string refFileLoc;
        public List<calRefData> refData = new List<calRefData>();

        public string coefFileLoc;
        public calCoefData coefData = new calCoefData();

        public List<hCalResults> calResults = new List<hCalResults>();
    }

    public class hCalResults
    {
        public double hSetPoint;
        public double tSetPoint;

        public double refHSTDEV;
        public double sondeHSTDEV;
        public double refHUncertainty;
        public double sondeHUncertainty;
        public double combinedHStdUncertaninty;
        public double meanRefH;
        public double meanSondeH;

        public double refTSTDEV;
        public double sondeTSTDEV;
        public double refTUncertainty;
        public double sondeTUncertainty;
        public double combinedTStdUncertaninty;
        public double meanRefT;
        public double meanSondeT;

        public List<alignedData> comData = new List<alignedData>(); //Data from the setpoint of the calibration.
    }

    public class calRawData
    {
        public double timeStamp;
        public double calcHumidity;
        public double humFreq;
        public double airTemp;
        public double internalTemp;

    }

    public class calCoefData
    {
        public string ID;
        public double[] coefs;
    }

    public class calRefData
    {
        public double timeStamp;
        public double airTemp;
        public double humidity;
    }

    public class alignedData
    {
        public calRawData rawData;
        public calRefData refData;
        public double varRefHumidity;
        public double varRefAirTemp;
        public double varSondeHumidity;
        public double varSondeAirTemp;
    }


#endregion

#endregion

}
